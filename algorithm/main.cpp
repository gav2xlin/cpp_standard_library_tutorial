#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <string>

using namespace std;

bool predicate(int n) {
    return (n %2 != 0);
}

bool predicate2(int n) {
    return (n > 3);
}

int main() {
    {
        vector<int> v = {1, 2, 3, 3, 5};
        auto it = adjacent_find(v.begin(), v.end());

        if (it != v.end())
            cout << "First occurrence of consecutive identical element = " << *it << endl;

        v[3] = 4;

        it = adjacent_find(v.begin(), v.end());

        if (it == v.end())
            cout << "There are no cosecutive identical elemens" << endl;
    }

    {
        struct predicate {
            bool operator()(int a, int b) {
                return (a == b);
            }
        };

        vector<int> v = {1, 2, 3, 3, 4, 5, 5};
        auto it = adjacent_find(v.begin(), v.end());

        if (it != v.end())
            cout << "First occurrence of consecutive identical element = "
                 << *it << endl;

        it = adjacent_find(++it, v.end(), predicate{});

        if (it != v.end())
            cout << "Second occurrence of consecutive identical element = "
                 << *it << endl;
    }

    {
        auto is_even = [](int n) -> bool {
            return (n % 2 == 0);
        };

        vector<int> v = {2, 4, 6, 8, 10};
        bool result;

        result = all_of(v.begin(), v.end(), is_even);

        if (result)
            cout << "Vector contains all even numbers." << endl;

        v[0] = 1;

        result = all_of(v.begin(), v.end(), is_even);

        if (!result)
            cout << "Vector doesn't contain all even numbers." << endl;
    }

    {
        auto is_odd = [](int n) {
            return (n % 2 != 0);
        };

        vector<int> v = {2, 4, 6, 8, 11};
        bool result;

        result = any_of(v.begin(), v.end(), is_odd);

        if (result)
            cout << "Vector contains at least one odd number." << endl;

        v[4] = 10;

        result = any_of(v.begin(), v.end(), is_odd);

        if (!result)
            cout << "Vector contains all even number." << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};
        bool result;

        result = binary_search(v.begin(), v.end(), 3);

        if (result)
            cout << "Element 3 exist in vector." << endl;

        v[2] = 10;

        result = binary_search(v.begin(), v.end(), 3);

        if (!result)
            cout << "Element 3 doesn't exist in vector." << endl;
    }

    {
        std::function<bool(string, string)> comp = [](string s1, string s2) {
            return (s1 == s2);
        };

        vector<string> v = {"ONE", "Two", "Three"};
        bool result;

        result = binary_search(v.begin(), v.end(), "one", comp);

        if (result)
            cout << "String \"one\" exist in vector." << endl;

        v[0] = "Ten";
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2(5);

        copy(v1.begin(), v1.end(), v2.begin());

        cout << "Vector v2 contains following elements" << endl;

        for (auto it = v2.begin(); it != v2.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2(5);

        copy_backward(v1.begin(), v1.end(), v2.end());

        cout << "Vector v2 contains following elements" << endl;

        for (auto it = v2.begin(); it != v2.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2(3);

        copy_if(v1.begin(), v1.end(), v2.begin(), predicate);

        cout << "Following are the Odd numbers from vector" << endl;

        for (auto it = v2.begin(); it != v2.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2(3);

        copy_n(v1.begin(), 3, v2.begin());

        cout << "Vector v2 contains following elements" << endl;

        for (auto it = v2.begin(); it != v2.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v = {1, 3, 3, 3, 3};
        int cnt;

        cnt = count(v.begin(), v.end(), 3);

        cout << "Number 3 occurs " << cnt << " times." << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};
        int cnt;

        cnt = count_if(v.begin(), v.end(), predicate2);

        cout << "There are " << cnt << " numbers are greater that 3." << endl;
    }

    {
        vector<int> v1 = {1, 2, 3};
        vector<int> v2 = {1, 2, 4, 4, 5};
        bool result;

        result = equal(v1.begin(), v1.end(), v2.begin());

        if (!result)
            cout << "Vector range is not equal." << endl;

        v2[2] = 3;

        result = equal(v1.begin(), v1.end(), v2.begin());

        if (result)
            cout << "Vector range is equal." << endl;
    }

    {
        vector<string> v1 = {"one", "two", "three"};
        vector<string> v2 = {"ONE", "THREE", "THREE"};
        bool result;

        result = equal(v1.begin(), v1.end(), v2.begin(), [](string, string) {return true;});

        if (result)
            cout << "Vector range is equal." << endl;
    }

    {
        vector<int> v = {1, 2, 2, 2, 2};
        int cnt = 0;

        auto range = equal_range(v.begin(), v.end(), 2);

        for (auto it = range.first; it != range.second; ++it)
            ++cnt;

        cout << "Element 2 occurs " << cnt << " times." << endl;
    }

    {
        vector<int> v = {2, 2, 2, 1, 1};
        int cnt = 0;

        auto range = equal_range(v.begin(), v.end(), 2, [](int a, int b) {
            return (a > b);
        });

        for (auto it = range.first; it != range.second; ++it)
            ++cnt;

        cout << "Element 2 occurs " << cnt << " times." << endl;
    }

    {
        vector<int> v(5);

        fill(v.begin(), v.end(), 1);

        cout << "Vector contains following elements" << endl;

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v(5, 1);

        fill_n(v.begin() + 2, 3, 4);

        cout << "Vector contains following elements" << endl;

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v(5, 1);

        fill_n(v.begin() + 2, 3, 4);

        cout << "Vector contains following elements" << endl;

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        int val = 5;
        vector<int> v = {1, 2, 3, 4, 5};

        auto result = find(v.begin(), v.end(), val);

        if (result != end(v))
            cout << "Vector contains element " << val << endl;

        val = 15;

        result = find(v.begin(), v.end(), val);

        if (result == end(v))
            cout << "Vector doesn't contain element " << val << endl;
    }

    {
        vector<int> v1 = {1, 2, 1, 2, 1, 2};
        vector<int> v2 = {1, 2};

        auto result = find_end(v1.begin(), v1.end(), v2.begin(), v2.end());

        if (result != v1.end())
            cout << "Last sequence found at location "
                 << distance(v1.begin(), result) << endl;

        v2 = {1, 3};

        result = find_end(v1.begin(), v1.end(), v2.begin(), v2.end());

        if (result == v1.end())
            cout << "Sequence doesn't present in vector." << endl;
    }

    {
        vector<int> v1 = {1, 2, 1, 2, 1, 2};
        vector<int> v2 = {1, 2};

        auto result = find_end(v1.begin(), v1.end(), v2.begin(), v2.end(), binary_pred);

        if (result != v1.end())
            cout << "Last sequence found at location "
                 << distance(v1.begin(), result) << endl;

        v2 = {1, 3};

        result = find_end(v1.begin(), v1.end(), v2.begin(), v2.end(), [](int a, int b) {
            return (a==b);
        });

        if (result == v1.end())
            cout << "Sequence doesn't present in vector." << endl;
    }

    {
        vector<int> v1 = {5, 2, 6, 1, 3, 4, 7};
        vector<int> v2 = {10, 1};

        auto result = find_first_of(v1.begin(), v1.end(), v2.begin(), v2.end());

        if (result != v1.end())
            cout << "Found first match at location "
                 << distance(v1.begin(), result) << endl;

        v2 = {11, 13};

        result = find_end(v1.begin(), v1.end(), v2.begin(), v2.end());

        if (result == v1.end())
            cout << "Sequence doesn't found." << endl;
    }

    {
        auto binary_pred = [](char a, char b) {
            return (tolower(a) == tolower(b));
        };

        vector<char> v1 = {'f', 'c', 'e', 'd', 'b', 'a'};
        vector<char> v2 = {'D', 'F'};

        auto result = find_first_of(v1.begin(), v1.end(), v2.begin(), v2.end(), binary_pred);

        if (result != v1.end())
            cout << "Found first match at location "
                 << distance(v1.begin(), result) << endl;

        v2 = {'x', 'y'};

        result = find_end(v1.begin(), v1.end(), v2.begin(), v2.end(), binary_pred);

        if (result == v1.end())
            cout << "Sequence doesn't found." << endl;
    }

    {
        vector<int> v = {10, 2, 3, 4, 5};
        auto it = find_if(v.begin(), v.end(), unary_pre);

        if (it != end(v))
            cout << "First even number is " << *it << endl;

        v = {1};

        it = find_if(v.begin(), v.end(), [](int n) {
            return ((n % 2) == 0);
        });

        if (it == end(v))
            cout << "Only odd elements present in the sequence." << endl;
    }

    {
        vector<int> v = {2, 4, 61, 8, 10};
        auto it = find_if_not(v.begin(), v.end(), unary_pred);

        if (it != end(v))
            cout << "First odd number is " << *it << endl;

        v = {2, 4, 6, 8, 10};

        it = find_if_not(v.begin(), v.end(), [](int n) {
            return ((n % 2) == 0);
        });

        if (it == end(v))
            cout << "Only enven elements present in the sequence." << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};

        cout << "Vector contains following even numebr" << endl;

        for_each(v.begin(), v.end(), [](int n) {
            if (n % 2 == 0)
                cout << n << ' ';
        });

        cout << endl;
    }

    {
        vector<int> v(5);

        generate(v.begin(), v.end(), rand);

        cout << "Vector contains following random numbers" << endl;

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        int arr[10] = {0};

        /* assign value to only first 5 elements */
        generate_n(arr, 5, rand);

        cout << "First five random numbers are" << endl;

        for (int i = 0; i < 10; ++i)
            cout << arr[i] << endl;
    }

    {
        int arr[10] = {0, 0, 0, 0, 0, -100};

        /* assign value to only first 5 elements */
        auto it = generate_n(arr, 5, rand);

        cout << "First five random numbers are" << endl;

        for (int i = 0; i < 10; ++i)
            cout << arr[i] << endl;

        cout << endl;

        cout << "Iterator points to " << *it << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2 = {3, 4, 5};
        bool result;

        result = includes(v1.begin(), v1.end(), v2.begin(), v2.end());

        if (result)
            cout << "Vector v2 is subset of v1" << endl;

        v2 = {10};

        result = includes(v1.begin(), v1.end(), v2.begin(), v2.end());

        if (!result)
            cout << "Vector v2 is not subset of v1" << endl;
    }

    {
        struct compare {
            bool operator()(char a, char b) {
                return (tolower(a) == tolower(b));
            }
        };

        vector<char> v1 = {'a', 'b', 'c', 'd', 'e'};
        vector<char> v2 = {'C', 'D', 'E'};
        bool result;

        result = includes(v1.begin(), v1.end(), v2.begin(), v2.end(), compare{});

        if (result)
            cout << "Vector v2 is subset of v1" << endl;
    }

    {
        vector<int> v = {1, 3, 2, 4, 5};

        inplace_merge(v.begin(), v.begin() + 2, v.end());

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v = {3, 1, 5, 4, 2};

        inplace_merge(v.begin(), v.begin() + 2, v.end(), [](int a, int b) {
            return (a > b);
        });

        for (int &it : v)
            cout << it << endl;
    }

    {
        vector<int> v = {3, 5, 2, 1, 4};
        bool result;

        result = is_heap(v.begin(), v.end());

        if (!result)
            cout << "Given sequence is not a max heap." << endl;

        v = {5, 4, 3, 2, 1};

        result = is_heap(v.begin(), v.end());

        if (result)
            cout << "Given sequence is a max heap." << endl;
    }

    {
        vector<char> v = {'E', 'd', 'C', 'b', 'A'};
        bool result;

        result = is_heap(v.begin(), v.end());

        if (!result)
            cout << "Given sequence is not a max heap." << endl;

        result = is_heap(v.begin(), v.end(), [](char a, char b) {
            return (tolower(a) == tolower(b));
        });

        if (result)
            cout << "Given sequence is a max heap." << endl;
    }

    {
        vector<int> v = {4, 3, 5, 1, 2};

        auto result = is_heap_until(v.begin(), v.end());

        cout << *result  << " is the first element which "
             << "violates the max heap." << endl;

        v = {5, 4, 3, 2, 1};

        result = is_heap_until(v.begin(), v.end());

        if (result == end(v))
            cout << "Entire range is valid heap." << endl;
    }

    {
        vector<char> v = {'E', 'd', 'C', 'b', 'A'};
        auto result = is_heap_until(v.begin(), v.end());

        cout << char(*result) << " is the first element which "
             << "violates the max heap." << endl;

        v = {5, 4, 3, 2, 1};

        result = is_heap_until(v.begin(), v.end(), [](char a, char b) {
            return (tolower(a) == tolower(b));
        });

        if (result == end(v))
            cout << "Entire range is valid heap." << endl;
    }

    {
        auto is_even = [](int n) {
            return (n % 2 == 0);
        };

        vector<int> v = {1, 2, 3, 4, 5};
        bool result;

        result = is_partitioned(v.begin(), v.end(), is_even);

        if (!result)
            cout << "Vector is not partitioned." << endl;

        partition(v.begin(), v.end(), is_even);

        result = is_partitioned(v.begin(), v.end(), is_even);

        if (result)
            cout << "Vector is paritioned." << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2 = {5, 4, 3, 2, 1};
        bool result;

        result = is_permutation(v1.begin(), v1.end(), v2.begin());

        if (result)
            cout << "Both vector contains same elements." << endl;

        v2[0] = 10;

        result = is_permutation(v1.begin(), v1.end(), v2.begin());

        if (!result)
            cout << "Both vector doesn't contain same elements." << endl;
    }

    {
        vector<char> v1 = {'A', 'B', 'C', 'D', 'E'};
        vector<char> v2 = {'a', 'b', 'c', 'd', 'e'};
        bool result;

        result = is_permutation(v1.begin(), v1.end(), v2.begin());

        if (!result)
            cout << "Both vector doesn't contain same elements." << endl;

        result = is_permutation(v1.begin(), v1.end(), v2.begin(), [](char a, char b) {
            return (tolower(a) == tolower(b));
        });

        if (result)
            cout << "Both vector contains same elements." << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};
        bool result;

        result = is_sorted(v.begin(), v.end());

        if (result)
            cout << "Vector elements are sorted in ascending order." << endl;

        v[0] = 10;

        result = is_sorted(v.begin(), v.end());

        if (!result)
            cout << "Vector elements are not sorted in ascending order." << endl;
    }

    {
        vector<char> v = {'A', 'b', 'C', 'd', 'E'};
        bool result;

        result = is_sorted(v.begin(), v.end());

        if (!result)
            cout << "Vector elements are not sorted in ascending order." << endl;

        result = is_sorted(v.begin(), v.end(), [](char a, char b) {
            return (tolower(a) == tolower(b));
        });

        if (result)
            cout << "Vector elements are sorted in ascending order." << endl;
    }

    {
        vector<int> v = {1, 2, 3, 5, 4};
        auto it = is_sorted_until(v.begin(), v.end());

        cout << "First unsorted element = " << *it << endl;

        v[3] = 4;

        it = is_sorted_until(v.begin(), v.end());

        if (it == end(v))
            cout << "Entire vector is sorted." << endl;
    }

    {
        vector<char> v = {'A', 'b', 'C', 'd', 'E'};

        auto it = is_sorted_until(v.begin(), v.end());

        cout << "First unsorted element = " << *it << endl;

        it = is_sorted_until(v.begin(), v.end(), [](char a, char b) {
            return (tolower(a) == tolower(b));
        });

        if (it == end(v))
            cout << "Entire vector is sorted." << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2 = {10, 20, 30, 40, 50};

        iter_swap(v1.begin(), v2.begin());
        iter_swap(v1.begin() + 1, v2.begin() + 2);

        cout << "Vector v2 contains following elements" << endl;

        for (auto it = v2.begin(); it != v2.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<string> v1 = {"One", "Two", "Three"};
        vector<string> v2 = {"one", "two", "three"};
        bool result;

        result = lexicographical_compare(v1.begin(), v1.end(), v2.begin(), v2.end());

        if (result)
            cout << "v1 is less than v2." << endl;

        v1[0] = "two";

        result = lexicographical_compare(v1.begin(), v1.end(), v2.begin(), v2.end());

        if (!result)
            cout << "v1 is not less than v2." << endl;
    }

    {
        vector<string> v1 = {"one", "two"};
        vector<string> v2 = {"One", "Two", "Three"};
        bool result;

        result = lexicographical_compare(v1.begin(), v1.end(),
                                         v2.begin(), v2.end());

        if (!result)
            cout << "v1 is not less than v2." << endl;

        result = lexicographical_compare(v1.begin(), v1.end(),
                                         v2.begin(), v2.end(), [](string s1, string s2) {
                    return true;
                });

        if (result)
            cout << "v1 is less than v2." << endl;
    }

    {
        vector<int> v = {1, 2, 5, 13, 14};
        auto it = lower_bound(v.begin(), v.end(), 2);

        cout << "First element which greater than 2 is " << *it << endl;

        it = lower_bound(v.begin(), v.end(), 30);

        if (it == end(v))
            cout << "All elements are less than 30" << endl;
    }

    {
        vector<char> v = {'A', 'b', 'C', 'd', 'E'};
        auto it = lower_bound(v.begin(), v.end(), 'C');

        cout << "First element which is greater than \'C\' is " << *it << endl;

        it = lower_bound(v.begin(), v.end(), 'C', ignore_case);

        cout << "First element which is greater than \'C\' is " << *it << endl;

        it = lower_bound(v.begin(), v.end(), 'z', [](char a, char b) {
            return(tolower(a) == tolower(b));
        });

        cout << "All elements are less than \'z\'." << endl;
    }

    return 0;
}