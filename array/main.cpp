#include <iostream>
#include <array>
#include <stdexcept>

using namespace std;

int main(void) {
    {
        array<int, 5> arr = {10, 20, 30, 40, 50};
        size_t i;

        /* print array contents */
        for (i = 0; i < 5; ++i)
            cout << arr.at(i) << " ";
        cout << endl;

        /* generate out_of_range exception. */
        try {
            arr.at(10);
        } catch (out_of_range e) {
            cout << "out_of_range expcepiton caught for " << e.what() << endl;
        }
    }

    {
        array<int, 5> arr = {1, 2, 3, 4, 5};

        /* print last element */
        cout << "Last element of array                    = " << arr.back()
             << endl;

        /* modify last element */
        arr.back() = 50;

        /* print modified array element */
        cout << "after modification last element of array = " << arr.back()
             << endl;
    }

    {
        array <int, 5> arr = {1, 2, 3, 4, 5};

        /* iterator pointing at the start of the array */
        auto itr = arr.begin();

        /* traverse complete container */
        while (itr != arr.end()) {
            cout << *itr << " ";
            ++itr;   /* increment iterator */
        }

        cout << endl;
    }

    {
        array<int, 5> arr = {1, 2, 3, 4, 5};
        auto it = arr.cbegin();

        /* iterate whole array */
        while (it < arr.end()) {
            cout << *it << " ";
            ++it;
        }

        cout << endl;
    }

    {
        /*array<int, 5> arr = {1, 2, 3, 4, 5};
        auto it = arr.cbegin();*/   /* returns a constant iterator */
        /*auto it = arr.cend();*/ /* iterator pointing to past−the−end of array */

        /* ERROR: attemp to modify value will report compilation error */
        /**it = 100;*/
        /**it = 5;*/
    }

    {
        array<int, 5> arr = {10, 20, 30, 40, 50};

        /* We can only iterate container by using it */
        /* Any attemp to modify value pointed by iterator will cause compliation error */
        for (auto it = arr.crbegin(); it != arr.crend(); ++it)
            cout << *it << " ";

        cout << endl;
    }

    {
        array<char, 128> s = {"C++ standard library from tutorialspoint.com"};
        char *p, *q;

        /* pointer to the first element of character array. */
        p = s.data();

        /* print string contents */
        cout << p << endl;

        q = p;

        /* print string using pointer arithmatic */
        while (*q) {
            cout << *q;
            ++q;
        }

        cout << endl;
    }

    {
        /* array size is zero, it will be treated as empty array */
        array<int, 0> arr1;
        array<int, 10> arr2;

        if (arr1.empty())
            cout << "arr1 is empty" << endl;
        else
            cout << "arr1 is not empty" << endl;

        if (arr2.empty())
            cout << "arr2 is empty" << endl;
        else
            cout << "arr2 is not empty" << endl;
    }

    {
        int i;
        array<int, 5> arr;

        for(i = 0; i < 5; ++i)
            arr[i] = i + 1;

        cout << "Original array\n";
        for(i = 0; i < 5; ++i)
            cout << arr[i] << " ";
        cout << endl;

        arr.fill(10);

        cout << "Modified array\n";
        for(i = 0; i < 5; ++i)
            cout << arr[i] << " ";
        cout << endl;
    }

    {
        array<int, 5> arr = {10, 20, 30, 40, 50};

        /* print first element */
        cout << "First element of array                    = " << arr.front()
             << endl;

        /* modify value */
        arr.front() = 1;

        /* print modified value */
        cout << "After modification first element of array = " << arr.front()
             << endl;
    }

    {
        array<int, 10>arr; /* array of 10 integers */

        cout << "maximum size of arr = " << arr.max_size()
             << endl;

        cout << "size of arr         = " << arr.size()
             << endl;
    }

    {
        array<int, 5> arr = {1, 2, 3, 4, 5};

        /* iterator array using [] operator */
        for (size_t i = 0; i < 5; ++i)
            cout << arr[i] << " ";
        cout << endl;

        /* assing new value to the first array element */
        arr[0] = 10;

        /* print modified array */
        for (size_t i = 0; i < 5; ++i)
            cout << arr[i] << " ";
        cout << endl;
    }

    {
        array<int, 5> arr = {10, 20, 30, 40, 50};
        /* reverse iterator points to the last element of the array */
        auto rev_begin = arr.rbegin();

        /* iterator array in reverse order */
        while (rev_begin < arr.rend()) {
            cout << *rev_begin << " ";
            ++rev_begin;
        }

        cout << endl;
    }

    {
        array<int, 5> int_arr;        /* Array of 5 integers */
        array<float, 0> float_arr;    /* Array of 0 floats */

        cout << "Number of elements in int_arr   = " << int_arr.size() << endl;
        cout << "Number of elements in float_arr = " << float_arr.size() << endl;
    }

    {
        array<int, 3> arr1 = {10, 20, 30};
        array<int, 3> arr2 = {51, 52, 53};

        cout << "Contents of arr1 and arr2 before swap operation\n";
        cout << "arr1 = ";
        for (int &i : arr1) cout << i << " ";
        cout << endl;

        cout << "arr2 = ";
        for (int &i : arr2) cout << i << " ";
        cout << endl << endl;

        arr1.swap(arr2);

        cout << "Contents of arr1 and arr2 after swap operation\n";
        cout << "arr1 = ";
        for (int &i : arr1) cout << i << " ";
        cout << endl;

        cout << "arr2 = ";
        for (int &i : arr2) cout << i << " ";
        cout << endl;
    }

    {
        array<int, 3> arr = {10, 20, 30};

        cout << "arr[0] = " << get<0>(arr) << "\n";
        cout << "arr[1] = " << get<1>(arr) << "\n";
        cout << "arr[2] = " << get<2>(arr) << "\n";
    }

    {
        array<int, 5> arr1 = {1, 2, 3, 4, 5};
        array<int, 5> arr2 = {1, 2, 3, 4, 5};
        array<int, 5> arr3 = {1, 2, 4, 5, 3};
        bool result = false;

        result = (arr1 == arr2);

        if (result == true)
            cout << "arr1 and arr2 are equal\n";
        else
            cout << "arr1 and arr2 are not equal\n";

        result = (arr2 == arr3);
        if (result == false)
            cout << "arr2 and arr3 are not equal\n";
        else
            cout << "arr2 and arr3 are equal\n";
    }

    {
        array<int, 5> arr1 = {1, 2, 3, 4, 5};
        array<int, 5> arr2 = {1, 2, 3, 4, 5};
        array<int, 5> arr3 = {1, 2, 4, 5, 3};
        bool result = false;

        result = (arr1 != arr2);
        if (result == false)
            cout << "arr1 and arr2 are equal\n";
        else
            cout << "arr1 and arr2 are not equal\n";

        result = (arr2 != arr3);
        if (result == true)
            cout << "arr2 and arr3 are not equal\n";
        else
            cout << "arr2 and arr3 are equal\n";
    }

    {
        array<int, 5> arr1 = {1, 2, 3, 4, 5};
        array<int, 5> arr2 = {1, 2, 4, 3, 5};
        array<int, 5> arr3 = {1, 2, 5, 3, 4};
        bool result;

        result = (arr1 < arr2);

        if (result == true)
            cout << "arr1 is less than arr2\n";
        else
            cout << "arr2 is less that arr1\n";

        result = (arr3 < arr2);

        if (result == false)
            cout << "arr2 is less than arr3\n";
        else
            cout << "arr3 is less than arr2\n";
    }

    {
        array<int, 5> arr1 = {1, 2, 3, 4, 5};
        array<int, 5> arr2 = {1, 2, 4, 3, 5};
        array<int, 5> arr3 = {1, 2, 1, 4, 3};
        bool result;

        result = (arr1 < arr2);

        if (result == true)
            cout << "arr1 is less than or equal to arr2\n";
        else
            cout << "arr2 is not less that or equal to arr1\n";

        result = (arr1 < arr3);

        if (result == false)
            cout << "arr1 is not less than or equal to arr3\n";
        else
            cout << "arr1 is less than or equal to arr3\n";
    }

    {
        array<int, 5> arr1 = {1, 2, 3, 4, 5};
        array<int, 5> arr2 = {1, 2, 4, 3, 5};
        bool result;

        result = (arr2 > arr1);

        if (result == true)
            cout << "arr2 is greater than arr1\n";
        else
            cout << "arr1 is greater that arr2\n";
    }

    {
        array<int, 5> arr1 = {1, 2, 3, 4, 5};
        array<int, 5> arr2 = {1, 2, 3, 4, 5};

        if (arr1 >= arr2)
            cout << "arr1 is greater than or equal to arr2\n";
    }

    {
        array <int, 5> arr = {1, 2, 3, 4, 5};

        /* iterator pointing at the start of the array */
        auto itr = arr.begin();

        /* traverse complete container */
        while (itr != arr.end()) {
            cout << *itr << " ";
            ++itr;   /* increment iterator */
        }

        cout << endl;
    }

    {
        typedef array<int, 4> arr;

        cout << "Size = " << tuple_size<arr>::value << endl;
    }

    return 0;
}