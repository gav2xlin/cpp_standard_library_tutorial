#include <thread>
#include <vector>
#include <iostream>
#include <atomic>
#include <string>
#include <chrono>
#include <numeric>

std::atomic<bool> lock(false);

void f(int n) {
    for (int cnt = 0; cnt < 100; ++cnt) {
        while(std::atomic_exchange_explicit(&lock, true, std::memory_order_acquire));
        std::cout << "Output from thread " << n << '\n';
        std::atomic_store_explicit(&lock, false, std::memory_order_release);
    }
}

template<class T>
struct node {
    T data;
    node* next;
    node(const T& data) : data(data), next(nullptr) {}
};

template<class T>
class stack {
    std::atomic<node<T>*> head;
public:
    void push(const T& data) {
        node<T>* new_node = new node<T>(data);
        new_node->next = head.load(std::memory_order_relaxed);
        while(!std::atomic_compare_exchange_weak_explicit(&head, &new_node->next,
                                                          new_node, std::memory_order_release, std::memory_order_relaxed));
    }
};

const int N = 10;
std::atomic<int> cnt = ATOMIC_VAR_INIT(N);

std::vector<int> data;

void reader(int id) {
    for(;;) {

        while(std::atomic_fetch_sub(&cnt, 1) <= 0)
            std::atomic_fetch_add(&cnt, 1);

        if(!data.empty())
            std::cout << (  "reader " + std::to_string(id)
                            + " sees " + std::to_string(*data.rbegin()) + '\n');
        if(data.size() == 100)
            break;

        std::atomic_fetch_add(&cnt, 1);

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

void writer() {
    for(int n = 0; n < 100; ++n) {

        while(std::atomic_fetch_sub(&cnt, N+1) != N)
            std::atomic_fetch_add(&cnt, N+1);

        data.push_back(n);
        std::cout << "writer pushed back " << n << '\n';

        std::atomic_fetch_add(&cnt, N+1);

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

const int _N = 10000;
std::atomic<int> _cnt;
std::vector<int> _data(_N);

void _reader(int id) {
    for (;;) {
        int idx = atomic_fetch_sub_explicit(&_cnt, 1, std::memory_order_relaxed);
        if (idx >= 0) {
            std::cout << "reader " << std::to_string(id) << " processed item "
                      << std::to_string(_data[idx]) << '\n';
        } else {
            std::cout << "reader " << std::to_string(id) << " done\n";
            break;
        }
    }
}

class Semaphore {
    std::atomic_char m_signaled;
public:
    Semaphore(bool initial = false) {
        m_signaled = initial;
    }

    void take() {
        while (!std::atomic_fetch_and(&m_signaled, false)) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }

    void put() {
        std::atomic_fetch_or(&m_signaled, true);
    }
};

class ThreadedCounter {
    static const int N = 100;
    static const int REPORT_INTERVAL = 10;
    int m_count;
    bool m_done;
    Semaphore m_count_sem;
    Semaphore m_print_sem;

    void count_up() {
        for (m_count = 1; m_count <= N; m_count++) {
            if (m_count % REPORT_INTERVAL == 0) {
                if (m_count == N) m_done = true;
                m_print_sem.put();
                m_count_sem.take();
            }
        }
        std::cout << "count_up() done\n";
        m_done = true;
        m_print_sem.put();
    }

    void print_count() {
        do {
            m_print_sem.take();
            std::cout << m_count << '\n';
            m_count_sem.put();
        } while (!m_done);
        std::cout << "print_count() done\n";
    }

public:
    ThreadedCounter() : m_done(false) {}
    void run() {
        auto print_thread = std::thread(&ThreadedCounter::print_count, this);
        auto count_thread = std::thread(&ThreadedCounter::count_up, this);
        print_thread.join();
        count_thread.join();
    }
};

std::atomic<int> foo (0);

void set_foo(int x) {
    foo.store(x,std::memory_order_relaxed);
}

void print_foo() {
    int x;
    do {
        x = foo.load(std::memory_order_relaxed);
    } while (x==0);
    std::cout << "test: " << x << '\n';
}

std::atomic<bool> ready (false);
std::atomic<bool> winner (false);

void count1m (int id) {
    while (!ready) {}
    for (int i=0; i<1000000; ++i) {}
    if (!winner.exchange(true)) { std::cout << "thread #" << id << " won!\n"; }
};

std::atomic<long long> data;
void do_work() {
    data.fetch_add(1, std::memory_order_relaxed);
}

int main() {
    {
        std::vector<std::thread> v;
        for (int n = 0; n < 10; ++n) {
            v.emplace_back(f, n);
        }
        for (auto &t: v) {
            t.join();
        }
    }

    {
        stack<int> s;
        s.push(1);
        s.push(2);
        s.push(3);
    }

    {
        std::vector<std::thread> v;
        for (int n = 0; n < N; ++n) {
            v.emplace_back(reader, n);
        }
        v.emplace_back(writer);
        for (auto& t : v) {
            t.join();
        }
    }

    {
        std::iota(_data.begin(), _data.end(), 1);
        _cnt = _data.size() - 1;

        std::vector<std::thread> v;
        for (int n = 0; n < 10; ++n) {
            v.emplace_back(_reader, n);
        }
        for (auto& t : v) {
            t.join();
        }
    }

    {
        ThreadedCounter m_counter;
        m_counter.run();
    }

    {
        std::thread first (print_foo);
        std::thread second (set_foo,10);
        first.join();
        second.join();
    }

    {
        std::vector<std::thread> threads;
        std::cout << "spawning 10 threads that count to 1 million...\n";
        for (int i=1; i<=10; ++i) threads.push_back(std::thread(count1m,i));
        ready = true;
        for (auto& th : threads) th.join();
    }

    {
        std::thread th1(do_work);
        std::thread th2(do_work);
        std::thread th3(do_work);
        std::thread th4(do_work);
        std::thread th5(do_work);

        th1.join();
        th2.join();
        th3.join();
        th4.join();
        th5.join();

        std::cout << "Ans:" << data << '\n';
    }
}