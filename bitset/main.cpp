#include <iostream>
#include <bitset>
#include <string>
#include <sstream>
#include <typeinfo>

using namespace std;

int main(void) {
    {
        bitset<4> b;

        cout << b << endl;
    }

    {
        bitset<4>b(8);

        cout << b << endl;
    }

    {
        string s = "1100";
        bitset<4>b(s);

        cout << b << endl;
    }

    {
        const char *s = "1100";

        bitset<4>b(s);

        cout << b << endl;
    }

    {
        bitset<4> b("1010");
        bitset<4> mask("1000");

        /* Turn off all bits except 3rd bit */
        b &= mask;

        cout << b << endl;
    }

    {
        bitset<4> b("1010");
        bitset<4> mask("0101");

        /* Turn on 0th and 2nd bit */
        b |= mask;

        cout << b << endl;
    }

    {
        bitset<4> b("1010");
        bitset<4> mask("1111");

        /* Invert each bit of bitset b */
        b ^= mask;

        cout << b << endl;
    }

    {
        bitset<4> b("0001");

        b <<= 1;

        cout << b << endl;
    }

    {
        bitset<4> b("1000");

        b >>= 1;

        cout << b << endl;
    }

    {
        bitset<4> b("0000");

        cout << ~b << endl;
    }

    {
        bitset<4> b("0001");

        auto result = b << 1;

        cout << result << endl;
    }

    {
        bitset<4> b("1000");

        auto result = b >> 1;

        cout << result << endl;
    }

    {
        bitset<4> b1("1010");
        bitset<4> b2("1010");

        if (b1 == b2)
            cout << "Both bitsets are equal." << endl;
        b1 >>= 1;

        if (!(b1 == b2))
            cout << "Both bitsets are not equal." << endl;
    }

    {
        bitset<4> b1("1010");
        bitset<4> b2("1110");

        if (b1 != b2)
            cout << "Both bitsets are not equal." << endl;

        b1 = b2;

        if (!(b1 != b2))
            cout << "Both bitsets are equal." << endl;
    }

    {
        bitset<4> b("1010");
        bitset<4> mask("1000");

        /* Trun off all bits except 3rd bit */
        auto result = b & mask;

        cout << result << endl;
    }

    {
        bitset<4> b("1010");
        bitset<4> mask("0101");

        /* Turn on all bits */
        auto result = b | mask;

        cout << result << endl;
    }

    {
        bitset<4> b("1010");
        bitset<4> mask("0101");

        /* Turn on all bits */
        auto result = b ^ mask;

        cout << result << endl;
    }

    {
        string s = "1000";
        istringstream stream(s);
        bitset<2> b;

        /* Store first 2 bits */
        stream >> b;

        cout << "b = " << b << endl;
    }

    {
        string s = "101010";
        istringstream stream(s);
        bitset<2> b1;
        bitset<6> b2;

        /* Store first 2 bits */
        stream >> b1;
        cout << "b1 = " << b1 << endl;

        /* Stores next 4 bits */
        stream >> b2;
        cout << "b2 = " << b2 << endl;
    }

    {
        bitset<4> b;
        bitset<4> mask("1111");

        if (!b.all())
            cout << "All bits are not set." << endl;

        b |= mask;

        if (b.all())
            cout << "All bit are set." << endl;
    }

    {
        bitset<4> b;
        bitset<4> mask("1010");

        if (!b.any())
            cout << "All bits are unset." << endl;

        b |= mask;

        if (b.any())
            cout << "At least one bit is set." << endl;
    }

    {
        bitset<4> b("1110");

        cout << "In bitset " << b << ", " << b.count() << " bits are set." << endl;
    }

    {
        bitset<4> b("1010");

        cout << "Before flip operation b = " << b << endl;

        b.flip();

        cout << "After flip operation b = " << b << endl;
    }

    {
        bitset<4> b("1110");

        cout << "Before flip operation b = " << b << endl;

        b.flip(1);

        cout << "After flip operation b = " << b << endl;
    }

    {
        bitset<4> b;
        bitset<4> mask("1010");

        if (b.none())
            cout << "All bits are unset." << endl;

        b |= mask;

        if (!b.none())
            cout << "At least one bit set." << endl;
    }

    {
        bitset<4> b("1001");

        for (int i = 0; i < 4; ++i)
            cout << "In bitset b[" << i << "] = " << b[i] << endl;
    }

    {
        bitset<4> b;

        cout << "Initial value of bitset = " << b << endl;

        b[1] = 1;
        b[3] = 1;

        cout << "Value of bitset after setting few bits = " << b << endl;
    }

    {
        bitset<4> b("1111");

        cout << "Before reset operation b = " << b << endl;

        b.reset();

        cout << "After reset operation b = " << b << endl;
    }

    {
        bitset<4> b("1111");

        cout << "Before reset operation b = " << b << endl;

        b.reset(0);

        cout << "After reset operation b = " << b << endl;
    }

    {
        bitset<4> b;

        cout << "Before set operation b = " << b << endl;

        b.set();

        cout << "After set operation b = " << b << endl;
    }

    {
        bitset<4> b;

        cout << "Before set operation b = " << b << endl;

        b.set(0, 1);

        cout << "After set operation b = " << b << endl;
    }

    {
        bitset<4> b;

        cout << "Bitset contains " << b.size() << " bits." << endl;
    }

    {
        bitset<4> b(1010);

        if (b.test(1))
            cout << "1st bit is set." << endl;

        if (!b.test(0))
            cout << "0th bit is not set." << endl;
    }

    {
        bitset<4> b;

        string s = b.to_string();

        cout << s << endl;
    }

    {
        bitset<4> b("1010");;
        auto result = b.to_ullong();

        cout << "Decimal representation of " << b << " = " << result << endl;
    }

    {
        bitset<4> b("1010");;
        auto result = b.to_ulong();

        cout << "Decimal representation of " << b << " = " << result << endl;
    }

    {
        bitset<4> b1(1);
        bitset<4> b2(4);

        std::hash<std::bitset<4>> hash_fun;

        cout << "Hash function for b1 = " << hash_fun(b1) << endl;
        cout << "Hash function for b2 = " << hash_fun(b2) << endl;
    }

    return 0;
}