#include <iostream>
#include <complex>
#include <limits>

int main () {
    {
        std::complex<double> mycomplex(50.0, 1.0);
        std::cout << "Real part is: " << std::real(mycomplex) << '\n';
    }

    {
        std::complex<double> mycomplex (50.0,12.0);
        std::cout << "Imaginary part is: " << std::imag(mycomplex) << '\n';
    }

    {
        std::complex<double> mycomplex (1.0,4.0);

        std::cout << "The absolute value of " << mycomplex << " is " << std::abs(mycomplex) << '\n';
    }

    {
        std::complex<double> mycomplex (1.0,4.0);

        std::cout << "The polar form of " << mycomplex;
        std::cout << " is " << std::abs(mycomplex) << "*e^i*" << std::arg(mycomplex) << "rad\n";
    }

    {
        std::complex<double> mycomplex (1.0,5.0);
        std::cout << "The norm of " << mycomplex << " is " << std::norm(mycomplex) << '\n';
    }

    {
        std::complex<double> mycomplex (50.0,2.0);

        std::cout << "The conjugate of " << mycomplex << " is " << std::conj(mycomplex) << '\n';
    }

    {
        std::cout << "The complex whose magnitude is " << 1.0 << '\n';
        std::cout << " and phase angle is " << 0.7 << '\n';
        std::cout << " is " << std::polar (1.0, 0.7) << '\n';
    }

    {
        std::complex<double> mycomplex (std::numeric_limits<double>::infinity(),3.0);

        std::cout << "The projection of " << mycomplex << " is " << std::proj(mycomplex) << '\n';
    }

    return 0;
}