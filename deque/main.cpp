#include <iostream>
#include <deque>

using namespace std;

int main(void) {
    {
        deque<int> d;

        cout << "Size of deque = " << d.size() << endl;
    }

    {
        deque<int> d(5, 1);

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2(d1.begin(), d1.begin() + 3);

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2(d1);

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2(move(d1));

        cout << "Size of deque d1 = " << d1.size() << endl;
        cout << endl;

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        deque<int> d(it);

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2;

        d2.assign(d1.begin(), d1.begin() + 3);

        cout << "Contents of deque d2 are" << endl;

        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;
    }

    {
        deque<int> d;

        d.assign(5, 1);

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        deque<int> d;

        d.assign(it);

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d.at(i) << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Last element of deque = " << d.back() << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque are" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque are" << endl;

        for (auto it = d.cbegin(); it != d.cend(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Size of deque before clear operation = " << d.size() << endl;

        d.clear();

        cout << "Size of deque after clear operation = " << d.size() << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque in reverse order" << endl;

        for (auto it = d.crbegin(); it != d.crend(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d = {1, 2, 5};

        auto it = d.emplace(d.begin() + 2, 3);

        d.emplace(it, 4);

        cout << "Contents of deque are" << endl;

        for (auto it = d.crend() - 1; it >= d.crbegin(); --it)
            cout << *it << endl;
    }

    {
        deque<int> d = {1, 2, 3};

        d.emplace_back(4);
        d.emplace_back(5);

        cout << "Contents of deque are" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d = {3, 4, 5};

        d.emplace_front(2);
        d.emplace_front(1);

        cout << "Contents of deque are" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d;

        if (d.empty())
            cout << "Deque is empty." << endl;

        d.assign(1, 1);

        if (!d.empty())
            cout << "Deque is not empty." << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque before erase operation" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;

        d.erase(d.begin());

        cout << "Contents of deque after erase operation" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque before erase operation" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;

        d.erase(d.begin(), d.begin() + 2);

        cout << "Contents of deque after erase operation" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "First element of deque = " << d.front() << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};
        int *p = NULL;

        p = d.get_allocator().allocate(5);

        for (int i = 0; i < 5; ++i)
            p[i] = i + 1;

        cout << "Contents of deque are" << endl;
        for (int i = 0; i < 5; ++i)
            cout << p[i] << endl;
    }

    {
        deque<int> d = {3, 4, 5};

        auto it = d.insert(d.begin(), 2);
        d.insert(it, 1);

        cout << "Content of deque are" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d;

        d.insert(d.begin(), 5, 5);

        cout << "Content of deque are" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d1 = {3, 4, 5};
        deque<int> d2 = {1, 2};

        d1.insert(d1.begin(), d2.begin(), d2.end());

        cout << "Content of deque are" << endl;

        for (auto it = d1.begin(); it != d1.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2;

        for (int i = 0; i < d1.size(); ++i)
            d2.insert(d2.begin() + i, move(*(d1.begin() + i)));

        cout << "Content of deque are" << endl;

        for (auto it = d2.begin(); it != d2.end(); ++it)
            cout << *it << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        deque<int> d;

        d.insert(d.begin(), it);

        cout << "Content of deque are" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d;

        cout << "max_size of deque = " << d.max_size() << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2;

        d2 = d1;

        cout << "Contents of deque are" << endl;

        for (auto it = d2.begin(); it != d2.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2;

        d2 = move(d1);

        cout << "Contents of deque are" << endl;

        for (auto it = d2.begin(); it != d2.end(); ++it)
            cout << *it << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        deque<int> d;

        d = it;

        cout << "Contents of deque are" << endl;

        for (auto it = d.begin(); it != d.end(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d {1, 2, 3, 4, 5};

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque before pop_back operation." << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;

        d.pop_back();
        d.pop_back();

        cout << endl;

        cout << "Contents of deque before pop_back operation." << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque before pop_front operation." << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;

        d.pop_front();
        d.pop_front();

        cout << endl;

        cout << "Contents of deque before pop_front operation." << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d;

        for (int i = 0; i < 5; ++i)
            d.push_back(i + 1);

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2;

        for (int i = 0; i < d1.size(); ++i)
            d2.push_back(move(d1[i]));

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;
    }

    {
        deque<int> d;

        for (int i = 0; i < 5; ++i)
            d.push_front(i + 1);

        cout << "Contents of deque are" << endl;

        for (int i = 0; i < 5; ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2;

        for (int i = 0; i < d1.size(); ++i)
            d2.push_front(move(d1[i]));

        cout << "Contents of deque d2 are" << endl;

        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;
    }

    {
        deque<int> d = {1, 2, 3, 4, 5};

        cout << "Contents of deque are" << endl;

        for (auto it = d.rbegin(); it != d.rend(); ++it)
            cout << *it << endl;
    }

    {
        deque<int> d;

        cout << "Initial size of deque = " << d.size() << endl;

        d.resize(5);

        cout << "Contents of deque after resize operaion are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d;

        cout << "Initial size of deque = " << d.size() << endl;

        d.resize(5, 10);

        cout << "Contents of deque after resize operaion are" << endl;

        for (int i = 0; i < d.size(); ++i)
            cout << d[i] << endl;
    }

    {
        deque<int> d(10);

        cout << "Initial size of deque = " << d.size() << endl;

        d.resize(5);

        cout << "size of deque after resize operation = " << d.size() << endl;

        d.shrink_to_fit();
    }

    {
        deque<int> d;

        cout << "Initial size of deque = " << d.size() << endl;

        d.assign(1, 1);

        cout << "Size of deque after assign = " << d.size() << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2 = {50, 60, 70};

        cout << "Content of d1 before swap operation" << endl;
        for (int i = 0; i < d1.size(); ++i)
            cout << d1[i] << endl;

        cout << "Content of d2 before swap operation" << endl;
        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;

        cout << endl;

        d1.swap(d2);

        cout << "Content of d1 after swap operation" << endl;
        for (int i = 0; i < d1.size(); ++i)
            cout << d1[i] << endl;

        cout << "Content of d2 after swap operation" << endl;
        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2 = {1, 2, 3, 4, 5};

        if (d1 == d2)
            cout << "Deque d1 and d2 are equal." << endl;

        d1.assign(2, 1);

        if (!(d1 == d2))
            cout << "Deque d1 and d2 are not equal." << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2 = {1, 1, 1};

        if (d1 != d2)
            cout << "Deque d1 and d2 are not equal." << endl;

        d1.assign(3, 1);

        if (!(d1 != d2))
            cout << "Deque d1 and d2 are equal." << endl;
    }

    {
        deque<int> d1 = {1, 2, 3};
        deque<int> d2 = {1, 2, 3, 4, 5};

        if (d1 < d2)
            cout << "Deque d1 is less than d2." << endl;

        d2.assign(1, 1);

        if (!(d1 < d2))
            cout << "Deque d1 is not less than d2." << endl;
    }

    {
        deque<int> d1 = {1, 2, 3};
        deque<int> d2 = {1, 2, 3};

        if (d1 <= d2)
            cout << "Deque d1 is less than or equal to d2." << endl;

        d2.assign(3, 1);

        if (!(d1 <= d2))
            cout << "Deque d1 is not less than or equal to d2." << endl;
    }

    {
        deque<int> d1 = {1, 2, 3};
        deque<int> d2 = {1, 2};

        if (d1 > d2)
            cout << "Deque d1 is greater than d2." << endl;

        d1.assign(1, 1);

        if (!(d1 > d2))
            cout << "Deque d1 is not greater than d2." << endl;
    }

    {
        deque<int> d1 = {1, 2, 3};
        deque<int> d2 = {1, 2, 3};

        if (d1 >= d2)
            cout << "Deque d1 is greater than or equal to d2." << endl;

        d1.assign(3, 1);

        if (!(d1 >= d2))
            cout << "Deque d1 is not greater than or equal to d2." << endl;
    }

    {
        deque<int> d1 = {1, 2, 3, 4, 5};
        deque<int> d2 = {50, 60, 70};

        cout << "Content of d1 before swap operation" << endl;
        for (int i = 0; i < d1.size(); ++i)
            cout << d1[i] << endl;

        cout << "Content of d2 before swap operation" << endl;
        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;

        cout << endl;

        swap(d1, d2);

        cout << "Content of d1 after swap operation" << endl;
        for (int i = 0; i < d1.size(); ++i)
            cout << d1[i] << endl;

        cout << "Content of d2 after swap operation" << endl;
        for (int i = 0; i < d2.size(); ++i)
            cout << d2[i] << endl;
    }

    return 0;
}