#include <thread>
#include <vector>
#include <iostream>
#include <atomic>
#include <new>
#include <typeinfo>
#include <exception>
#include <stdexcept>
#include <functional>
#include <memory>
#include <bitset>
#include <climits>

std::atomic_flag lock = ATOMIC_FLAG_INIT;

void f(int n) {
    for (int cnt = 0; cnt < 100; ++cnt) {
        while (lock.test_and_set(std::memory_order_acquire))
            ;
        std::cout << "Output from thread " << n << '\n';
        lock.clear(std::memory_order_release);
    }
}

struct Foo { virtual ~Foo() {} };
struct Bar { virtual ~Bar() {} };

void my_unexp() { throw; }

void test() throw(std::bad_exception) {
    throw std::runtime_error("test error");
}

struct S {
    virtual void f();
};

struct ooops : std::exception {
    const char* what() const noexcept {return "Ooops! It is a identity error\n";}
};

int main() {
    {
        std::vector<std::thread> v;
        for (int n = 0; n < 10; ++n) {
            v.emplace_back(f, n);
        }
        for (auto &t: v) {
            t.join();
        }
    }

    {
        try {
            int* myarray= new int[500000];
        } catch (std::bad_alloc& ba) {
            std::cerr << "bad_alloc caught: " << ba.what() << '\n';
        }
    }

    {
        Bar b;
        try {
            Foo& f = dynamic_cast<Foo&>(b);
        } catch(const std::bad_cast& e) {
            std::cout << e.what() << '\n';
        }
    }

    {
        std::set_unexpected(my_unexp);
        try {
            test();
        } catch(const std::bad_exception& e) {
            std::cerr << "Caught " << e.what() << '\n';
        }
    }

    {
        std::function<int()> f = nullptr;
        try {
            f();
        } catch(const std::bad_function_call& e) {
            std::cout << e.what() << '\n';
        }
    }

    {
        S* p = nullptr;
        try {
            std::cout << typeid(*p).name() << '\n';
        } catch(const std::bad_typeid& e) {
            std::cout << e.what() << '\n';
        }
    }

    {
        std::shared_ptr<int> p1(new int(42));
        std::weak_ptr<int> wp(p1);
        p1.reset();
        try {
            std::shared_ptr<int> p2(wp);
        } catch(const std::bad_weak_ptr& e) {
            std::cout << e.what() << '\n';
        }
    }

    {
        try {
            throw ooops();
        } catch (std::exception& ex) {
            std::cout << ex.what();
        }
    }

    {
        try {
            std::bitset<5> mybitset (std::string("9848011223"));
        }
        catch (const std::invalid_argument& ia) {
            std::cerr << "Invalid argument: " << ia.what() << '\n';
        }
    }

    {
        int negative = -1;
        int small = 1;
        int large = INT_MAX;
        try {
            new int[negative];
            new int[small]{1,2,3,4};
            new int[large][50000000];
        } catch(const std::bad_array_new_length &e) {
            std::cout << e.what() << '\n';
        }
    }
}