#include <iostream>
#include <forward_list>

using namespace std;

bool cmp_fun(int a, int b) {
    return a > b;
}

bool foo(int n) {
    return (n > 5);
}

int main(void) {
    {
        forward_list<int> fl;

        if (fl.empty())
            cout << "Forward list contains zero elements." << endl;
    }

    {
        forward_list<int> fl(5, 10);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl(5);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2(fl1.begin(), fl1.end());

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2(fl1);

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2(move(fl1));

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        auto il = {1, 2, 3, 4, 5};
        forward_list<int> fl(il);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2;

        fl2.assign(fl1.begin(), fl1.end());

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl;

        fl.assign(5, 10);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        forward_list<int> fl;

        fl.assign(it);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {2, 3, 4, 5};
        auto it = fl.before_begin();

        fl.insert_after(it, 1);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {2, 3, 4, 5};
        auto it = fl.cbefore_begin();

        fl.insert_after(it, 1);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};

        cout << "List contains following elements" << endl;

        for (auto it = fl.cbegin(); it != fl.cend(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};

        if (!fl.empty())
            cout << "List is non−empty before clear operation." << endl;

        fl.clear();

        if (fl.empty())
            cout << "List is empty after clear operation." << endl;
    }

    {
        forward_list<int> fl = {1, 3, 4, 5};

        fl.emplace_after(fl.begin(), 2);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {2, 3, 4, 5};

        fl.emplace_front(1);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl;

        if (fl.empty())
            cout << "List is empty." << endl;

        fl.emplace_front(1);

        if (!fl.empty())
            cout << "List is not empty." << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 1000, 2, 3, 4, 5};

        fl.erase_after(fl.begin());

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};

        fl.erase_after(fl.begin(), fl.end());

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};

        cout << "First element of the list = " << fl.front() << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};
        int *p = NULL;

        p = fl.get_allocator().allocate(5);

        for (int i = 0; i < 5; ++i)
            p[i] = i + 1;

        cout << "List contains following elements" << endl;

        for (int i = 0; i < 5; ++i)
            cout << p[i] << endl;
    }

    {
        forward_list<int> fl ={1};

        for (int i = 4; i > 0; --i)
            fl.insert_after(fl.begin(), i + 1);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl ={1};

        for (int i = 4; i > 0; --i)
            fl.insert_after(fl.begin(), move(i + 1));

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1};

        fl.insert_after(fl.begin(), 4, 1);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {2, 3, 4, 5};
        forward_list<int> fl2 = {1};

        fl2.insert_after(fl2.begin(), fl1.begin(), fl1.end());

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1};
        auto it = {2, 3, 4, 5};

        fl.insert_after(fl.begin(), it);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl;

        cout << "max_size of list = " << fl.max_size() << endl;
    }

    {
        forward_list<int> fl1 = {1, 5, 11, 31};
        forward_list<int> fl2 = {10, 20, 30};

        fl1.merge(fl2);

        cout << "List contains following elements" << endl;

        for (auto it = fl1.begin(); it != fl1.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 5, 11, 31};
        forward_list<int> fl2 = {10, 20, 30};

        fl1.merge(move(fl2));

        cout << "List contains following elements" << endl;

        for (auto it = fl1.begin(); it != fl1.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {31, 11, 5, 1};
        forward_list<int> fl2 = {30, 20, 10};

        fl2.merge(fl1, cmp_fun);

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {31, 11, 5, 1};
        forward_list<int> fl2 = {30, 20, 10};

        fl2.merge(move(fl1), cmp_fun);

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2;

        fl2 = fl1;

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2;

        fl2 = move(fl1);

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        forward_list<int> fl1;

        fl1 = it;

        cout << "List contains following elements" << endl;

        for (auto it = fl1.begin(); it != fl1.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};

        cout << "List contains following elements before"
                " pop_front operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;

        fl.pop_front();
        fl.pop_front();

        cout << "List contains following elements after"
                " pop_front operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl;

        for (int i = 0; i < 5; ++i)
            fl.push_front(i + 1);

        cout << "List contains following elements" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2;

        for (auto it = fl1.begin(); it != fl1.end(); ++it)
            fl2.push_front(move(*it));

        cout << "List fl2 contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 2, 3, 3, 3, 4, 5};

        cout << "List contents before remove operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;

        fl.remove(2);

        cout << "List contents after remove operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        cout << "List contents before remove operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;

        /* remove all elements greater than 5 */
        fl.remove_if(foo);

        cout << "List contents after remove operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl;

        fl.resize(5);

        cout << "List contents after resize operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl;

        fl.resize(5, 10);

        cout << "List contents after resize operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 2, 3, 4, 5};;

        fl.reverse();

        cout << "List contents after reverse operation" << endl;

        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 4, 2, 5, 3};

        cout << "List contents before sorting" << endl;
        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;

        fl.sort();

        cout << "List contents after sorting" << endl;
        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 4, 2, 5, 3};

        cout << "List contents before sorting" << endl;
        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;

        fl.sort(cmp_fun);

        cout << "List contents after sorting" << endl;
        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {2, 3, 4};
        forward_list<int> fl2 = {1, 5};

        fl2.splice_after(fl2.begin(), fl1);

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {2, 3, 4};
        forward_list<int> fl2 = {1, 5};

        fl2.splice_after(fl2.begin(), move(fl1));

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2};
        forward_list<int> fl2 = {1, 3, 4, 5};

        fl2.splice_after(fl2.begin(), fl1, fl1.begin());

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2};
        forward_list<int> fl2 = {1, 3, 4, 5};

        fl2.splice_after(fl2.begin(), move(fl1), fl1.begin());

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4};
        forward_list<int> fl2 = {1, 5};

        fl2.splice_after(fl2.begin(), fl1, fl1.begin(), fl1.end());

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4};
        forward_list<int> fl2 = {1, 5};

        fl2.splice_after(fl2.begin(), move(fl1), fl1.begin(), fl1.end());

        cout << "List contains following elements" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};;
        forward_list<int> fl2 = {10, 20, 30};

        cout << "List fl1 contents before swap operation" << endl;

        for (auto it = fl1.begin(); it != fl1.end(); ++it)
            cout << *it << endl;

        cout << "List fl2 contents before swap operation" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;

        fl1.swap(fl2);

        cout << endl;

        cout << "List fl1 contents after swap operation" << endl;

        for (auto it = fl1.begin(); it != fl1.end(); ++it)
            cout << *it << endl;

        cout << "List fl2 contents after swap operation" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, 1, 2, 2, 3, 4, 5, 5};

        cout << "List elements before unique operation" << endl;
        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;

        fl.unique();

        cout << "List elements after unique operation" << endl;
        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl = {1, -1, -1, -1, 2, 2, -2, -2, 3, -4, 4, -5, -5, 5};

        cout << "List elements before unique operation" << endl;
        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;

        fl.unique(cmp_fun);

        cout << "List elements after unique operation" << endl;
        for (auto it = fl.begin(); it != fl.end(); ++it)
            cout << *it << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2 = {1, 2, 3, 4, 5};

        if (fl1 == fl2)
            cout << "Both lists are equal." << endl;

        fl1.pop_front();

        if (!(fl1 == fl2))
            cout << "Both lists are not equal." << endl;
    }

    {
        forward_list<int> fl1 = {2, 3, 4, 5};
        forward_list<int> fl2 = {1, 2, 3, 4, 5};

        if (fl1 != fl2)
            cout << "Both lists not are equal." << endl;

        fl1.push_front(1);

        if (!(fl1 != fl2))
            cout << "Both lists not equal." << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4};
        forward_list<int> fl2 = {1, 2, 3, 4, 5};

        if (fl1 < fl2)
            cout << "First list is less than second." << endl;

        fl1.push_front(5);
        fl1.push_front(6);
        fl1.push_front(7);

        if (!(fl1 < fl2))
            cout << "First list is not less than second." << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2 = {1, 2, 3, 4, 5};

        if (fl1 <= fl2)
            cout << "First list is less than or equal to second." << endl;

        fl1.push_front(6);

        if (!(fl1 <= fl2))
            cout << "First list is not less than or equal to second." << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2 = {1, 2, 3, 4};

        if (fl1 > fl2)
            cout << "First list is greter than second." << endl;

        fl1 = fl2;

        if (!(fl1 > fl2))
            cout << "First list is not greter than second." << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};
        forward_list<int> fl2 = {1, 2, 3, 4, 5};

        if (fl1 >= fl2)
            cout << "First list is greater than or equal to second." << endl;

        fl2.push_front(6);

        if (!(fl1 >= fl2))
            cout << "First list is not greater than or equal to second." << endl;
    }

    {
        forward_list<int> fl1 = {1, 2, 3, 4, 5};;
        forward_list<int> fl2 = {10, 20, 30};

        cout << "List fl1 contents before swap operation" << endl;

        for (auto it = fl1.begin(); it != fl1.end(); ++it)
            cout << *it << endl;

        cout << "List fl2 contents before swap operation" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;

        swap(fl1, fl2);

        cout << endl;

        cout << "List fl1 contents after swap operation" << endl;

        for (auto it = fl1.begin(); it != fl1.end(); ++it)
            cout << *it << endl;

        cout << "List fl2 contents after swap operation" << endl;

        for (auto it = fl2.begin(); it != fl2.end(); ++it)
            cout << *it << endl;
    }

    return 0;
}