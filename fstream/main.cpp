#include <fstream>
#include <iostream>
#include <cstdio>

using namespace std;

int main () {
    {
        std::fstream fs;
        fs.open("test.txt", std::fstream::in | std::fstream::out | std::fstream::app);

        fs << " more lorem ipsum";

        fs.close();
    }

    {
        std::fstream fs;
        fs.open ("test.txt");
        if (fs.is_open()) {
            fs << "lorem ipsum";
            std::cout << "Operation successfully performed\n";
            fs.close();
        } else {
            std::cout << "Error opening file";
        }
    }

    {
        std::fstream foo;
        std::fstream bar ("test.txt");

        swap(foo,bar);

        foo << "tutorialspoint";

        foo.close();
    }

    {
        std::fstream src,dest;
        src.open ("test.txt");
        dest.open ("copy.txt");

        std::filebuf* inbuf  = src.rdbuf();
        std::filebuf* outbuf = dest.rdbuf();

        char c = inbuf->sbumpc();
        while (c != EOF) {
            outbuf->sputc (c);
            c = inbuf->sbumpc();
        }

        dest.close();
        src.close();
    }

    {
        std::fstream foo;
        std::fstream bar ("test.txt");

        foo.swap(bar);

        foo << "lorem ipsum";

        foo.close();
    }

    {
        std::fstream foo;
        std::fstream bar ("test.txt");

        swap(foo,bar);

        foo << "tutorialspoint";

        foo.close();
    }

    return 0;
}