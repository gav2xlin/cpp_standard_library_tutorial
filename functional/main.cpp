#include <functional>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <numeric>

struct Foo {
    Foo(int num) : num_(num) {}
    void print_add(int i) const { std::cout << num_+i << '\n'; }
    int num_;
};

void print_num(int i) {
    std::cout << i << '\n';
}

struct PrintNum {
    void operator()(int i) const {
        std::cout << i << '\n';
    }
};

void sampleFunction() {
    std::cout << "This is the sample example of function!\n";
}

void checkFunc( std::function<void()> &func ) {
    if( func ) {
        std::cout << "Function is not empty! It is a calling function.\n";
        func();
    } else {
        std::cout << "Function is empty.\n";
    }
}

void call(std::function<int()> f) {
    std::cout << f() << '\n';
}

int normal_function() {
    return 50;
}

using SomeVoidFunc = std::function<void(int)>;

class C {
public:
    C(SomeVoidFunc void_func = nullptr) :
            void_func_(void_func) {
        if (void_func_ == nullptr) {
            void_func_ = std::bind(&C::default_func, this, std::placeholders::_1);
        }
        void_func_(9);
    }

    void default_func(int i) { std::cout << i << '\n'; };

private:
    SomeVoidFunc void_func_;
};

void user_func(int i) {
    std::cout << (i + 1) << '\n';
}

int main() {
    {
        std::function<void(int)> f_display = print_num;
        f_display(-9);

        std::function<void()> f_display_42 = []() { print_num(42); };
        f_display_42();

        std::function<void()> f_display_31337 = std::bind(print_num, 31337);
        f_display_31337();

        std::function<void(const Foo &, int)> f_add_display = &Foo::print_add;
        const Foo foo(314159);
        f_add_display(foo, 1);

        std::function<int(Foo const &)> f_num = &Foo::num_;
        std::cout << "num_: " << f_num(foo) << '\n';

        using std::placeholders::_1;
        std::function<void(int)> f_add_display2 = std::bind(&Foo::print_add, foo, _1);
        f_add_display2(2);

        std::function<void(int)> f_add_display3 = std::bind(&Foo::print_add, &foo, _1);
        f_add_display3(3);

        std::function<void(int)> f_display_obj = PrintNum();
        f_display_obj(18);
    }

    {
        std::function<void()> f1;
        std::function<void()> f2( sampleFunction );

        std::cout << "f1: ";
        checkFunc( f1 );

        std::cout << "f2: ";
        checkFunc( f2 );
    }

    {
        int n = 4;
        std::function<int()> f = [&n](){ return n; };
        call(f);

        n = 5;
        call(f);

        f = normal_function;
        call(f);
    }

    {
        C c1;
        C c2(user_func);
    }

    {
        int values[] = {1000,2000,3000,4000,5000};
        int masks[] = {0xf,0xf,0xf,255,255};
        int results[5];

        std::transform (values, std::end(values), masks, results, std::bit_and<int>());

        std::cout << "results:";
        for (const int& x: results)
            std::cout << ' ' << x;
        std::cout << '\n';
    }

    {
        int flags[] = {10,20,40,80,160,320,640,1280};
        int acc = std::accumulate (flags, std::end(flags), 0, std::bit_or<int>());
        std::cout << "accumulated: " << acc << '\n';
    }

    {
        int flags[] = {10,20,30,40,50,60,70,80,90,100};
        int acc = std::accumulate (flags, std::end(flags), 0, std::bit_xor<int>());
        std::cout << "xor: " << acc << '\n';
    }

    {
        int first[]={10,40,90,40,10};
        int second[]={10,20,30,40,50};
        int results[5];
        std::transform (first, first+5, second, results, std::divides<int>());
        for (int i=0; i<5; i++)
            std::cout << results[i] << ' ';
        std::cout << '\n';
    }

    {
        std::pair<int*,int*> ptiter;
        int foo[]={10,20,30,40};
        int bar[]={10,50,40,80};
        ptiter = std::mismatch (foo, foo+5, bar, std::equal_to<int>());
        std::cout << "First mismatching pair is: " << *ptiter.first;
        std::cout << " and " << *ptiter.second << '\n';
    }

    {
        int numbers[]={200,40,50,100,30};
        std::sort (numbers, numbers+5, std::greater<int>());
        for (int i=0; i<5; i++)
            std::cout << numbers[i] << ' ';
        std::cout << '\n';
    }

    {
        int numbers[]={200,-30,10,-40,0};
        int cx = std::count_if (numbers, numbers+5, std::bind2nd(std::greater_equal<int>(),0));
        std::cout << "There are " << cx << " non-negative elements.\n";
    }

    {
        int foo[]={10,20,5,15,25};
        int bar[]={15,10,20};
        std::sort (foo, foo+5, std::less<int>());
        std::sort (bar, bar+3, std::less<int>());
        if (std::includes (foo, foo+5, bar, bar+3, std::less<int>()))
            std::cout << "foo included.\n";
    }

    {
        int numbers[]={250,500,70,100,125};
        int cx = std::count_if (numbers, numbers+5, std::bind2nd(std::less_equal<int>(),100));
        std::cout << "There are " << cx << " elements lower than or equal to 100.\n";
    }

    {
        bool foo[] = {true,false,true,false};
        bool bar[] = {true,true,false,false};
        bool result[4];
        std::transform (foo, foo+4, bar, result, std::logical_and<bool>());
        std::cout << std::boolalpha << "Logical AND:\n";
        for (int i=0; i<4; i++)
            std::cout << foo[i] << " AND " << bar[i] << " = " << result[i] << "\n";
    }

    {
        bool values[] = {true,false};
        bool result[2];
        std::transform (values, values+2, result, std::logical_not<bool>());
        std::cout << std::boolalpha << "Logical NOT Example as shown below:\n";
        for (int i=0; i<2; i++)
            std::cout << "NOT " << values[i] << " = " << result[i] << "\n";
    }

    {
        bool foo[] = {true,true,false,false};
        bool bar[] = {true,false,true,false};
        bool result[4];
        std::transform (foo, foo+4, bar, result, std::logical_or<bool>());
        std::cout << std::boolalpha << "Logical OR example as shown below:\n";
        for (int i=0; i<4; i++)
            std::cout << foo[i] << " OR " << bar[i] << " = " << result[i] << "\n";
    }

    {
        int numbers[]={10000,3000,35000};
        int result;
        result = std::accumulate (numbers, numbers+3, 100, std::minus<int>());
        std::cout << "The result is " << result << ".\n";
    }

    {
        int numbers[]={1,20,1003,42,56};
        int remainders[5];
        std::transform (numbers, numbers+5, remainders, std::bind2nd(std::modulus<int>(),2));
        for (int i=0; i<5; i++)
            std::cout << numbers[i] << " is " << (remainders[i]==0?"even":"odd") << '\n';
    }

    {
        int numbers[9];
        int factorials[9];
        for (int i=0;i<9;i++) numbers[i]=i+1;
        std::partial_sum (numbers, numbers+9, factorials, std::multiplies<int>());
        for (int i=0; i<5; i++)
            std::cout << "factorial of " << numbers[i] << "! is " << factorials[i] << '\n';
    }

    {
        int numbers[]={100,-200,300,-40,500,-300,200};
        std::transform (numbers, numbers+5, numbers, std::negate<int>());
        for (int i=0; i<4; i++)
            std::cout << numbers[i] << ' ';
        std::cout << '\n';
    }

    {
        int numbers[]={10,20,30,40,50};
        int* pt = std::adjacent_find (numbers, numbers+5, std::not_equal_to<int>()) +1;
        std::cout << " Different element is " << *pt << '\n';
    }

    {
        int first[]={15,12,30,45,15};
        int second[]={10,20,30,40,50};
        int results[5];
        std::transform (first, first+5, second, results, std::plus<int>());
        for (int i=0; i<5; i++)
            std::cout << results[i] << ' ';
        std::cout << '\n';
    }
}