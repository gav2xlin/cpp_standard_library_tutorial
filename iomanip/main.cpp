#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

int main () {
    {
        std::cout << std::hex;
        std::cout << std::setiosflags(std::ios::showbase | std::ios::uppercase);
        std::cout << 100 << std::endl;
    }

    {
        std::cout << std::hex << std::setiosflags(std::ios::showbase);
        std::cout << 100 << std::endl;
        std::cout << std::resetiosflags(std::ios::showbase) << 100 << std::endl;
    }

    {
        std::cout << std::setbase(16);
        std::cout << 110 << std::endl;
    }

    {
        std::cout << std::setfill ('x') << std::setw (10);
        std::cout << 77 << std::endl;
    }

    {
        double f =3.14159;
        std::cout << std::setprecision(5) << f << '\n';
        std::cout << std::setprecision(9) << f << '\n';
        std::cout << std::fixed;
        std::cout << std::setprecision(5) << f << '\n';
        std::cout << std::setprecision(9) << f << '\n';
    }

    {
        std::cout << std::setw(10);
        std::cout << 77 << std::endl;
    }

    {
        long double price;
        std::cout << "Please, enter the price: ";
        std::cin >> std::get_money(price);

        if (std::cin.fail()) std::cout << "Error reading price\n";
        else std::cout << "The price entered is: " << price << '\n';
    }

    {
        std::cout << "Price:" << std::put_money(10.50L) << '\n';
    }

    {
        struct std::tm when;
        std::cout << "Please, enter the time: ";
        std::cin >> std::get_time(&when,"%R");

        if (std::cin.fail()) std::cout << "Error reading time\n";
        else {
            std::cout << "The time entered is: ";
            std::cout << when.tm_hour << " hours and " << when.tm_min << " minutes\n";
        }
    }

    {
        using std::chrono::system_clock;
        std::time_t tt = system_clock::to_time_t (system_clock::now());

        struct std::tm * ptm = std::localtime(&tt);
        std::cout << "Now (local time): " << std::put_time(ptm,"%c") << '\n';

    }

    return 0;
}