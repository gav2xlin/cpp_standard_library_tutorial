cmake_minimum_required(VERSION 3.23)
project(ios)

set(CMAKE_CXX_STANDARD 14)

add_executable(ios main.cpp)
