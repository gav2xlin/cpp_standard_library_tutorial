#include <iostream>
#include <sstream>
#include <fstream>
#include <locale>

void print_state (const std::ios& stream) {
    std::cout << " good()=" << stream.good();
    std::cout << " eof()=" << stream.eof();
    std::cout << " fail()=" << stream.fail();
    std::cout << " bad()=" << stream.bad();
}

std::ostream& Counter (std::ostream& os) {
    const static int index = os.xalloc();
    return os << ++os.iword(index);
}

const int name_index = std::ios::xalloc();

void SetStreamName (std::ios& stream, const char* name) {
    stream.pword(name_index) = const_cast<char*>(name);
}

std::ostream& StreamName (std::ostream& os) {
    const char* name = static_cast<const char*>(os.pword(name_index));
    if (name) os << name;
    else os << "(unknown)";
    return os;
}

void testfn (std::ios::event ev, std::ios_base& stream, int index) {
    /*switch (ev) {
        case stream.copyfmt_event:
            std::cout << "copyfmt_event\n"; break;
        case stream.imbue_event:
            std::cout << "imbue_event\n"; break;
        case stream.erase_event:
            std::cout << "erase_event\n"; break;
    }*/
}

int main() {
    {
        std::cout.setf (std::ios_base::hex , std::ios_base::basefield);
        std::cout.setf (std::ios_base::showbase);
        std::cout << 100 << '\n';

        std::cout.setf (std::ios::hex , std::ios::basefield);
        std::cout.setf (std::ios::showbase);
        std::cout << 100 << '\n';

        std::cout.setf (std::cout.hex , std::cout.basefield);
        std::cout.setf (std::cout.showbase);
        std::cout << 100 << '\n';

        std::ios_base::fmtflags ff;
        ff = std::cout.flags();
        ff &= ~std::cout.basefield;
        ff |= std::cout.hex;
        ff |= std::cout.showbase;
        std::cout.flags(ff);
        std::cout << 100 << '\n';

        std::cout << std::hex << std::showbase << 100 << '\n';
    }

    {
        std::stringstream stream;

        stream.clear (stream.goodbit);
        std::cout << "goodbit:"; print_state(stream); std::cout << '\n';

        stream.clear (stream.eofbit);
        std::cout << " eofbit:"; print_state(stream); std::cout << '\n';

        stream.clear (stream.failbit);
        std::cout << "failbit:"; print_state(stream); std::cout << '\n';

        stream.clear (stream.badbit);
        std::cout << " badbit:"; print_state(stream); std::cout << '\n';
    }

    {
        std::ifstream is("example.txt");

        char c;
        while (is.get(c))
            std::cout << c;

        if (is.eof())
            std::cout << "[EoF reached]\n";
        else
            std::cout << "[error reading]\n";

        is.close();
    }

    {
        std::ifstream is;
        is.open ("test.txt");
        if (!is)
            std::cerr << "Error opening 'test.txt'\n";
    }

    {
        std::ifstream is;
        is.open ("test.txt");
        if ( (is.rdstate() & std::ifstream::failbit ) != 0 )
            std::cerr << "Error opening 'test.txt'\n";
    }

    {
        char buffer [80];
        std::fstream myfile;

        myfile.open ("test.txt",std::fstream::in);

        myfile << "test";
        if (myfile.fail()) {
            std::cout << "Error writing to test.txt\n";
            myfile.clear();
        }

        myfile.getline (buffer,80);
        std::cout << buffer << " successfully read from file.\n";
    }

    {
        std::ofstream filestr;
        filestr.open ("test.txt");

        std::cout.fill ('*');
        std::cout.width (10);
        filestr.copyfmt (std::cout);

        std::cout << 40;
        filestr << 40;
    }

    {
        char prev;

        std::cout.width (10);
        std::cout << 40 << '\n';

        prev = std::cout.fill ('x');
        std::cout.width (10);
        std::cout << 40 << '\n';

        std::cout.fill(prev);
    }

    {
        std::ifstream file;
        file.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
        try {
            file.open ("test.txt");
            while (!file.eof()) file.get();
            file.close();
        }
        catch (std::ifstream::failure e) {
            std::cerr << "Exception opening/reading/closing file\n";
        }
    }

    {
        std::locale mylocale("");
        std::cout.imbue(mylocale);
        std::cout << 3.14159 << '\n';
    }

    {
        std::ostream *prevstr;
        std::ofstream ofs;
        ofs.open ("test.txt");

        std::cout << "tie example:\n";

        *std::cin.tie() << "This is inserted into cout";
        prevstr = std::cin.tie (&ofs);
        *std::cin.tie() << "This is inserted into the file";
        std::cin.tie (prevstr);

        ofs.close();
    }

    {
        std::streambuf *psbuf, *backup;
        std::ofstream filestr;
        filestr.open ("test.txt");

        backup = std::cout.rdbuf();

        psbuf = filestr.rdbuf();
        std::cout.rdbuf(psbuf);

        std::cout << "This is written to the file";

        std::cout.rdbuf(backup);

        filestr.close();
    }

    {
        std::cout << Counter << ": first line\n";
        std::cout << Counter << ": second line\n";
        std::cout << Counter << ": third line\n";

        std::cerr << Counter << ": error line\n";
    }

    {
        SetStreamName(std::cout, "standard output stream");
        SetStreamName(std::cerr, "standard error stream");
        std::cout << StreamName << '\n';
        std::cerr << StreamName << '\n';
        std::clog << StreamName << '\n';
    }

    {
        std::ofstream filestr;
        filestr.register_callback (testfn,0);
        filestr.imbue (std::cout.getloc());
    }

    {
        std::cin.exceptions (std::ios::failbit|std::ios::badbit);
        try {
            std::cin.rdbuf(nullptr);
        } catch (std::ios::failure& e) {
            std::cerr << "failure caught: ";
            if ( e.code() == std::make_error_condition(std::io_errc::stream) )
                std::cerr << "stream error condition\n";
            else
                std::cerr << "some other error condition\n";
        }
    }

    {
        bool b = true;
        std::cout << std::boolalpha << b << '\n';
        std::cout << std::noboolalpha << b << '\n';
    }

    {
        int n = 20;
        std::cout << std::hex << std::showbase << n << '\n';
        std::cout << std::hex << std::noshowbase << n << '\n';
    }

    {
        double a = 30;
        double b = 10000.0;
        double pi = 3.1416;
        std::cout.precision (5);
        std::cout <<   std::showpoint << a << '\t' << b << '\t' << pi << '\n';
        std::cout << std::noshowpoint << a << '\t' << b << '\t' << pi << '\n';
    }

    {
        int p = 1;
        int z = 0;
        int n = -1;
        std::cout << std::showpos   << p << '\t' << z << '\t' << n << '\n';
        std::cout << std::noshowpos << p << '\t' << z << '\t' << n << '\n';
    }

    {
        char a, b, c;

        std::istringstream iss ("  123");
        iss >> std::skipws >> a >> b >> c;
        std::cout << a << b << c << '\n';

        iss.seekg(0);
        iss >> std::noskipws >> a >> b >> c;
        std::cout << a << b << c << '\n';
    }

    {
        std::ofstream outfile ("test.txt");
        outfile << std::unitbuf <<  "Test " << "file" << '\n';
        outfile.close();
    }

    {
        std::cout << std::showbase << std::hex;
        std::cout << std::uppercase << 77 << '\n';
        std::cout << std::nouppercase << 77 << '\n';
    }

    {
        int n = 70;
        std::cout << std::dec << n << '\n';
        std::cout << std::hex << n << '\n';
        std::cout << std::oct << n << '\n';
    }

    {
        double a = 3.1415926534;
        double b = 2006.0;
        double c = 1.0e-10;

        std::cout.precision(5);

        std::cout << "default:\n";
        std::cout << a << '\n' << b << '\n' << c << '\n';

        std::cout << '\n';

        std::cout << "fixed:\n" << std::fixed;
        std::cout << a << '\n' << b << '\n' << c << '\n';

        std::cout << '\n';

        std::cout << "scientific:\n" << std::scientific;
        std::cout << a << '\n' << b << '\n' << c << '\n';
    }

    {
        int n = -77;
        std::cout.width(6); std::cout << std::internal << n << '\n';
        std::cout.width(6); std::cout << std::left << n << '\n';
        std::cout.width(6); std::cout << std::right << n << '\n';
    }

    return 0;
}
