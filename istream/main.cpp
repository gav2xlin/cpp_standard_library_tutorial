#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <sstream>
#include <locale>

struct Phone {
    std::string digits;
};

std::istream& operator>>(std::istream& is, Phone& tel) {
    std::istream::sentry s(is);
    if (s) while (is.good()) {
            char c = is.get();
            if (std::isspace(c,is.getloc())) break;
            if (std::isdigit(c,is.getloc())) tel.digits+=c;
        }
    return is;
}

int main() {
    {
        int n;

        std::cout << "Enter a number: ";
        std::cin >> n;
        std::cout << "You have entered: " << n << '\n';

        std::cout << "Enter a hexadecimal number: ";
        std::cin >> std::hex >> n;
        std::cout << "Its decimal equivalent is: " << n << '\n';
    }

    {
        char str[20];

        std::cout << "Please, enter a word: ";
        std::cin.getline(str,20);
        std::cout << std::cin.gcount() << " characters read: " << str << '\n';
    }

    {
        char str[256];

        std::cout << "Enter the name of an existing text file: ";
        std::cin.get (str,256);

        std::ifstream is(str);

        char c;
        while (is.get(c))
            std::cout << c;

        is.close();
    }

    {
        char name[256], title[256];

        std::cout << "Please, enter your name: ";
        std::cin.getline (name,256);

        std::cout << "Please, enter your favourite movie: ";
        std::cin.getline (title,256);

        std::cout << name << "'s favourite movie is " << title;
    }

    {
        char first, last;

        std::cout << "Please, enter your first name followed by your surname: ";

        first = std::cin.get();
        std::cin.ignore(256,' ');

        last = std::cin.get();

        std::cout << "Your initials are " << first << last << '\n';
    }

    {
        std::cout << "Please, enter a number or a word: ";
        std::cout.flush();

        std::cin >> std::ws;
        std::istream::int_type c;
        c = std::cin.peek();

        if ( c == std::char_traits<char>::eof() )
            return 1;
        if ( std::isdigit(c) ) {
            int n;
            std::cin >> n;
            std::cout << "You entered the number: " << n << '\n';
        } else {
            std::string str;
            std::cin >> str;
            std::cout << "You entered the word: " << str << '\n';
        }
    }

    {
        std::ifstream is ("test.txt", std::ifstream::binary);
        if (is) {

            is.seekg (0, is.end);
            int length = is.tellg();
            is.seekg (0, is.beg);

            char * buffer = new char [length];

            std::cout << "Reading " << length << " characters... ";

            is.read (buffer,length);

            if (is)
                std::cout << "all characters read successfully.";
            else
                std::cout << "error: only " << is.gcount() << " could be read";
            is.close();

            delete[] buffer;
        }
    }

    {
        std::cout << "Please, enter a number or a word: ";
        char c = std::cin.get();

        if ( (c >= '0') && (c <= '9') ) {
            int n;
            std::cin.putback (c);
            std::cin >> n;
            std::cout << "You entered a number: " << n << '\n';
        } else {
            std::string str;
            std::cin.putback (c);
            getline (std::cin,str);
            std::cout << "You entered a word: " << str << '\n';
        }
    }

    {
        std::cout << "Please, enter a number or a word: ";
        char c = std::cin.get();

        if ( (c >= '0') && (c <= '9') ) {
            int n;
            std::cin.unget();
            std::cin >> n;
            std::cout << "You entered a number: " << n << '\n';
        } else {
            std::string str;
            std::cin.unget();
            getline (std::cin,str);
            std::cout << "You entered a word: " << str << '\n';
        }
    }

    {
        std::ifstream is ("test.txt", std::ifstream::binary);
        if (is) {

            is.seekg (0, is.end);
            int length = is.tellg();
            is.seekg (0, is.beg);

            char * buffer = new char [length];

            is.read (buffer,length);
            is.close();

            std::cout.write (buffer,length);

            delete[] buffer;
        }
    }

    {
        std::ifstream is ("test.txt", std::ifstream::binary);
        if (is) {
            is.seekg (0, is.end);
            int length = is.tellg();
            is.seekg (0, is.beg);

            char * buffer = new char [length];

            is.read (buffer,length);
            is.close();

            std::cout.write (buffer,length);

            delete[] buffer;
        }
    }

    {
        char first, second;

        std::cout << "Please, enter a word: ";
        first = std::cin.get();
        std::cin.sync();

        std::cout << "Please, enter another word: ";
        second = std::cin.get();

        std::cout << "The first word began by " << first << '\n';
        std::cout << "The second word began by " << second << '\n';
    }

    {
        std::stringstream parseme ("   (555)2326");
        Phone myphone;
        parseme >> myphone;
        std::cout << "digits parsed: " << myphone.digits << '\n';
    }

    {
        char a[10], b[10];

        std::istringstream iss ("one \n \t two");
        iss >> std::noskipws;
        iss >> a >> std::ws >> b;
        std::cout << a << ", " << b << '\n';
    }

    return 0;
}
