#include <iostream>
#include <iterator>
#include <list>
#include <algorithm>
#include <vector>
#include <typeinfo>
#include <string>

int main () {
    {
        std::list<int> mylist;
        for (int i = 0; i < 10; i++) mylist.push_back(i * 10);

        auto it = mylist.begin();

        std::advance(it, 9);

        std::cout << "The 9th element in mylist is: " << *it << '\n';
    }

    {
        std::list<int> mylist;
        for (int i = 0; i < 5; i++) mylist.push_back (i*1);

        std::list<int>::iterator first = mylist.begin();
        std::list<int>::iterator last = mylist.end();

        std::cout << "The distance between first and last is: " << std::distance(first,last) << '\n';
    }

    {
        int foo[] = {1,2,3,4,5};
        std::vector<int> bar;

        for (auto it = std::begin(foo); it!=std::end(foo); ++it)
            bar.push_back(*it);

        std::cout << "bar contains:";
        for (auto it = std::begin(bar); it!=std::end(bar); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::list<int> mylist;
        for (int i = 0; i < 10; i++) mylist.push_back (i*1);

        std::cout << "The last element is " << *std::prev(mylist.begin()) << '\n';
    }

    {
        std::list<int> mylist;
        for (int i = 0; i < 10; i++) mylist.push_back (i*1);

        std::cout << "mylist:";
        std::for_each (mylist.begin(),
                       std::next(mylist.begin(),4),
                       [](int x) {std::cout << ' ' << x;} );

        std::cout << '\n';
    }

    {
        std::vector<int> foo,bar;
        for (int i = 1; i <= 3; i++) {
            foo.push_back(i); bar.push_back(i*1);
        }

        std::copy (bar.begin(),bar.end(),back_inserter(foo));

        std::cout << "foo contains:";
        for (int & it : foo)
            std::cout << ' ' << it;
        std::cout << '\n';
    }

    {
        std::list<int> foo,bar;
        for (int i = 1; i <= 5; i++) {
            foo.push_back(i); bar.push_back(i*5);
        }

        std::list<int>::iterator it = foo.begin();
        advance (it,3);

        std::copy (bar.begin(),bar.end(),std::inserter(foo,it));

        std::cout << "foo contains:";
        for ( std::list<int>::iterator it = foo.begin(); it!= foo.end(); ++it )
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::vector<std::string> foo (3);
        std::vector<std::string> bar {"tutorialspont","com","india"};

        std::copy ( make_move_iterator(bar.begin()),
                    make_move_iterator(bar.end()),
                    foo.begin() );

        // bar now contains unspecified values; clear it:
        bar.clear();

        std::cout << "foo:";
        for (std::string& x : foo) std::cout << ' ' << x;
        std::cout << '\n';
    }

    {
        class MyIterator : public std::iterator<std::input_iterator_tag, int> {
            int* p;
        public:
            MyIterator(int* x) :p(x) {}
            MyIterator(const MyIterator& mit) : p(mit.p) {}
            MyIterator& operator++() {++p;return *this;}
            MyIterator operator++(int) {MyIterator tmp(*this); operator++(); return tmp;}
            bool operator==(const MyIterator& rhs) {return p==rhs.p;}
            bool operator!=(const MyIterator& rhs) {return p!=rhs.p;}
            int& operator*() {return *p;}
        };

        int numbers[] = {1,2,3,4,5};
        MyIterator from(numbers);
        MyIterator until(numbers+5);
        for (MyIterator it = from; it!=until; it++)
            std::cout << *it << ' ';
        std::cout << '\n';
    }

    {
        typedef std::iterator_traits<int*> traits;
        if (typeid(traits::iterator_category) == typeid(std::random_access_iterator_tag))
            std::cout << "int* is a random-access iterator";
    }

    {
        std::string s = "tutorialspoint";
        std::reverse_iterator<std::string::iterator> r = s.rbegin();
        r[10] = 'O'; // replaces 'o' with 'O'
        r += 10; // iterator now points at 'O'
        std::string rev(r, s.rend());
        std::cout << rev << '\n';
    }

    {
        std::vector<std::string> foo (3);
        std::vector<std::string> bar {"sai","ram","krishna"};

        typedef std::vector<std::string>::iterator Iter;

        std::copy ( std::move_iterator<Iter>(bar.begin()),
                    std::move_iterator<Iter>(bar.end()),
                    foo.begin() );

        bar.clear();

        std::cout << "foo:";
        for (std::string& x : foo) std::cout << ' ' << x;
        std::cout << '\n';
    }

    {
        std::vector<int> foo, bar;
        for (int i = 10; i >= 5; i--) {
            foo.push_back(i); bar.push_back(i*10);
        }

        std::back_insert_iterator< std::vector<int> > back_it (foo);

        std::copy (bar.begin(),bar.end(),back_it);

        std::cout << "foo:";
        for ( std::vector<int>::iterator it = foo.begin(); it!= foo.end(); ++it )
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::deque<int> foo, bar;
        for (int i = 10; i >= 5; i--) {
            foo.push_back(i); bar.push_back(i*10);
        }

        std::front_insert_iterator< std::deque<int> > front_it (foo);

        std::copy (bar.begin(),bar.end(),front_it);

        std::cout << "foo:";
        for ( std::deque<int>::iterator it = foo.begin(); it!= foo.end(); ++it )
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::list<int> foo, bar;
        for (int i = 10; i >= 5; i--) {
            foo.push_back(i); bar.push_back(i*10);
        }

        std::list<int>::iterator it = foo.begin();
        advance(it,3);

        std::insert_iterator< std::list<int> > insert_it (foo,it);

        std::copy (bar.begin(),bar.end(),insert_it);

        std::cout << "foo:";
        for ( std::list<int>::iterator it = foo.begin(); it!= foo.end(); ++it )
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        double value1, value2;
        std::cout << "Please insert values: ";

        std::istream_iterator<double> eos;
        std::istream_iterator<double> iit (std::cin);

        if (iit!=eos) value1=*iit;

        ++iit;
        if (iit!=eos) value2=*iit;

        std::cout << value1 << "*" << value2 << "=" << (value1*value2) << '\n';
    }

    {
        std::vector<int> myvector;
        for (int i = 10; i > 1; i--) myvector.push_back(i*10);

        std::ostream_iterator<int> out_it (std::cout,", ");
        std::copy ( myvector.begin(), myvector.end(), out_it );
    }

    {
        std::istringstream in("Hello, world");
        std::vector<char> v( (std::istreambuf_iterator<char>(in)),
                             std::istreambuf_iterator<char>() );
        std::cout << "v has " << v.size() << " bytes. ";
        v.push_back('\0');
        std::cout << "it holds \"" << &v[0] << "\"\n";

        std::istringstream s("abc");
        std::istreambuf_iterator<char> i1(s), i2(s);
        std::cout << "i1 returns " << *i1 << '\n'
                  << "i2 returns " << *i2 << '\n';
        ++i1;
        std::cout << "after incrementing i1, but not i2\n"
                  << "i1 returns " << *i1 << '\n'
                  << "i2 returns " << *i2 << '\n';
        ++i2;
        std::cout << "after incrementing i2, but not i1\n"
                  << "i1 returns " << *i1 << '\n'
                  << "i2 returns " << *i2 << '\n';
    }

    {
        std::string mystring ("Tutorailspoint.com");
        std::ostreambuf_iterator<char> out_it (std::cout); // stdout iterator

        std::copy ( mystring.begin(), mystring.end(), out_it);
    }

    return 0;
}