#include <iostream>
#include <list>
#include <array>
#include <stdexcept>
#include <utility>

using namespace std;

bool pred(int a, int b) {
    return (abs(a) == abs(b));
}

bool cmp_func(int a, int b) {
    return a > b;
}

bool foo(int n) {
    return n > 5;
}

int main(void) {
    {
        list<int> l;

        cout << "Size of list = " << l.size() << endl;
    }

    {
        array<int, 5> arr = {10, 20, 30, 40, 50};
        size_t i;

        /* print array contents */
        for (i = 0; i < 5; ++i)
            cout << arr.at(i) << " ";
        cout << endl;

        /* generate out_of_range exception. */
        try {
            arr.at(10);
        } catch(out_of_range e) {
            cout << "out_of_range expcepiton caught for " << e.what() << endl;
        }
    }

    {
        list<int> l(5);

        cout << "List contains following element" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4, 5};
        list<int> l2(l1.begin(), l1.end());

        cout << "List contains following element" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4, 5};
        list<int> l2(move(l1));

        cout << "List contains following element" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        list<int> l(it);

        cout << "List contains following element" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4, 5};
        list<int> l2;

        l2.assign(l1.begin(), l1.end());

        cout << "List contains following element" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        l.assign(5, 10);

        cout << "List contains following element" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        list<int> l;

        l.assign(it);

        cout << "List contains following element" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "Last element of the list is = " << l.back() << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "List contains following elements" << endl;

        for (auto it = l.cbegin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "Initial size of list = " << l.size() << endl;

        l.clear();

        cout << "Size of list after clear operation = " << l.size() << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "List contains following element in reverse order" << endl;

        for (auto it = l.crbegin(); it != l.crend(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {3, 4, 5};

        auto it = l.emplace(l.begin(), 2);

        l.emplace(it, 1);

        cout << "List contains following element" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4};

        l.emplace_back(5);

        cout << "List contains following element" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {2, 3, 4, 5};

        l.emplace_front(1);

        cout << "List contains following element" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        if (l.empty())
            cout << "List is empty." << endl;

        l.emplace_back(1);

        if (!l.empty())
            cout << "List is not empty." << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "Size of list befor erase operation = " << l.size() << endl;

        l.erase(l.begin());

        cout << "Size of list after erase operation = " << l.size() << endl;

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "Size of list befor erase operation = " << l.size() << endl;

        l.erase(l.begin(), l.end());

        cout << "Size of list after erase operation = " << l.size() << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "First element of the list = " << l.front() << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};
        int *p = NULL;

        p = l.get_allocator().allocate(5);

        for (int i = 0; i < 5; ++i)
            p[i] = i + 1;

        cout << "List contains following elements" << endl;

        for (int i = 0; i < 5; ++i)
            cout << p[i] << endl;
    }

    {
        list<int> l;

        for (int i = 0; i < 5; ++i)
            l.insert(l.end(), i + 1);

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        l.insert(l.begin(), 5, 5);

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {3, 4, 5};
        list<int> l2 = {1, 2};

        l1.insert(l1.begin(), l2.begin(), l2.end());

        cout << "List contains following elements" << endl;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4, 5};
        list<int> l2;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            l2.insert(l2.end(), move(*it));

        cout << "List l2 contains following elements" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        list<int> l;

        l.insert(l.begin(), it);

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        cout << "max_size of list = " << l.max_size() << endl;
    }

    {
        list<int> l1 = {1, 5, 11, 31};
        list<int> l2 = {10, 20, 30};

        l2.merge(l1);

        cout << "List contains following elements after merge operation" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {31, 11, 5, 1};
        list<int> l2 = {30, 20, 10};

        l2.merge(l1, cmp_func);

        cout << "List contains following elements after merge operation" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 5, 11, 31};
        list<int> l2 = {10, 20, 30};

        l2.merge(move(l1));

        cout << "List contains following elements after merge operation" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {31, 11, 5, 1};
        list<int> l2 = {30, 20, 10};

        l2.merge(move(l1), cmp_func);

        cout << "List contains following elements after merge operation" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4, 5};
        list<int> l2;

        l2 = l1;

        cout << "List contains following elements" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4, 5};
        list<int> l2;

        l2 = move(l1);

        cout << "List contains following elements" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        list<int> l;

        l = it;

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "List contains following elements before pop_back operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;

        l.pop_back();

        cout << "List contains following elements after pop_back operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "List contains following elements before pop_back operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;

        l.pop_front();

        cout << "List contains following elements after pop_back operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        for (int i = 0; i < 5; ++i)
            l.push_back(i + 1);

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4, 5};
        list<int> l2;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            l2.push_back(move(*it));

        cout << "List l2 contains following elements" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        for (int i = 0; i < 5; ++i)
            l.push_front(i + 1);

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4, 5};
        list<int> l2;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            l2.push_front(move(*it));

        cout << "List l2 contains following elements" << endl;

        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "List contains following elements in reverse order" << endl;

        for (auto it = l.rbegin(); it != l.rend(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {3, 1, 2, 3, 3, 4, 5, 3};

        cout << "List contents before remove opration" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;

        l.remove(3);

        cout << "List contents after remove opration" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        cout << "Contents of list before remove_if operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;

        /* remove all elements larger than 5 */
        l.remove_if(foo);

        cout << "Contents of list after remove_if operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        cout << "List contains following elements in reverse order" << endl;

        for (auto it = l.rbegin(); it != l.rend(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        cout << "Initial size of list = " << l.size() << endl;

        l.resize(5);

        cout << "Size of list after resize operation = " << l.size() << endl;

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        cout << "Initial size of list = " << l.size() << endl;

        l.resize(5, 10);

        cout << "Size of list after resize operation = " << l.size() << endl;

        cout << "List contains following elements" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 2, 3, 4, 5};

        l.reverse();

        cout << "List contains following elements after reverse operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l;

        cout << "Initial size of list = " << l.size() << endl;

        l.resize(5);

        cout << "size of list after resize operation = " << l.size() << endl;
    }

    {
        list<int> l = {1, 4, 2, 5, 3};

        cout << "Contents of list before sort operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;

        l.sort();

        cout << "Contents of list after sort operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l = {1, 4, 2, 5, 3};

        cout << "Contents of list before sort operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;

        /* Descending sort */
        l.sort(cmp_func);

        cout << "Contents of list after sort operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {4, 5};

        l1.splice(l1.end(), l2);

        cout << "Contents of list l1 after splice operation" << endl;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {4, 5};

        l1.splice(l1.end(), l2, l2.begin());

        cout << "Contents of list l1 after splice operation" << endl;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {4, 5};

        l1.splice(l1.end(), move(l2));

        cout << "Contents of list l1 after splice operation" << endl;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2};
        list<int> l2 = {3, 4, 5};

        l1.splice(l1.end(), l2, l2.begin(), l2.end());

        cout << "Contents of list l1 after splice operation" << endl;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {4, 5};

        l1.splice(l1.end(), move(l2), l2.begin());

        cout << "Contents of list l1 after splice operation" << endl;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2};
        list<int> l2 = {3, 4, 5};

        l1.splice(l1.end(), move(l2), l2.begin(), l2.end());

        cout << "Contents of list l1 after splice operation" << endl;

        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {10, 20, 30, 40, 50};

        cout << "List l1 contains following elements before swap operation" << endl;
        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;

        cout << "List l2 contains following elements before swap operation" << endl;
        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;

        l1.swap(l2);

        cout << "List l1 contains following elements after swap operation" << endl;
        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;

        cout << "List l2 contains following elements after swap operation" << endl;
        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    {
        list <int> l = {1, 1, 2, 2, 3, 4, 5, 5};

        cout << "List elements before unique operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;

        l.unique();

        cout << "List elements after unique operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list <int> l = {1, -1, -1, -1, 2, 2, -2, -2, 3, -4, 4, -5, -5, 5};

        cout << "List elements before unique operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;

        /* Ignore sign of the value */
        l.unique(pred);

        cout << "List elements after unique operation" << endl;

        for (auto it = l.begin(); it != l.end(); ++it)
            cout << *it << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {1, 2, 3};

        if (l1 == l2)
            cout << "List l1 and l2 are equal" << endl;

        l1.push_back(4);

        if (!(l1 == l2))
            cout << "List l1 and l2 are not equal" << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {1, 2, 3, 4};

        if (l1 != l2)
            cout << "List l1 and l2 are not equal" << endl;

        l2.pop_back();

        if (!(l1 != l2))
            cout << "List l1 and l2 are equal" << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {1, 2, 3, 4};

        if (l1 < l2)
            cout << "List l1 is less that l2" << endl;

        l2.pop_back();

        if (!(l1 < l2))
            cout << "List l1 is not less that l2" << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {1, 2, 3};

        if (l1 <= l2)
            cout << "List l1 is less that or equal to l2" << endl;

        l2.pop_back();

        if (!(l1 <= l2))
            cout << "List l1 is not less that or equal to l2" << endl;
    }

    {
        list<int> l1 = {1, 2, 3, 4};
        list<int> l2 = {1, 2, 3};

        if (l1 > l2)
            cout << "List l1 is greater that l2" << endl;

        l1.pop_back();

        if (!(l1 > l2))
            cout << "List l1 is not greater that l2" << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {1, 2, 3};

        if (l1 >= l2)
            cout << "List l1 is greater that or equal to l2" << endl;

        l1.pop_back();

        if (!(l1 >= l2))
            cout << "List l1 is not greater that or equal to l2" << endl;
    }

    {
        list<int> l1 = {1, 2, 3};
        list<int> l2 = {10, 20, 30, 40, 50};

        cout << "List l1 contains following elements before swap operation" << endl;
        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;

        cout << "List l2 contains following elements before swap operation" << endl;
        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;

        swap(l1, l2);

        cout << "List l1 contains following elements after swap operation" << endl;
        for (auto it = l1.begin(); it != l1.end(); ++it)
            cout << *it << endl;

        cout << "List l2 contains following elements after swap operation" << endl;
        for (auto it = l2.begin(); it != l2.end(); ++it)
            cout << *it << endl;
    }

    return 0;
}