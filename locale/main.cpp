#include <cstdio>
#include <cctype>
#include <cstdlib>
#include <locale>
#include <string>
#include <iostream>
#include <cwchar>
#include <cstddef>

using namespace std;

int main () {
    {
        char c;
        int i = 0;
        char str[] = "tutorials point india pvt ltd\n";
        while (str[i]) {
            c = str[i];
            if (isspace(c)) c = '\n';
            putchar(c);
            i++;
        }
    }

    {
        int i=0;
        char str[]="Tutorialspoint.com \n sairamkrishna Mammahe \n";
        while (isprint(str[i])) {
            putchar (str[i]);
            i++;
        }
    }

    {
        int i=0;
        char str[]="tutorialspoint.com \n sairamkrishna  \n";
        while (!iscntrl(str[i])) {
            putchar (str[i]);
            i++;
        }
    }

    {
        int i=0;
        char str[]="Tutorialspoint india pvt ltd.\n";
        char c;
        while (str[i]) {
            c=str[i];
            if (isupper(c)) c=tolower(c);
            putchar (c);
            i++;
        }
    }

    {
        int i=0;
        char str[]="Tutorials point india pvt ltd.\n";
        char c;
        while (str[i]) {
            c=str[i];
            if (islower(c)) c=toupper(c);
            putchar (c);
            i++;
        }
    }

    {
        int i=0;
        char str[]="C++";
        while (str[i]) {
            if (isalpha(str[i])) printf ("character %c is alphabetic\n",str[i]);
            else printf ("character %c is not alphabetic\n",str[i]);
            i++;
        }
    }

    {
        char str[]="2016ad";
        int year;
        if (isdigit(str[0])) {
            year = atoi (str);
            printf ("The year that followed %d was %d.\n",year,year+1);
        }
    }

    {
        int i=0;
        int cx=0;
        char str[]="tutorialspoint india pvt ltd!";
        while (str[i]) {
            if (ispunct(str[i])) cx++;
            i++;
        }
        printf ("Sentence contains %d punctuation characters.\n", cx);
    }

    {
        char str[]="ffff";
        long int number;
        if (isxdigit(str[0])) {
            number = strtol (str,NULL,16);
            printf ("The hexadecimal number %lx is %ld.\n",number,number);
        }
    }

    {
        int i;
        char str[]="sairamkrishna1234566789.";
        i=0;
        while (isalnum(str[i])) i++;
        printf ("The first %d characters are alphanumeric.\n",i);
    }

    {
        FILE * pFile;
        int c;
        pFile=fopen ("myfile.txt","r");
        if (pFile) {
            do {
                c = fgetc (pFile);
                if (isgraph(c)) putchar (c);
            } while (c != EOF);
            fclose (pFile);
        }
    }

    {
        std::locale foo;
        foo.global(std::locale(""));
        std::locale bar;

        std::cout << "bar and foo both are ";
        std::cout << (foo==bar?"the same":"different");
        std::cout << ".\n";
    }

    {
        std::locale loc;
        const char quote[] = "Tutorialspoint is a one of the best site .";

        std::cout << '"' << quote << "\"\n";

        std::cout << "The quote begins with an uppercase letter? ";
        std::cout << std::boolalpha;
        std::cout << std::use_facet< std::ctype<char> >(loc).is (std::ctype
                                                                         <char>::upper, quote[0]);
        std::cout << '\n';

        int cx = 0;
        std::ctype<char>::mask * masks = new std::ctype<char>::mask [60];
        std::use_facet< std::ctype<char> >(loc).is (quote, quote+60, masks);
        for (int i=0; i<60; ++i) if (masks[i] & std::ctype<char>::space) ++cx;
        std::cout << "The quote has " << cx << " whitespaces.\n";
    }

    {
        std::locale loc;

        const char quote[] = "tutorialspoint. sairamkrishna, He had developed this tutorial.";

        const char * p = std::use_facet< std::ctype<char> >(loc).scan_is
                ( std::ctype<char>::punct, quote, quote+76 );

        std::cout << "The second sentence is: " << p << '\n';
    }

    {
        std::locale loc;

        const char period[] = "june2018";

        const char * p = std::use_facet< std::ctype<char> >(loc).scan_not
                ( std::ctype<char>::alpha, period, period+12 );

        std::cout << "The first non-alphabetic character is: " << *p << '\n';
    }

    {
        std::locale loc;

        char site[] = "Tutorialspoint.com";

        std::cout << "The first letter of " << site << " as an uppercase is: ";
        std::cout << std::use_facet< std::ctype<char> >(loc).toupper(*site);
        std::cout << '\n';

        std::cout << "The result of converting " << site << " to uppercase is: ";
        std::use_facet< std::ctype<char> >(loc).toupper ( site, site+sizeof(site) );
        std::cout << site << '\n';
    }

    {
        std::locale loc;

        char site[] = "Tutorialspoint.com ";

        std::cout << "The first letter of " << site << " as a lowercase is: ";
        std::cout << std::use_facet< std::ctype<char> >(loc).tolower ( *site );
        std::cout << '\n';

        std::cout << "The result of converting " << site << " to lowercase is: ";
        std::use_facet< std::ctype<char> >(loc).tolower ( site, site+sizeof(site) );
        std::cout << site << '\n';
    }

    {
        std::locale loc;

        const char narrow_phrase[] = "Sairamkrishna Mammahe";
        wchar_t wide_phrase[sizeof(narrow_phrase)];

        std::wcout << L"The first wide character is: ";
        wchar_t wc = std::use_facet< std::ctype<wchar_t> >(loc).widen ( *narrow_phrase );
        std::wcout << wc << std::endl;

        std::wcout << L"The wide-character phrase is: ";
        std::use_facet< std::ctype<wchar_t> >(loc).widen (narrow_phrase,
                                                          narrow_phrase+sizeof(narrow_phrase),
                                                          wide_phrase);
        std::wcout << wide_phrase << std::endl;
    }

    {
        std::locale loc;
        std::wstring yourname;

        std::cout << "Please enter your a word: ";
        std::getline (std::wcin,yourname);
        std::wstring::size_type length = yourname.length();

        std::cout << "The first (narrow) character in your word is: ";
        std::cout << std::use_facet< std::ctype<wchar_t> >(loc).narrow ( yourname[0], '?' );
        std::cout << '\n';

        std::cout << "The narrow transformation of your word is: ";
        char * pc = new char [length+1];
        std::use_facet< std::ctype<wchar_t> >(loc).narrow ( yourname.c_str(),
                                                            yourname.c_str()+length+1, '?', pc);
        std::cout << pc << '\n';
    }

    {
        std::locale foo;
        foo.global(std::locale(""));
        std::locale bar;

        std::cout << "bar and foo both are ";
        std::cout << (foo==bar?"the same":"different");
        std::cout << ".\n";
    }

    {
        typedef std::codecvt<wchar_t,char,std::mbstate_t> facet_type;

        std::locale mylocale;

        const facet_type& myfacet = std::use_facet<facet_type>(mylocale);

        const char mystr[] = "Tutorialspoint.com";

        wchar_t pwstr[sizeof(mystr)];
        std::mbstate_t mystate = std::mbstate_t();
        const char* pc;
        wchar_t* pwc;

        facet_type::result myresult = myfacet.in (mystate,
                                                  mystr, mystr+sizeof(mystr), pc, pwstr, pwstr+sizeof(mystr), pwc);

        if ( myresult == facet_type::ok ) {
            std::wcout << L"Translation was successful: ";
            std::wcout << pwstr << std::endl;
        }
    }

    {
        typedef std::codecvt<wchar_t,char,std::mbstate_t> facet_type;

        std::locale mylocale;

        const facet_type& myfacet = std::use_facet<facet_type>(mylocale);

        std::wstring mywstring;
        std::cout << "Enter a sentence: ";
        std::getline (std::wcin,mywstring);

        std::wstring::size_type length = mywstring.length();

        char* pstr= new char [length+1];
        std::mbstate_t mystate = std::mbstate_t();
        const wchar_t* pwc;
        char* pc;

        facet_type::result myresult = myfacet.out (mystate,
                                                   mywstring.c_str(), mywstring.c_str()+length+1, pwc, pstr, pstr+length+1, pc);

        if (myresult==facet_type::ok)
            std::cout << "Translation successful and sentance should be: " << pstr << '\n';

        delete[] pstr;
    }

    {
        std::locale loc;

        std::cout << std::boolalpha;

        std::cout << "always_noconv for codecvt<char,char,mbstate_t>: ";
        std::cout << std::use_facet<std::codecvt<char,char,mbstate_t> >(loc).always_noconv();
        std::cout << '\n';

        std::cout << "always_noconv for codecvt<wchar_t,char,mbstate_t>: ";
        std::cout << std::use_facet<std::codecvt<wchar_t,char,mbstate_t> >(loc).always_noconv();
        std::cout << '\n';
    }

    {
        std::locale loc;

        const std::codecvt<wchar_t,char,mbstate_t>& myfacet =
                std::use_facet<std::codecvt<wchar_t,char,mbstate_t> >(loc);

        std::cout << "Characteristics of codecvt<wchar_t,char,mbstate_t>:\n";
        std::cout << "Encoding: " << myfacet.encoding() << '\n';
        std::cout << "Always noconv: " << myfacet.always_noconv() << '\n';
        std::cout << "Max length: " << myfacet.max_length() << '\n';
    }

    {
        typedef std::codecvt<wchar_t,char,std::mbstate_t> facet_type;

        std::locale loc;
        const facet_type& myfacet = std::use_facet<facet_type>(loc);

        const char source[] = "sairamkrishna mammahe";

        std::mbstate_t mystate;
        const char * pc;
        wchar_t * pwc;

        std::size_t length = myfacet.length (mystate, source, source+sizeof(source), 30);

        wchar_t* dest = new wchar_t[length];
        myfacet.in (mystate, source, source+sizeof(source), pc, dest, dest+length, pwc);

        std::wcout << dest << std::endl;

        delete[] dest;
    }

    {
        std::locale loc;

        std::cout << "max_length for codecvt<char,char,mbstate_t>: ";
        std::cout << std::use_facet<std::codecvt<char,char,mbstate_t> >(loc).max_length();
        std::cout << '\n';

        std::cout << "max_length for codecvt<wchar_t,char,mbstate_t>: ";
        std::cout << std::use_facet<std::codecvt<wchar_t,char,mbstate_t> >(loc).max_length();
        std::cout << '\n';
    }

    {
        std::locale foo;
        foo.global(std::locale(""));
        std::locale bar;

        std::cout << "bar and foo both are ";
        std::cout << (foo==bar?"the same":"different");
        std::cout << ".\n";
    }

    {
        std::locale loc("");

        loc = loc.combine< std::num_put<char> > (std::locale::classic());

        std::cout.imbue(loc);
        std::cout << 3.1 << '\n';
    }

    {
        std::locale foo;
        foo.global(std::locale(""));
        std::locale bar;

        std::cout << "foo and bar are ";
        std::cout << (foo==bar?"equal":"different");
        std::cout << ".\n";
    }

    {
        std::locale loc;

        std::cout << "The global locale should be : " << loc.name() << '\n';
    }

    {
        if (std::cout.getloc() != std::locale("C"))
            std::cout << "cout is not using the locale C.\n";
        else
            std::cout << "cout is using The locale C.\n";
    }

    return 0;
}