#include <iostream>
#include <map>

using namespace std;

int main(void) {
    {
        map<char, int> m;

        cout << "Size of map = " << m.size() << endl;
    }

    {
        map<char, int> m1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        map<char, int> m2(m1.begin(), m1.end());

        cout << "Map contains following elements" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 = {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        multimap<char, int> m2(m1);

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 = {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        multimap<char, int> m2(move(m1));

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Map contains following elements" << endl;

        for (auto it = m1.begin(); it != m1.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Value of key m['a'] = " << m.at('a') << endl;

        try {
            m.at('z');
        } catch (const out_of_range &e) {
            cerr << "Exception at " << e.what() << endl;
        }
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements" << endl;

        for (auto it = m.cbegin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Initial size of map = " << m.size() << endl;

        m.clear();

        cout << "Size of map after clear opearation = " << m.size() << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        if (m.count('a') == 1) {
            cout << "m['a'] = " << m.at('a') << endl;
        }

        if (m.count('z') == 0) {
            cout << "Value not present for key m['z']" << endl;
        }
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements in reverse order" << endl;

        for (auto it = m.crbegin(); it != m.crend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m;

        m.emplace('a', 1);
        m.emplace('b', 2);
        m.emplace('c', 3);
        m.emplace('d', 4);
        m.emplace('e', 5);

        cout << "Map contains following elements in reverse order" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        map<char, int> m = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        m.emplace_hint(m.end(), 'e', 5);
        m.emplace_hint(m.begin(), 'a', 1);

        cout << "Map contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m;

        if (m.empty())
            cout << "Map is empty" << endl;

        m.emplace('a', 1);

        if (!m.empty())
            cout << "Map is not empty" << endl;
    }

    {
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        auto ret = m.equal_range('b');

        cout << "Lower bound is " << ret.first->first <<
             " = " << ret.first->second << endl;

        cout << "Upper bound is " << ret.second->first <<
             " = " << ret.second->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements before erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        m.erase(m.begin());

        cout << "Map contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements before erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        auto it = m.erase(m.begin());

        cout << "Map contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        cout << "After erase operation iterator points to = " << it->first << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements before erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        m.erase('a');

        cout << "Map contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements befor erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        auto it = m.begin();
        ++it, ++it;

        m.erase(m.begin(), it);

        cout << "Map contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements befor erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        auto it = m.begin();
        ++it, ++it;

        it = m.erase(m.begin(), it);

        cout << "Map contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        cout << "After erase operation iterator points to = " << it->first << endl;
    }

    {
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        auto it = m.find('c');

        cout << "Iterator points to " << it->first <<
             " = " << it->second << endl;
    }

    {
        map<char, int> m;
        pair<const char, int> *p;

        p = m.get_allocator().allocate(5);

        cout << "Allocated size = " <<  sizeof(*p) * 5 << endl;
    }

    {
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
        };

        m.insert(pair<char, int>('d', 4));
        m.insert(pair<char, int>('e', 5));

        cout << "Map contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        map<char, int> m = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        m.insert(m.begin(), pair<char, int>('a', 1));
        m.insert(m.end(), pair<char, int>('e', 5));

        cout << "Map contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        map<char, int> m2;

        m2.insert(m1.begin(), m1.end());

        cout << "Map contains following elements" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        map<char, int> m = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        m.insert(m.begin(), move(pair<char, int>('a', 1)));
        m.insert(m.end(), move(pair<char, int>('e', 5)));

        cout << "Map contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        map<char, int> m = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        m.insert({{'e', 5}, {'a', 1}});

        cout << "Map contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        auto comp = m.key_comp();
        char last = m.rbegin()->first;
        auto it = m.begin();

        cout << "Map contains following elements" << endl;

        do
            cout << it->first << " = " << it->second << endl;
        while (comp((*it++).first, last));
    }

    {
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        auto it = m.lower_bound('b');

        cout << "Lower bound is " << it->first <<
             " = " << it->second << endl;
    }

    {
        map <char, int> m;

        cout << "max_size for map = " << m.max_size() << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        map<char, int> m2 = m1;

        cout << "Map contains following elements" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        map<char, int> m2 = move(m1);

        cout << "Map contains following elements" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        map<char, int> m;

        m =  {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements" << endl;

        cout << "m['a'] = " << m['a'] << endl;
        cout << "m['b'] = " << m['b'] << endl;
        cout << "m['c'] = " << m['c'] << endl;
        cout << "m['d'] = " << m['d'] << endl;
        cout << "m['e'] = " << m['e'] << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements" << endl;

        cout << "m['a'] = " << move(m['a']) << endl;
        cout << "m['b'] = " << move(m['b']) << endl;
        cout << "m['c'] = " << move(m['c']) << endl;
        cout << "m['d'] = " << move(m['d']) << endl;
        cout << "m['e'] = " << move(m['e']) << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Map contains following elements in reverse order" << endl;

        for (auto it = m.rbegin(); it != m.rend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        map<char, int> m;

        cout << "Initial size of map = " << m.size() << endl;

        m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Size of map after inserting elements = " << m.size() << endl;
    }

    {
        map<char, int> m1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        map<char, int> m2;

        m2.swap(m1);

        cout << "Map contains following elements" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        auto it = m.upper_bound('b');

        cout << "Upper bound is " << it->first <<
             " = " << it->second << endl;
    }

    {
        /* Initializer_list constructor */
        map<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        auto last = *m.rbegin();
        auto it = m.begin();

        cout << "Map contains following elements" << endl;

        do
            cout << it->first << " = " << it->second << endl;
        while (m.value_comp()(*it++, last));
    }

    {
        map<char, int> m1;
        map<char, int> m2;

        if (m1 == m2)
            cout << "Both maps are equal." << endl;

        m1.emplace('a', 1);

        if (!(m1 == m2))
            cout << "Both maps are not equal." << endl;
    }

    {
        map<char, int> m1;
        map<char, int> m2;

        m1.emplace('a', 1);

        if (m1 != m2)
            cout << "Both maps not are equal." << endl;

        m1 = m2;

        if (!(m1 != m2))
            cout << "Both maps are equal." << endl;
    }

    {
        map<char, int> m1;
        map<char, int> m2;

        m2.emplace('a', 1);

        if (m1 < m2)
            cout << "Map m1 is less than m2." << endl;

        m1 = m2;

        if (!(m1 < m2))
            cout << "Map m1 is not less than m2." << endl;
    }

    {
        map<char, int> m1;
        map<char, int> m2;

        m1.emplace('a', 1);
        m2.emplace('a', 1);

        if (m1 <= m2)
            cout << "Map m1 is less than or equal to m2." << endl;

        m1.emplace('b', 2);

        if (!(m1 <= m2))
            cout << "Map m1 is not less than or equal to m2." << endl;
    }

    {
        map<char, int> m1;
        map<char, int> m2;

        m1.emplace('a', 1);

        if (m1 > m2)
            cout << "Map m1 is greater than m2." << endl;

        m1 = m2;

        if (!(m1 > m2))
            cout << "Map m1 is not greater than m2." << endl;
    }

    {
        map<char, int> m1;
        map<char, int> m2;

        m1.emplace('a', 1);
        m2.emplace('a', 1);

        if (m1 >= m2)
            cout << "Map m1 is greater than or equal to m2." << endl;

        m2.emplace('b', 2);

        if (!(m1 >= m2))
            cout << "Map m1 is not greater than or equal to m2." << endl;
    }

    {
        map<char, int> m1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        map<char, int> m2;

        swap(m1, m2);

        cout << "Map contains following elements" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        multimap<char, int> m;

        cout << "Size of multimap = " << m.size() << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 = {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        multimap<char, int>m2(m1.begin(), m1.end());

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 = {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        multimap<char, int>m2(m1);

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 = {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        multimap<char, int>m2(move(m1));

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m.cbegin(); it != m.cend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        cout << "Initial size of multimap = " << m.size() << endl;

        m.clear();

        cout << "Size of multimap after clear operation = "
             << m.size() << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        cout << "count of 'a' = " << m.count('a') << endl;
        cout << "count of 'b' = " << m.count('b') << endl;
        cout << "count of 'c' = " << m.count('c') << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        cout << "Multimap contains following elements in reverse order" << endl;

        for (auto it = m.crbegin(); it != m.crend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        multimap<char, int> m {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        m.emplace('a', 2);
        m.emplace('b', 2);

        cout << "Multimap contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        multimap<char, int> m {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        m.emplace_hint(m.begin(), 'a', 1);
        m.emplace_hint(m.end(), 'e', 5);

        cout << "Multimap contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        multimap<char, int> m;

        if (m.empty())
            cout << "Multimap is empty." << endl;

        m.emplace_hint(m.begin(), 'a', 1);

        if (!m.empty())
            cout << "Multimap is not empty." << endl;
    }

    {
        multimap<char, int> m = {
                {'a', 1},
                {'b', 2},
                {'b', 3},
                {'b', 4},
                {'d', 5}
        };

        auto ret = m.equal_range('b');

        cout << "Elements associated with key 'b': ";

        for (auto it = ret.first; it != ret.second; ++it)
            cout << it->second << " ";
        cout << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        cout << "Multimap contains following elements before erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        cout << endl << endl;

        m.erase(m.begin());

        cout << "Multimap contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };
        cout << "Multimap contains following elements before erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        auto it = m.erase(m.begin());

        cout << "Multimap contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        cout << "After erase operation iterator points to " << it->first
             << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };
        size_t ret;

        cout << "Multimap contains following elements before erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        cout << endl;

        ret = m.erase('a');

        cout << "Number of value removed are = " << ret << endl;

        cout << endl;

        cout << "Multimap contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };
        cout << "Multimap contains following elements before erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        auto it = m.begin();
        ++it, ++it;

        m.erase(m.begin(), it);

        cout << "Multimap contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };
        cout << "Multimap contains following elements before erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        auto it = m.begin();
        ++it, ++it;

        it = m.erase(m.begin(), it);

        cout << "Multimap contains following elements after erase operation" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        cout << "After erase operation iterator points to " << it->first
             << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'c', 5},
        };

        auto pos = m.find('a');

        cout << pos->first << " = " << pos->second << endl;
    }

    {
        multimap<char, int> m;
        pair<const char, int> *p;

        p = m.get_allocator().allocate(5);

        cout << "Allocated size = " <<  sizeof(*p) * 5 << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
        };

        auto pos = m.insert(pair<char, int>('d', 5));

        cout << "After inserting new element iterator points to" << endl;
        cout << pos->first << " = " << pos->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
        };

        auto pos = m.insert(m.begin(), pair<char, int>('a', 0));

        cout << "After inserting new element iterator points to" << endl;
        cout << pos->first << " = " << pos->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        multimap<char, int> m2;

        m2.insert(m1.begin(), m1.end());

        cout << "Multimap contains the following elements:" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
        };

        auto pos = m.insert(m.begin(), move(pair<char, int>('a', 0)));

        cout << "After inserting new element iterator points to" << endl;
        cout << pos->first << " = " << pos->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
        };

        m.insert({{'c', 4}, {'d', 5}});

        cout << "Multimap contains the following elements:" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        multimap<char, int> m = {
                {'a', 1},
                {'a', 2},
                {'d', 3},
                {'d', 4},
                {'e', 5},
        };

        auto comp = m.key_comp();
        char last = m.rbegin()->first;
        auto it = m.begin();

        cout << "Multimap contains following elements" << endl;

        do
            cout << it->first << " = " << it->second << endl;
        while (comp((*it++).first, last));
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        auto it = m.lower_bound('a');

        cout << "Lower bound is" << endl;

        cout << it->first << " = " << it->second << endl;
    }

    {
        multimap<char, int> m;

        cout << "Max size of multimap = " << m.max_size() << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        multimap<char, int> m2;

        m2 = m1;

        cout << "Multimap contains following elements" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        multimap<char, int> m2;

        m2 = move(m1);

        cout << "Multimap contains following elements" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        multimap<char, int> m;

        m = {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        cout << "Multimap contains following elements" << endl;

        for (auto it = m.begin(); it != m.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        cout << "Multimap contains following elements in reverse order" << endl;

        for (auto it = m.rbegin(); it != m.rend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        cout << "Multimap contains " << m.size() << " elements." << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        multimap<char, int> m2;

        m2.swap(m1);

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        auto it = m.upper_bound('a');

        cout << "Upper bound is" << endl;

        cout << it->first << " = " << it->second << endl;
    }

    {
        multimap<char, int> m = {
                {'a', 1},
                {'a', 2},
                {'d', 3},
                {'d', 4},
                {'e', 5},
        };

        auto last = *m.rbegin();
        auto it = m.begin();

        cout << "Multimap contains following elements" << endl;

        do
            cout << it->first << " = " << it->second << endl;
        while (m.value_comp()(*it++, last));
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1;
        multimap<char, int> m2;

        if (m1 == m2)
            cout << "Both multimaps are equal." << endl;

        m1.insert(pair<char, int>('a', 1));

        if (!(m1 == m2))
            cout << "Both multimaps are not equal." << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1;
        multimap<char, int> m2;

        m1.insert(pair<char, int>('a', 1));

        if (m1 != m2)
            cout << "Both multimaps are not equal." << endl;

        m1 = m2;

        if (!(m1 != m2))
            cout << "Both multimaps not equal." << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1;
        multimap<char, int> m2;

        m2.insert(pair<char, int>('a', 1));

        if (m1 < m2)
            cout << "m1 multimap is less than m2." << endl;

        m1 = m2;

        if (!(m1 < m2))
            cout << "m1 multimap is not less than m2." << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1;
        multimap<char, int> m2;

        if (m1 <= m2)
            cout << "m1 multimap is less than or equal to m2." << endl;

        m1.insert(pair<char, int>('a', 1));

        if (!(m1 <= m2))
            cout << "m1 multimap is not less than or equal to m2." << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1;
        multimap<char, int> m2;

        m1.insert(pair<char, int>('a', 1));

        if (m1 > m2)
            cout << "m1 multimap is greater than m2." << endl;

        m1 = m2;

        if (!(m1 > m2))
            cout << "m1 multimap is not greater than m2." << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1;
        multimap<char, int> m2;

        m1.insert(pair<char, int>('a', 1));

        if (m1 >= m2)
            cout << "m1 multimap is greater than or equal to m2." << endl;

        m2.insert(pair<char, int>('a', 1));
        m2.insert(pair<char, int>('a', 1));

        if (!(m1 >= m2))
            cout << "m1 multimap is not greater than or equal to m2." << endl;
    }

    {
        /* Multimap with duplicates */
        multimap<char, int> m1 {
                {'a', 1},
                {'a', 2},
                {'b', 3},
                {'c', 4},
                {'d', 5}
        };

        multimap<char, int> m2;

        swap(m1, m2);

        cout << "Multimap contains following elements:" << endl;

        for (auto it = m2.begin(); it != m2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    return 0;
}