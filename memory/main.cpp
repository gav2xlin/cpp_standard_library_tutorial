#include <memory>
#include <iostream>
#include <string>
#include <set>

struct BaseClass {};

struct DerivedClass : BaseClass {
    void f() const {
        std::cout << "Sample word!\n";
    }
};

struct A {
    static const char* static_type;
    const char* dynamic_type;
    A() { dynamic_type = static_type; }
};
struct B: A {
    static const char* static_type;
    B() { dynamic_type = static_type; }
};

const char* A::static_type = "sample text A";
const char* B::static_type = "sample text B";

struct D {
    void operator()(int* p) {
        std::cout << "[deleter called]\n";
        delete[] p;
    }
};

struct C : std::enable_shared_from_this<C> { };

int main() {
    {
        std::allocator<int> a1;
        int *a = a1.allocate(10);

        a[9] = 7;

        std::cout << a[9] << '\n';

        a1.deallocate(a, 10);

        std::allocator<std::string> a2;

        decltype(a1)::rebind<std::string>::other a2_1;

        std::allocator_traits<decltype(a1)>::rebind_alloc<std::string> a2_2;

        std::string *s = a2.allocate(2);

        a2.construct(s, "foo");
        a2.construct(s + 1, "bar");

        std::cout << s[0] << ' ' << s[1] << '\n';

        a2.destroy(s);
        a2.destroy(s + 1);
        a2.deallocate(s, 2);
    }

    {
        std::shared_ptr<int> foo = std::make_shared<int> (100);
        std::shared_ptr<int> foo2 (new int(100));

        auto bar = std::make_shared<int> (200);

        auto baz = std::make_shared<std::pair<int,int>> (300,400);

        std::cout << "*foo: " << *foo << '\n';
        std::cout << "*bar: " << *bar << '\n';
        std::cout << "*baz: " << baz->first << ' ' << baz->second << '\n';
    }

    {
        std::allocator<int> alloc;
        std::default_delete<int> del;

        std::shared_ptr<int> foo = std::allocate_shared<int> (alloc,100);

        auto bar = std::allocate_shared<int> (alloc,200);

        auto baz = std::allocate_shared<std::pair<int,int>> (alloc,300,400);

        std::cout << "*foo: " << *foo << '\n';
        std::cout << "*bar: " << *bar << '\n';
        std::cout << "*baz: " << baz->first << ' ' << baz->second << '\n';
    }

    {
        std::shared_ptr<BaseClass> ptr_to_base(std::make_shared<DerivedClass>());

        std::static_pointer_cast<DerivedClass>(ptr_to_base)->f();

        static_cast<DerivedClass*>(ptr_to_base.get())->f();
    }

    {
        std::shared_ptr<A> foo;
        std::shared_ptr<B> bar;

        bar = std::make_shared<B>();

        foo = std::dynamic_pointer_cast<A>(bar);

        std::cout << "foo's static type: " << foo->static_type << '\n';
        std::cout << "foo's dynamic type: " << foo->dynamic_type << '\n';
        std::cout << "bar's static type: " << bar->static_type << '\n';
        std::cout << "bar's dynamic type: " << bar->dynamic_type << '\n';
    }

    {
        std::shared_ptr<int> foo;
        std::shared_ptr<const int> bar;

        foo = std::make_shared<int>(100);

        bar = std::const_pointer_cast<const int>(foo);

        std::cout << "*bar: " << *bar << '\n';
        *foo = 200;
        std::cout << "*bar: " << *bar << '\n';
    }

    {
        std::shared_ptr<int> foo (new int[10],D());
        int * bar = new int[20];
        (*std::get_deleter<D>(foo))(bar);
    }

    {
        int * p = new int (10);

        std::shared_ptr<int> a (new int (20));
        std::shared_ptr<int> b (a,p);

        std::set < std::shared_ptr<int> > value_based;
        std::set < std::shared_ptr<int>, std::owner_less<std::shared_ptr<int>> > owner_based;

        value_based.insert (a);
        value_based.insert (b);

        owner_based.insert (a);
        owner_based.insert (b);

        std::cout << "value_based.size() is " << value_based.size() << '\n';
        std::cout << "owner_based.size() is " << owner_based.size() << '\n';
    }

    {
        std::shared_ptr<C> foo, bar;

        foo = std::make_shared<C>();

        bar = foo->shared_from_this();

        if (!foo.owner_before(bar) && !bar.owner_before(foo))
            std::cout << "bother pointers shared ownership";
    }
}