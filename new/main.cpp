#include <iostream>
#include <new>
#include <exception>

struct MyClass {
    int data[100];
    MyClass() {std::cout << "It constructed [" << this << "]\n";}
};

struct MyClass2 {
    int data;
    MyClass2() {std::cout << '@';}
};

struct MyClass3 {
    MyClass3() {std::cout <<"It is a MyClass() constructed\n";}
    ~MyClass3() {std::cout <<"It is a MyClass() destroyed\n";}
};

int main () {
    {
        std::cout << "1: ";
        MyClass *p1 = new MyClass;

        std::cout << "2: ";
        MyClass *p2 = new(std::nothrow) MyClass;

        std::cout << "3: ";
        new(p2) MyClass;

        std::cout << "4: ";
        MyClass *p3 = (MyClass *) ::operator new(sizeof(MyClass));

        delete p1;
        delete p2;
        delete p3;
    }

    {
        std::cout << "constructions (1): ";
        MyClass2 * p1 = new MyClass2[10];
        std::cout << '\n';

        std::cout << "constructions (2): ";
        MyClass2 * p2 = new (std::nothrow) MyClass2[5];
        std::cout << '\n';

        delete[] p2;
        delete[] p1;
    }

    {
        MyClass3 * pt = new (std::nothrow) MyClass3;
        delete pt;
    }

    {
        MyClass3 * pt;

        pt = new MyClass3[3];
        delete[] pt;
    }

    {
        try {
            while (true) {
                new int[100000000ul];
            }
        } catch (const std::bad_alloc& e) {
            std::cout << "Allocation failed: " << e.what() << '\n';
        }
    }

    {
        try {
            int* p = new int[-1];
        } catch (std::bad_array_new_length& e) {
            std::cerr << "bad_array_new_length caught: " << e.what() << '\n';
        } catch (std::exception& e) {
            std::cerr << "some other standard exception caught: " << e.what() << '\n';
        }
    }

    {
        std::cout << "Attempting to allocate...";
        char* p = new (std::nothrow) char [1024*1024];
        if (p==0) std::cout << "Failed!\n";
        else {
            std::cout << "Succeeded!\n";
            delete[] p;
        }
    }

    return 0;
}