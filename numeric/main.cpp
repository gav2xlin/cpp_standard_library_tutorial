#include <iostream>
#include <functional>
#include <numeric>

int myfunction (int x, int y) {return x+2*y;}
struct myclass {
    int operator()(int x, int y) {return x+3*y;}
} myobject;

int myop (int x, int y) {return x+y;}

int myaccumulator (int x, int y) {return x-y;}
int myproduct (int x, int y) {return x+y;}

int myop2 (int x, int y) {return x+y+1;}

int main () {
    {
        int init = 100;
        int numbers[] = {5, 10, 20};

        std::cout << "using default accumulate: ";
        std::cout << std::accumulate(numbers, numbers + 3, init);
        std::cout << '\n';

        std::cout << "using functional's minus: ";
        std::cout << std::accumulate(numbers, numbers + 3, init, std::minus<int>());
        std::cout << '\n';

        std::cout << "using custom function: ";
        std::cout << std::accumulate(numbers, numbers + 3, init, myfunction);
        std::cout << '\n';

        std::cout << "using custom object: ";
        std::cout << std::accumulate(numbers, numbers + 3, init, myobject);
        std::cout << '\n';
    }

    {
        int val[] = {10,20,30,50,60,70};
        int result[7];

        std::adjacent_difference (val, val+7, result);
        std::cout << "Default adjacent_difference: ";
        for (int i=0; i<7; i++) std::cout << result[i] << ' ';
        std::cout << '\n';

        std::adjacent_difference (val, val+7, result, std::multiplies<int>());
        std::cout << "Functional operation multiplies: ";
        for (int i=0; i<7; i++) std::cout << result[i] << ' ';
        std::cout << '\n';

        std::adjacent_difference (val, val+7, result, myop);
        std::cout << "Custom function operation: ";
        for (int i=0; i<7; i++) std::cout << result[i] << ' ';
        std::cout << '\n';
    }

    {
        int init = 100;
        int series1[] = {20,30,40};
        int series2[] = {1,2,3};

        std::cout << "Default inner_product: ";
        std::cout << std::inner_product(series1,series1+3,series2,init);
        std::cout << '\n';

        std::cout << "Functional operations: ";
        std::cout << std::inner_product(series1,series1+3,series2,init,
                                        std::minus<int>(),std::divides<int>());
        std::cout << '\n';

        std::cout << "Ccustom functions: ";
        std::cout << std::inner_product(series1,series1+3,series2,init,
                                        myaccumulator,myproduct);
        std::cout << '\n';
    }

    {
        int val[] = {10,20,30,40,50};
        int result[5];

        std::partial_sum (val, val+5, result);
        std::cout << "Default partial_sum: ";
        for (int i=0; i<5; i++) std::cout << result[i] << ' ';
        std::cout << '\n';

        std::partial_sum (val, val+5, result, std::multiplies<int>());
        std::cout << "Functional operation multiplies: ";
        for (int i=0; i<5; i++) std::cout << result[i] << ' ';
        std::cout << '\n';

        std::partial_sum (val, val+5, result, myop2);
        std::cout << "Custom function: ";
        for (int i=0; i<5; i++) std::cout << result[i] << ' ';
        std::cout << '\n';
    }

    {
        int numbers[5];

        std::iota (numbers,numbers+10,10);

        std::cout << "numbers are :";
        for (int& i:numbers) std::cout << ' ' << i;
        std::cout << '\n';
    }

    return 0;
}