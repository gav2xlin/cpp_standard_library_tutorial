#include <iostream>
#include <fstream>

int main() {
    {
        int n;

        std::cout << "Enter a number: ";
        std::cin >> n;
        std::cout << "You have entered: " << n << '\n';

        std::cout << "Enter a hexadecimal number: ";
        std::cin >> std::hex >> n;
        std::cout << "Its decimal equivalent is: " << n << '\n';
    }

    {
        std::ofstream outfile ("test.txt");
        char ch;

        std::cout << "Type some text (type a dot to finish):\n";
        do {
            ch = std::cin.get();
            outfile.put(ch);
        } while (ch!='.');
    }

    {
        std::ifstream infile ("test.txt",std::ifstream::binary);
        std::ofstream outfile ("new.txt",std::ofstream::binary);

        infile.seekg (0,infile.end);
        long size = infile.tellg();
        infile.seekg (0);

        char* buffer = new char[size];

        infile.read (buffer,size);

        outfile.write (buffer,size);

        delete[] buffer;

        outfile.close();
        infile.close();
    }

    {
        std::ofstream outfile;
        outfile.open ("test.txt");

        outfile.write ("This is an apple",16);
        long pos = outfile.tellp();
        outfile.seekp (pos-7);
        outfile.write (" sam",4);

        outfile.close();
    }

    {
        std::ofstream outfile;
        outfile.open ("tutorialspoint.txt");

        outfile.write ("This is an apple",16);
        long pos = outfile.tellp();
        outfile.seekp (pos-7);
        outfile.write (" sai",4);

        outfile.close();
    }

    {
        std::ofstream outfile ("test.txt");

        for (int n=0; n<100; ++n) {
            outfile << n;
            outfile.flush();
        }
        outfile.close();
    }

    return 0;
}
