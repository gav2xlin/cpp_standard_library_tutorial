#include <iostream>
#include <queue>
#include <vector>

using namespace std;

int main(void) {
    {
        deque<int> d(5, 100);
        queue<int> q1;
        queue<int> q2(d);

        cout << "Size of q1 = " << q1.size() << endl;
        cout << "Size of q2 = " << q2.size() << endl;
    }

    {
        auto it = {1, 2, 3, 4, 5};
        queue<int> q(it);

        cout << "Queue contents are" << endl;
        while (!q.empty()) {
            cout << q.front() << endl;
            q.pop();
        }
    }

    {
        auto it = {1, 2, 3, 4, 5};
        queue<int> q1(it);
        queue<int>q2(move(q1));

        cout << "Contents of q1 after move operation" << endl;
        while (!q1.empty()) {
            cout << q1.front() << endl;
            q1.pop();
        }

        cout << endl << endl;

        cout << "Contents of q2 after move operation" << endl;
        while (!q2.empty()) {
            cout << q2.front() << endl;
            q2.pop();
        }
    }

    {
        auto it = {1, 2, 3, 4, 5};
        queue<int> q1(it);
        queue<int>q2(q1);

        cout << "Contents of q1" << endl;
        while (!q1.empty()) {
            cout << q1.front() << endl;
            q1.pop();
        }

        cout << endl;

        cout << "Contents of q2" << endl;
        while (!q2.empty()) {
            cout << q2.front() << endl;
            q2.pop();
        }
    }

    {
        queue<int> q;

        for (int i = 0; i < 5; ++i)
            q.push(i + 1);

        cout << "Last element of queue q is = " << q.back() << endl;
    }

    {
        queue<int> q;

        for (int i = 0; i < 5; ++i)
            q.emplace(i + 1);

        cout << "Contents of queue" << endl;
        while (!q.empty()) {
            cout << q.front() << endl;
            q.pop();
        }
    }

    {
        queue<int> q;

        if (q.empty())
            cout << "Queue is empty." << endl;

        q.push(10);

        if (!q.empty())
            cout << "Queue is not empty." << endl;
    }

    {
        queue<int> q;

        for (int i = 0; i < 5; ++i)
            q.emplace(i + 1);

        cout << "First element of queue = " << q.front() << endl;
    }

    {
        auto it1 = {1, 2, 3, 4, 5};
        auto it2 = {10, 20};
        queue<int> q1(it1);
        queue<int> q2(it2);

        q2 = q1;

        cout << "Contents of q1" << endl;
        while (!q1.empty()) {
            cout << q1.front() << endl;
            q1.pop();
        }

        cout << endl;

        cout << "Contents of q2" << endl;
        while (!q2.empty()) {
            cout << q2.front() << endl;
            q2.pop();
        }
    }

    {
        auto it1 = {1, 2, 3, 4, 5};
        auto it2 = {10, 20};
        queue<int> q1(it1);
        queue<int> q2(it2);

        q2 = move(q1);

        cout << "Contents of q1" << endl;
        while (!q1.empty()) {
            cout << q1.front() << endl;
            q1.pop();
        }

        cout << endl;

        cout << "Contents of q2" << endl;
        while (!q2.empty()) {
            cout << q2.front() << endl;
            q2.pop();
        }
    }

    {
        queue<int> q;

        for (int i = 0; i < 5; ++i)
            q.emplace(i + 1);

        cout << "Contents of queue" << endl;
        while (!q.empty()) {
            cout << q.front() << endl;
            q.pop();
        }
    }

    {
        queue<int> q;

        for (int i = 0; i < 5; ++i)
            q.push(i + 1);

        cout << "Contents of queue" << endl;
        while (!q.empty()) {
            cout << q.front() << endl;
            q.pop();
        }
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i)
            q1.push(i + 1);

        while (!q1.empty()) {
            /* push element by performing move operation */
            q2.push(move(q1.front()));
            q1.pop();
        }

        cout << "Contents of queue" << endl;
        while (!q2.empty()) {
            cout << q2.front() << endl;
            q2.pop();
        }
    }

    {
        queue<int> q;

        for (int i = 0; i < 5; ++i)
            q.push(i + 1);

        cout << "Size of queue = " << q.size() << endl;
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i)
            q1.push(i + 1);

        for (int i = 0; i < 2; ++i)
            q2.push(i + 150);

        q1.swap(q2);

        cout << "Content of q1 and q2 after swap operation" << endl;;
        while (!q1.empty()) {
            cout << q1.front() << endl;
            q1.pop();
        }

        cout << endl << endl;

        while (!q2.empty()) {
            cout << q2.front() << endl;
            q2.pop();
        }
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i) {
            q1.push(i + 1);
            q2.push(i + 1);
        }

        if (q1 == q2)
            cout << "q1 and q2 are identical." << endl;

        q1.push(6);

        if (!(q1 == q2))
            cout << "q1 and q2 are not identical." << endl;
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i) {
            q1.push(i + 1);
            q2.push(i + 1);
        }

        q1.pop();

        if (q1 != q2)
            cout << "q1 and q2 are not identical." << endl;

        q2.pop();

        if (!(q1 != q2))
            cout << "q1 and q2 are identical." << endl;
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i)
            q1.push(i);

        for (int i = 0; i < 15; ++i)
            q2.push(i);

        if (q1 < q2)
            cout << "q1 is less than q2." << endl;

        q2.swap(q1);

        if (!(q1 < q2))
            cout << "q1 is not less than q2." << endl;
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i) {
            q1.push(i);
            q2.push(i);
        }

        if (q1 <= q2)
            cout << "q1 is less than or equal to q2." << endl;

        q1.pop();

        if (!(q1 <= q2))
            cout << "q1 is not less than or equal to q2." << endl;
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i) {
            q1.push(i);
            q2.push(i);
        }

        q1.emplace(6);

        if (q1 > q2)
            cout << "q1 is greater than q2." << endl;

        q2.emplace(6);
        q2.emplace(7);

        if (!(q1 > q2))
            cout << "q1 is not greater than q2." << endl;
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i) {
            q1.push(i);
            q2.push(i);
        }

        if (q1 >= q2)
            cout << "q1 is greater than or equal to q2." << endl;

        q2.emplace(6);

        if (!(q1 >= q2))
            cout << "q1 is not greater than or equal to q2." << endl;
    }

    {
        queue<int> q1, q2;

        for (int i = 0; i < 5; ++i)
            q1.push(i + 1);

        for (int i = 0; i < 3; ++i)
            q2.push(i + 100);

        swap(q1, q2);

        cout << "Contents of q1 and q2 after swap operation" << endl;
        while (!q1.empty()) {
            cout << q1.front() << endl;
            q1.pop();
        }

        cout << endl << endl;

        while (!q2.empty()) {
            cout << q2.front() << endl;
            q2.pop();
        }
    }

    {
        priority_queue<int> q;

        q.push(3);
        q.push(1);
        q.push(5);
        q.push(2);
        q.push(4);

        cout << "Queue contents are" << endl;
        while (!q.empty()) {
            cout << q.top() << endl;
            q.pop();
        }
    }

    {
        auto it = {3, 1, 5, 2, 4};
        priority_queue<int> q(less<int>(), it);

        cout << "Queue contents are" << endl;
        while (!q.empty()) {
            cout << q.top() << endl;
            q.pop();
        }
    }

    {
        vector<int> v = {3, 1, 5, 2, 4};
        priority_queue<int> q(v.begin(), v.begin() + 4);

        cout << "Queue contents are" << endl;
        while (!q.empty()) {
            cout << q.top() << endl;
            q.pop();
        }
    }

    {
        auto it = {3, 1, 5, 2, 4};
        priority_queue<int> q1(less<int>(), it);
        priority_queue<int> q2(move(q1));

        cout << "Contents of q1 after move operation" << endl;
        while (!q1.empty()) {
            cout << q1.top() << endl;
            q1.pop();
        }

        cout << endl;

        cout << "Contents of q2 after move operation" << endl;
        while (!q2.empty()) {
            cout << q2.top() << endl;
            q2.pop();
        }
    }

    {
        vector<int> v = {3, 1, 5, 2, 4};
        priority_queue<int> q1(less<int>(), v);
        priority_queue<int> q2(q1);

        cout << "Contents of q1" << endl;
        while (!q1.empty()) {
            cout << q1.top() << endl;
            q1.pop();
        }

        cout << endl;

        cout << "Contents of q2" << endl;
        while (!q2.empty()) {
            cout << q2.top() << endl;
            q2.pop();
        }
    }

    {
        priority_queue<int> q;

        q.emplace(3);
        q.emplace(1);
        q.emplace(5);
        q.emplace(2);
        q.emplace(4);

        cout << "Queue contents are" << endl;
        while (!q.empty()) {
            cout << q.top() << endl;
            q.pop();
        }
    }

    {
        priority_queue<int> q;

        if (q.empty())
            cout << "Priority_queue is empty." << endl;

        q.emplace(1);

        if (!q.empty())
            cout << "Priority_queue is not empty." << endl;
    }

    {
        auto it1 = {3, 1, 5, 2, 4};
        auto it2 = {3, 1, 5};
        priority_queue<int> q1(less<int>(), it1);
        priority_queue<int> q2(less<int>(), it2);

        q1 = q2;

        cout << "Contents of q1" << endl;
        while (!q1.empty()) {
            cout << q1.top() << endl;
            q1.pop();
        }

        cout << "Contents of q2" << endl;
        while (!q2.empty()) {
            cout << q2.top() << endl;
            q2.pop();
        }
    }

    {
        auto it1 = {3, 1, 5, 2, 4};
        auto it2 = {3, 1, 5};
        priority_queue<int> q1(less<int>(), it1);
        priority_queue<int> q2(less<int>(), it2);

        q1 = move(q2);

        cout << "Contents of q1" << endl;
        while (!q1.empty()) {
            cout << q1.top() << endl;
            q1.pop();
        }

        cout << "Contents of q2" << endl;
        while (!q2.empty()) {
            cout << q2.top() << endl;
            q2.pop();
        }
    }

    {
        auto it = {3, 1, 5, 2, 4};
        priority_queue<int> q(less<int>(), it);

        cout << "Queue contents are" << endl;
        while (!q.empty()) {
            cout << q.top() << endl;
            q.pop();
        }
    }

    {
        priority_queue<int> q;

        for (int i = 0; i < 5; ++i) {
            q.push(i + 1);
        }

        cout << "Queue contents are" << endl;
        while (!q.empty()) {
            cout << q.top() << endl;
            q.pop();
        }
    }

    {
        auto it1 = {3, 1, 5, 2, 4};
        priority_queue<int> q1(less<int>(), it1);
        priority_queue<int> q2;

        for (int i = 0; i < 5; ++i) {
            q2.push(move(q1.top()));
            q1.pop();
        }

        cout << "Contents of q1" << endl;
        while (!q1.empty()) {
            cout << q1.top() << endl;
            q1.pop();
        }

        cout << "Contents of q2" << endl;
        while (!q2.empty()) {
            cout << q2.top() << endl;
            q2.pop();
        }
    }

    {
        priority_queue<int> q;

        cout << "Initial size of queue = " << q.size() << endl;

        for (int i = 0; i < 5; ++i)
            q.push(i + 1);

        cout << "After push opration size of queue = " << q.size() << endl;
    }

    {
        auto it1 = {3, 1, 5, 2, 4};
        auto it2 = {5, 2, 4};
        priority_queue<int> q1(less<int>(), it1);
        priority_queue<int> q2(less<int>(), it2);

        q1.swap(q2);

        cout << "Contents of q1 after swap operation" << endl;
        while (!q1.empty()) {
            cout << q1.top() << endl;
            q1.pop();
        }

        cout << "Contents of q2 after swap operation" << endl;
        while (!q2.empty()) {
            cout << q2.top() << endl;
            q2.pop();
        }
    }

    {
        auto it = {3, 1, 5, 2, 4};
        priority_queue<int> q(less<int>(), it);

        cout << "Queue contents are" << endl;
        while (!q.empty()) {
            cout << q.top() << endl;
            q.pop();
        }
    }

    {
        auto it1 = {3, 1, 5, 2, 4};
        auto it2 = {2, 4};
        priority_queue<int> q1(less<int>(), it1);
        priority_queue<int> q2(less<int>(), it2);

        swap(q1, q2);

        cout << "Content of q1" << endl;
        while (!q1.empty()) {
            cout << q1.top() << endl;
            q1.pop();
        }

        cout << "Content of q2" << endl;
        while (!q2.empty()) {
            cout << q2.top() << endl;
            q2.pop();
        }
    }

    return 0;
}