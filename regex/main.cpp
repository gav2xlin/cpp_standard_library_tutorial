#include <iostream>
#include <string>
#include <regex>
#include <iterator>

int main () {
    {
        if (std::regex_match("subject", std::regex("(sub)(.*)")))
            std::cout << "string literal matched\n";

        const char cstr[] = "subject";
        std::string s("subject");
        std::regex e("(sub)(.*)");

        if (std::regex_match(s, e))
            std::cout << "string object matched\n";

        if (std::regex_match(s.begin(), s.end(), e))
            std::cout << "range matched\n";

        std::cmatch cm;
        std::regex_match(cstr, cm, e);
        std::cout << "string literal with " << cm.size() << " matches\n";

        std::smatch sm;
        std::regex_match(s, sm, e);
        std::cout << "string object with " << sm.size() << " matches\n";

        std::regex_match(s.cbegin(), s.cend(), sm, e);
        std::cout << "range with " << sm.size() << " matches\n";

        std::regex_match(cstr, cm, e, std::regex_constants::match_default);

        std::cout << "the matches were: ";
        for (unsigned i = 0; i < sm.size(); ++i) {
            std::cout << "[" << sm[i] << "] ";
        }

        std::cout << std::endl;
    }

    {
        std::string s ("this subject has a submarine as a subsequence");
        std::smatch m;
        std::regex e ("\\b(sub)([^ ]*)");

        std::cout << "Target sequence: " << s << std::endl;
        std::cout << "Regular expression: /\\b(sub)([^ ]*)/" << std::endl;
        std::cout << "The following matches and submatches were found:" << std::endl;

        while (std::regex_search (s,m,e)) {
            for (auto x:m) std::cout << x << " ";
            std::cout << std::endl;
            s = m.suffix().str();
        }
    }

    {
        std::string s ("there is a subsequence in the string\n");
        std::regex e ("\\b(sub)([^ ]*)");

        std::cout << std::regex_replace (s,e,"sub-$2");

        std::string result;
        std::regex_replace (std::back_inserter(result), s.begin(), s.end(), e, "$2");
        std::cout << result;

        std::cout << std::regex_replace (s,e,"$1 and $2",std::regex_constants::format_no_copy);
        std::cout << std::endl;
    }

    {
        const std::string s = "Tutorialspoint.com india pvt ltd.";

        std::regex words_regex("[^\\s]+");
        auto words_begin =
                std::sregex_iterator(s.begin(), s.end(), words_regex);
        auto words_end = std::sregex_iterator();

        std::cout << "Found "
                  << std::distance(words_begin, words_end)
                  << " words:\n";

        for (std::sregex_iterator i = words_begin; i != words_end; ++i) {
            std::smatch match = *i;
            std::string match_str = match.str();
            std::cout << match_str << '\n';
        }
    }

    {
        std::string text = "Tutorialspoint india pvt ltd.";

        std::regex ws_re("\\s+");
        std::copy( std::sregex_token_iterator(text.begin(), text.end(), ws_re, -1),
                   std::sregex_token_iterator(),
                   std::ostream_iterator<std::string>(std::cout, "\n"));

        std::string html = "<p><a href=\"http://tutorialspoint.com\">google</a> "
                           "< a HREF =\"http://indiabbc.com\">cppreference</a>\n</p>";
        std::regex url_re("<\\s*A\\s+[^>]*href\\s*=\\s*\"([^\"]*)\"", std::regex::icase);
        std::copy( std::sregex_token_iterator(html.begin(), html.end(), url_re, 1),
                   std::sregex_token_iterator(),
                   std::ostream_iterator<std::string>(std::cout, "\n"));
    }

    return 0;
}