#include <iostream>
#include <set>
#include <string>

using namespace std;

int main(void) {
    {
        // Default constructor
        std::set<char> t_set;

        t_set.insert('a');
        t_set.insert('e');
        t_set.insert('i');
        t_set.insert('o');
        t_set.insert('u');

        int size = t_set.size();

        std::cout << "Contents of set container t_set = " << size;
    }

    {
        char vowels[] = {'a','e','i','o','u'};

        // Range Constructor
        std::set<char> t_set (vowels, vowels+5);

        std::cout << "Size of set container t_set is : " << t_set.size();
    }

    {
        //Default Constructor
        std::set<int> t_set;
        t_set.insert(5);
        t_set.insert(10);

        std::cout << "Size of set container t_set is : " << t_set.size();

        // Copy constructor
        std::set<int> t_set_new(t_set);
        std::cout << "\nSize of new set container t_set_new is : " << t_set_new.size();
    }

    {
        // Default constructor
        std::set<char> t_set;
        t_set.insert('x');
        t_set.insert('y');

        std::cout << "Size of set container t_set is : " << t_set.size();

        // Move constructor
        std::set<char> t_set_new(std::move(t_set));
        std::cout << "\nSize of new set container t_set_new is : " << t_set_new.size();
    }

    {
        // Initializer list constructor
        std::set<std::string> fruit {
                "orange", "apple", "mango", "peach", "grape"
        };

        std::cout << "Size of set container fruit is : " << fruit.size();
    }

    {
        //Default constructor
        std::set<string> t_set;

        t_set.insert("Tutorials Point");
    }

    {
        int myints[] = { 10,20,30,40,50 };
        std::set<int> first (myints,myints+10);
        std::set<int> second;

        second = first;
        first = std::set<int>();

        std::cout << "Size of first: " << int (first.size()) << '\n';
        std::cout << "Size of second: " << int (second.size()) << '\n';
    }

    {
        int myints[] = {50,40,30,20,10};
        std::set<int> myset (myints,myints+10);

        std::cout << "myset contains:";
        for (std::set<int>::iterator it = myset.begin(); it!=myset.end(); ++it)
            std::cout << ' ' << *it;

        std::cout << '\n';
    }

    {
        std::set<int> myset = {50,40,30,20,10};

        std::cout << "myset contains:";
        for (auto it = myset.cbegin(); it != myset.cend(); ++it)
            std::cout << ' ' << *it;

        std::cout << '\n';
    }

    {
        int myints[] = {20,40,60,80,100};
        std::set<int> myset (myints,myints+10);

        std::set<int>::reverse_iterator rit;

        std::cout << "myset contains:";
        for (rit = myset.rbegin(); rit != myset.rend(); ++rit)
            std::cout << ' ' << *rit;

        std::cout << '\n';
    }

    {
        std::set<int> myset = {50,40,30,20,10};

        std::cout << "myset backwards:";
        for (auto rit = myset.crbegin(); rit != myset.crend(); ++rit)
            std::cout << ' ' << *rit;

        std::cout << '\n';
    }

    {
        std::set<int> myset;

        myset.insert(0);
        myset.insert(10);
        myset.insert(20);

        std::cout << "myset contains:";
        while (!myset.empty()) {
            std::cout << ' ' << *myset.begin();
            myset.erase(myset.begin());
        }
        std::cout << '\n';
    }

    {
        std::set<int> myints;
        std::cout << "0. size: " << myints.size() << '\n';

        for (int i = 0; i < 5; ++i) myints.insert(i);
        std::cout << "1. size: " << myints.size() << '\n';

        myints.insert (200);
        std::cout << "2. size: " << myints.size() << '\n';

        myints.erase(10);
        std::cout << "3. size: " << myints.size() << '\n';
    }

    {
        int i;
        std::set<int> myset;

        if (myset.max_size()>100) {
            for (i = 0; i < 100; i++) myset.insert(i);
            std::cout << "The set contains 100 elements.\n";
        }
        else std::cout << "The set could not hold 100 elements.\n";
    }

    {
        std::set<int> myset;

        myset.insert (10);
        myset.insert (20);
        myset.insert (30);

        std::cout << "myset contains:";
        for (std::set<int>::iterator it = myset.begin(); it!=myset.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';

        myset.clear();
        myset.insert (111);
        myset.insert (222);

        std::cout << "myset contains:";
        for (std::set<int>::iterator it = myset.begin(); it!=myset.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::set<std::string> myset;

        myset.emplace("foo");
        myset.emplace("bar");
        auto ret = myset.emplace("bar");

        if (!ret.second) std::cout << "bar already exists in myset\n";
    }

    {
        std::set<std::string> myset;
        auto it = myset.cbegin();

        myset.emplace_hint (it,"sairam");
        it = myset.emplace_hint (myset.cend(),"krishna");
        it = myset.emplace_hint (it,"prasad");
        it = myset.emplace_hint (it,"Mammahe");

        std::cout << "myset contains:";
        for (const std::string& x: myset)
            std::cout << ' ' << x;
        std::cout << '\n';
    }

    {
        std::set<int> myset;
        std::set<int>::iterator it;

        for (int i = 1; i < 10; i++) myset.insert(i*20);

        it = myset.begin();
        ++it;

        myset.erase (it);

        myset.erase (80);

        it = myset.find (60);
        myset.erase (it, myset.end());

        std::cout << "myset contains:";
        for (it = myset.begin(); it!=myset.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        int myints[] = {10,20,30,40,50,60};
        std::set<int> first (myints,myints+3);
        std::set<int> second (myints+3,myints+6);

        first.swap(second);

        std::cout << "first contains:";
        for (std::set<int>::iterator it = first.begin(); it!=first.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';

        std::cout << "second contains:";
        for (std::set<int>::iterator it = second.begin(); it!=second.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::set<int> myset;

        for (int i = 1; i < 15;++i) myset.insert(i*5);

        for (int i = 0; i < 5; ++i) {
            std::cout << i;
            if (myset.count(i)!=0)
                std::cout << " is an element of myset.\n";
            else
                std::cout << " is not an element of myset.\n";
        }
    }

    {
        std::set<int> myset;
        std::set<int>::iterator it;

        for (int i = 1; i <= 10; i++) myset.insert(i*10);
        it = myset.find(40);
        myset.erase (it);
        myset.erase (myset.find(60));

        std::cout << "myset contains:";
        for (it = myset.begin(); it!=myset.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::set<int> myset;
        std::set<int>::iterator itlow,itup;

        for (int i = 1; i < 10; i++) myset.insert(i*10);

        itlow = myset.lower_bound (30);

        myset.erase(itlow);

        std::cout << "myset contains:";
        for (std::set<int>::iterator it = myset.begin(); it!=myset.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::set<int> myset;
        std::set<int>::iterator itlow,itup;

        for (int i = 1; i < 10; i++) myset.insert(i*10);

        itup = myset.upper_bound (60);

        myset.erase(itup);

        std::cout << "myset contains:";
        for (std::set<int>::iterator it = myset.begin(); it!=myset.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::set<int> myset;

        for (int i = 1; i <= 5; i++) myset.insert(i*10);

        std::pair<std::set<int>::const_iterator,std::set<int>::const_iterator> ret;
        ret = myset.equal_range(10);

        std::cout << "the lower bound points to: " << *ret.first << '\n';
        std::cout << "the upper bound points to: " << *ret.second << '\n';
    }

    {
        std::set<int> myset;
        int highest;

        std::set<int>::key_compare mycomp = myset.key_comp();

        for (int i = 0; i <= 10; i++) myset.insert(i);

        std::cout << "myset contains:";

        highest=*myset.rbegin();
        std::set<int>::iterator it = myset.begin();
        do {
            std::cout << ' ' << *it;
        } while ( mycomp(*(++it), highest) );

        std::cout << '\n';
    }

    {
        std::set<int> myset;

        std::set<int>::value_compare mycomp = myset.value_comp();

        for (int i = 0; i <= 10; i++) myset.insert(i);

        std::cout << "myset contains:";

        int highest=*myset.rbegin();
        std::set<int>::iterator it = myset.begin();
        do {
            std::cout << ' ' << *it;
        } while ( mycomp(*(++it),highest) );

        std::cout << '\n';
    }

    {
        std::set<int> myset;
        int * p;
        unsigned int i;

        p = myset.get_allocator().allocate(5);

        for (i = 0; i < 5; i++) p[i]=(i+1)*10;

        std::cout << "The allocated array contains:";
        for (i = 0; i < 5; i++) std::cout << ' ' << p[i];
        std::cout << '\n';

        myset.get_allocator().deallocate(p,5);
    }

    return 0;
}