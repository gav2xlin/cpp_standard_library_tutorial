#include <iostream>
#include <stack>
#include <vector>

using namespace std;

int main(void) {
    {
        stack<int> s1;
        vector<int> v = {1, 2, 3, 4, 5};
        stack<int, vector<int>> s2(v);

        cout << "Size of stack s1 = " << s1.size() << endl;

        cout << "Contents of stack s2" << endl;
        while (!s2.empty()) {
            cout << s2.top() << endl;
            s2.pop();
        }
    }

    {
        stack<int> s1;

        for (int i = 0; i < 5; ++i)
            s1.push(i + 1);

        stack<int> s2(s1);

        cout << "Contents of stack s2" << endl;
        while (!s2.empty()) {
            cout << s2.top() << endl;
            s2.pop();
        }
    }

    {
        stack<int> s1;

        for (int i = 0; i < 5; ++i)
            s1.push(i + 1);

        cout << "Size of stack s1 before move operation = " << s1.size() << endl;

        stack<int> s2(move(s1));

        cout << "Size of stack s1 after move operation = " << s1.size() << endl;

        cout << "Contents of stack s2" << endl;
        while (!s2.empty()) {
            cout << s2.top() << endl;
            s2.pop();
        }
    }

    {
        stack<int> s;

        for (int i = 0; i < 5; ++i)
            s.emplace(i + 1);

        while (!s.empty()) {
            cout << s.top() << endl;
            s.pop();
        }
    }

    {
        stack<int> s;

        if (s.empty())
            cout << "Stack is empty." << endl;

        s.emplace(1);

        if (!s.empty())
            cout << "Stack is not empty." << endl;
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i)
            s1.push(i + 1);

        s2 = s1;

        cout << "Contents of stack s2" << endl;
        while (!s2.empty()) {
            cout << s2.top() << endl;
            s2.pop();
        }
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i)
            s1.push(i + 1);

        cout << "Size of stack s1 before move operation = " << s1.size() << endl;

        s2 = move(s1);

        cout << "Size of stack s1 after move operation = " << s1.size() << endl;

        cout << "Contents of stack s2" << endl;
        while (!s2.empty()) {
            cout << s2.top() << endl;
            s2.pop();
        }
    }

    {
        stack<int> s;

        for (int i = 0; i < 5; ++i)
            s.emplace(i + 1);

        while (!s.empty()) {
            cout << s.top() << endl;
            s.pop();
        }
    }

    {
        stack<int> s;

        for (int i = 0; i < 5; ++i)
            s.push(i + 1);

        cout << "Stack contents are" << endl;

        while (!s.empty()) {
            cout << s.top() << endl;
            s.pop();
        }
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i)
            s1.push(i + 1);

        while (!s1.empty()) {
            s2.push(move(s1.top()));
            s1.pop();
        }

        cout << "Stack contents are" << endl;
        while (!s2.empty()) {
            cout << s2.top() << endl;
            s2.pop();
        }
    }

    {
        stack<int> s;

        cout << "Initial size of stack = " << s.size() << endl;

        for (int i = 0; i < 5; ++i)
            s.push(i + 1);

        cout << "Stack size after push operation = " << s.size() << endl;
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i)
            s1.push(i + 1);

        for (int i = 0; i < 3; ++i)
            s2.push(100 + i);

        s1.swap(s2);

        cout << "Contents of stack s1 after swap operation" << endl;
        while (!s1.empty()) {
            cout << s1.top() << endl;
            s1.pop();
        }

        cout << endl;
        cout << "Contents of stack s2 after swap operation" << endl;
        while (!s2.empty()) {
            cout << s2.top() << endl;
            s2.pop();
        }
    }

    {
        stack<int> s;

        for (int i = 0; i < 5; ++i)
            s.emplace(i + 1);

        while (!s.empty()) {
            cout << s.top() << endl;
            s.pop();
        }
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i) {
            s1.push(i + 1);
            s2.push(i + 1);
        }

        if (s1 == s2)
            cout << "Both stacks are equal." << endl;

        s1.push(6);

        if (!(s1 == s2))
            cout << "Both stacks are not equal." << endl;
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i) {
            s1.push(i + 1);
            s2.push(i + 1);
        }

        s1.pop();

        if (s1 != s2)
            cout << "Both stacks not are equal." << endl;

        s1.push(5);

        if (!(s1 != s2))
            cout << "Both stacks are equal." << endl;
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i) {
            s1.push(i + 1);
            s2.push(i + 1);
        }

        s1.pop();

        if (s1 < s2)
            cout << "Stack s1 is less than s2." << endl;

        s1.push(5);

        if (!(s1 < s2))
            cout << "Stack s1 is not less than s2." << endl;
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i) {
            s1.push(i + 1);
            s2.push(i + 1);
        }

        if (s1 <= s2)
            cout << "Stack s1 is less than or equal to s2." << endl;

        s1.push(6);

        if (!(s1 <= s2))
            cout << "Stack s1 is not less than or equal to s2." << endl;
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i) {
            s1.push(i + 1);
            s2.push(i + 1);
        }

        s1.push(6);
        if (s1 > s2)
            cout << "Stack s1 is greater than s2." << endl;

        s2.push(7);

        if (!(s1 > s2))
            cout << "Stack s1 is not greater than s2." << endl;
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i) {
            s1.push(i + 1);
            s2.push(i + 1);
        }

        s1.push(6);
        if (s1 >= s2)
            cout << "Stack s1 is greater than or equal to s2." << endl;

        s2.push(7);

        if (!(s1 >= s2))
            cout << "Stack s1 is not greater than or equal to s2." << endl;
    }

    {
        stack<int> s1;
        stack<int> s2;

        for (int i = 0; i < 5; ++i)
            s1.push(i + 1);

        for (int i = 0; i < 3; ++i)
            s2.push(i + 100);

        swap(s1, s2);

        cout << "Contents of stack s1 after swap operation." << endl;
        while (!s1.empty()) {
            cout << s1.top() << endl;
            s1.pop();
        }

        cout << endl;

        cout << "Contents of stack s2 after swap operation." << endl;
        while (!s2.empty()) {
            cout << s2.top() << endl;
            s2.pop();
        }
    }

    return 0;
}