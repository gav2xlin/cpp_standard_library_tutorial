#include <iostream>
#include <stdexcept>
#include <bitset>
#include <string>
#include <vector>

int main (void) {
    {
        try {
            std::bitset<5> mybitset(std::string("01203040"));
        } catch (const std::invalid_argument &ia) {
            std::cerr << "Invalid argument: " << ia.what() << '\n';
        }
    }

    {
        try {
            std::vector<int> myvector;
            myvector.resize(myvector.max_size()+1);
        } catch (const std::length_error& le) {
            std::cerr << "Length error: " << le.what() << '\n';
        }
    }

    {
        std::vector<int> myvector(10);
        try {
            myvector.at(20) = 100;
        } catch (const std::out_of_range& oor) {
            std::cerr << "Out of Range error: " << oor.what() << '\n';
        }
    }

    return 0;
}