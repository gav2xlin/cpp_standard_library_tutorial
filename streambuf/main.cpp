#include <iostream>
#include <fstream>
#include <cstdio>

int main() {
    {
        char mybuffer [1024];
        std::fstream filestr;
        filestr.rdbuf()->pubsetbuf(mybuffer,1024);
    }

    {
        std::fstream filestr ("sample.txt");
        if (filestr) {
            std::streambuf* pbuf = filestr.rdbuf();
            long size = pbuf->pubseekoff(0,filestr.end);
            std::cout << "The file size is " << size << " characters.\n";
            filestr.close();
        }
    }

    {
        std::fstream filestr ("test.txt");
        if (filestr) {
            std::streambuf* pbuf = filestr.rdbuf();
            long size = pbuf->pubseekoff(0,filestr.end);
            if (size>20) {
                char buffer[11];

                pbuf->pubseekpos(10);

                pbuf->sgetn (buffer,10);

                buffer[10]=0;
                std::cout << buffer << '\n';
            }
            filestr.close();
        }
    }

    {
        std::ofstream ostr ("sample.txt");
        if (ostr) {
            std::streambuf * pbuf = ostr.rdbuf();

            pbuf->sputn ("First sentence\n",25);
            pbuf->pubsync();
            pbuf->sputn ("Second sentence\n",26);

            ostr.close();
        }
    }

    {
        std::ifstream ifs ("sample.txt");
        if (ifs.good()) {
            std::streambuf* pbuf = ifs.rdbuf();
            char c; ifs >> c;
            std::streamsize size = pbuf->in_avail();
            std::cout << "first character in file: " << c << '\n';
            std::cout << size << " characters in buffer after it\n";
        }
        ifs.close();
    }

    {
        std::ifstream istr ("sample.txt");
        if (istr) {
            std::streambuf * pbuf = istr.rdbuf();
            do {
                char ch = pbuf->sgetc();
                std::cout << ch;
            } while ( pbuf->snextc() != std::streambuf::traits_type::eof() );
            istr.close();
        }
    }

    {
        std::ifstream istr ("sample.txt");
        if (istr) {
            std::streambuf * pbuf = istr.rdbuf();
            while ( pbuf->sgetc() != std::streambuf::traits_type::eof() ) {
                char ch = pbuf->sbumpc();
                std::cout << ch;
            }
            istr.close();
        }
    }

    {
        std::ifstream istr ("sample.txt");
        if (istr) {
            std::streambuf * pbuf = istr.rdbuf();
            do {
                char ch = pbuf->sgetc();
                std::cout << ch;
            } while ( pbuf->snextc() != std::streambuf::traits_type::eof() );
            istr.close();
        }
    }

    {
        char* contents;
        std::ifstream istr ("sample.txt");

        if (istr) {
            std::streambuf * pbuf = istr.rdbuf();
            std::streamsize size = pbuf->pubseekoff(0,istr.end);
            pbuf->pubseekoff(0,istr.beg);
            contents = new char [size];
            pbuf->sgetn (contents,size);
            istr.close();
            std::cout.write (contents,size);
        }
    }

    {
        char ch;
        std::streambuf * pbuf = std::cin.rdbuf();

        std::cout << "Please, enter some letters and then a number: ";
        do {
            ch = pbuf->sbumpc();

            if ( (ch>='0') && (ch <='9') ) {
                pbuf->sputbackc (ch);
                long n;
                std::cin >> n;
                std::cout << "You entered number " << n << '\n';
                break;
            }
        } while ( ch != EOF );
    }

    {
        char ch;
        std::streambuf * pbuf = std::cin.rdbuf();

        std::cout << "Please, enter some letters and then a number: ";
        do {
            ch = pbuf->sbumpc();

            if ( (ch>='0') && (ch <='9') ) {
                pbuf->sungetc ();
                long n;
                std::cin >> n;
                std::cout << "You entered number " << n << '\n';
                break;
            }
        } while ( ch != EOF );
    }

    {
        char ch;
        std::ofstream ostr ("test.txt");
        if (ostr) {
            std::cout << "Writing to file. Type a dot (.) to end.\n";
            std::streambuf * pbuf = ostr.rdbuf();
            do {
                ch = std::cin.get();
                pbuf->sputc(ch);
            } while (ch!='.');
            ostr.close();
        }
    }

    {
        const char sentence[]= "Sample sentence";

        std::ofstream ostr ("test.txt");
        if (ostr) {
            std::streambuf * pbuf = ostr.rdbuf();
            pbuf->sputn (sentence,sizeof(sentence)-1);
            ostr.close();
        }
    }

    return 0;
}
