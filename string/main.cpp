#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include <cstddef>

using namespace std::literals;

void SplitFilename (const std::string& str) {
    std::cout << "Splitting: " << str << '\n';
    std::size_t found = str.find_last_of("/\\");
    std::cout << " path: " << str.substr(0,found) << '\n';
    std::cout << " file: " << str.substr(found+1) << '\n';
}

int main () {
    {
        std::string s0("initial string");
        std::string s1;
        std::string s2(s0);
        std::string s3(s0, 8, 3);
        std::string s4("A character sequence", 6);
        std::string s5("Another character sequence");
        std::string s6a(10, 'x');
        std::string s6b(10, 42);
        std::string s7(s0.begin(), s0.begin() + 7);

        std::cout << "s1: " << s1 << "\ns2: " << s2 << "\ns3: " << s3;
        std::cout << "\ns4: " << s4 << "\ns5: " << s5 << "\ns6a: " << s6a;
        std::cout << "\ns6b: " << s6b << "\ns7: " << s7 << '\n';
    }

    {
        std::string str1, str2, str3;
        str1 = "Test string: ";
        str2 = 'abc';
        str3 = str1 + str2;

        std::cout << str3  << '\n';
    }

    {
        std::string str ("Tutorials point");
        for ( std::string::iterator it=str.begin(); it!=str.end(); ++it)
            std::cout << *it;
        std::cout << '\n';
    }

    {
        std::string str ("Tutorials Point...");
        for (std::string::reverse_iterator rit=str.rbegin(); rit!=str.rend(); ++rit)
            std::cout << *rit;
    }

    {
        std::string str ("Tutorials Point");
        for (auto it=str.cbegin(); it!=str.cend(); ++it)
            std::cout << *it;
        std::cout << '\n';
    }

    {
        std::string str ("Tutorials Point");
        for (auto rit=str.crbegin(); rit!=str.crend(); ++rit)
            std::cout << *rit;
        std::cout << '\n';
    }

    {
        std::string str ("Sairaqmkrishna Mammahe");
        std::cout << "The size of str is " << str.size() << " bytes.\n";
    }

    {
        std::string str ("Sairamkrishna Mammahe");
        std::cout << "The size of str is " << str.length() << " bytes.\n";
    }

    {
        std::string str ("Sairamkrishna");
        std::cout << "max_size: " << str.max_size() << "\n";
    }

    {
        std::string str ("Sairamkrishna Mammahe");
        std::cout << str << '\n';

        unsigned sz = str.size();

        str.resize (sz+2,'+');
        std::cout << str << '\n';

        str.resize (14);
        std::cout << str << '\n';
    }

    {
        std::string str ("Sairamkrishna Mammahe");
        std::cout << "capacity: " << str.capacity() << "\n";
    }

    {
        std::string str;

        std::ifstream file ("test.txt",std::ios::in|std::ios::ate);
        if (file) {
            std::ifstream::streampos filesize = file.tellg();
            str.reserve(filesize);

            file.seekg(0);
            while (!file.eof()) {
                str += file.get();
            }
            std::cout << str;
        }
    }

    {
        char c;
        std::string str;
        std::cout << "Please type some lines of text. Enter a start (*) to finish:\n";
        do {
            c = std::cin.get();
            str += c;
            if (c=='\n') {
                std::cout << str;
                str.clear();
            }
        } while (c!='*');
    }

    {
        std::string content;
        std::string line;
        std::cout << "Please introduce a text. Enter an empty line to finish:\n";
        do {
            getline(std::cin,line);
            content += line + '\n';
        } while (!line.empty());
        std::cout << "The text you introduced above was:\n" << content;
    }

    {
        std::string str (500,'x');
        std::cout << "1. capacity of str: " << str.capacity() << '\n';

        str.resize(10);
        std::cout << "2. capacity of str: " << str.capacity() << '\n';

        str.shrink_to_fit();
        std::cout << "3. capacity of str: " << str.capacity() << '\n';
    }

    {
        std::string str ("Sairamkrishna Mammahe");
        for (int i=0; i<str.length(); ++i) {
            std::cout << str[i];
        }
    }

    {
        std::string str ("Sairamkrishna Mammahe");
        for (unsigned i=0; i<str.length(); ++i) {
            std::cout << str.at(i);
        }
    }

    {
        std::string str ("sairamkrishna mammahe.");
        str.back() = '!';
        std::cout << str << '\n';
    }

    {
        std::string str ("Sairamkrishna Mammahe");
        str.front() = 'A';
        std::cout << str << '\n';
    }

    {
        std::string name ("Sairamkrishna");
        std::string family ("Mammahe");
        name += " Prasad. ";
        name += family;
        name += '\n';

        std::cout << name;
    }

    {
        std::string str;
        std::string str2="Writing ";
        std::string str3="print 10 and then 5 more";

        str.append(str2);
        str.append(str3,6,3);
        str.append("dots are cool",5);
        str.append("here: ");
        str.append(10u,'.');
        str.append(str3.begin()+8,str3.end());
        // str.append<int>(5,0x2E);
        std::cout << str << '\n';
    }

    {
        std::string str;
        std::ifstream file ("sample.txt",std::ios::in);
        if (file) {
            while (!file.eof()) str.push_back(file.get());
        }
        std::cout << str << '\n';
    }

    {
        std::string str;
        std::string base="Sairamkrishna is a one of the tech person in tutorialspoint.";

        str.assign(base);
        std::cout << str << '\n';

        str.assign(base,10,9);
        std::cout << str << '\n';

        str.assign("pangrams are cool",7);
        std::cout << str << '\n';

        str.assign("c-string");
        std::cout << str << '\n';

        str.assign(10,'*');
        std::cout << str << '\n';

        // str.assign<int>(10,0x2D);
        std::cout << str << '\n';

        str.assign(base.begin()+16,base.end()-12);
        std::cout << str << '\n';
    }

    {
        std::string s = "xmplr";

        s.insert(0, 1, 'E');
        assert("Exmplr" == s);

        s.insert(2, "e");
        assert("Exemplr" == s);

        s.insert(6, "a"s);
        assert("Exemplar" == s);

        s.insert(8, " is an example string."s, 0, 14);
        assert("Exemplar is an example" == s);

        s.insert(s.cbegin() + s.find_first_of('n') + 1, ':');
        assert("Exemplar is an: example" == s);

        s.insert(s.cbegin() + s.find_first_of(':') + 1, 2, '=');
        assert("Exemplar is an:== example" == s);
        {
            std::string seq = " string";
            s.insert(s.begin() + s.find_last_of('e') + 1,
                     std::begin(seq), std::end(seq));
            assert("Exemplar is an:== example string" == s);
        }

        s.insert(s.cbegin() + s.find_first_of('g') + 1, { '.' });
        assert("Exemplar is an:== example string." == s);
    }

    {
        std::string str ("sairamkrishna Mammahe is a one of the tech person.");
        std::cout << str << '\n';

        str.erase (10,8);
        std::cout << str << '\n';

        str.erase (str.begin()+9);
        std::cout << str << '\n';

        str.erase (str.begin()+5, str.end()-9);
        std::cout << str << '\n';
    }

    {
        std::string base="this is a test string.";
        std::string str2="n example";
        std::string str3="sample phrase";
        std::string str4="useful.";

        std::string str=base;
        str.replace(9,5,str2);
        str.replace(19,6,str3,7,6);
        str.replace(8,10,"just a");
        str.replace(8,6,"a shorty",7);
        str.replace(22,1,3,'!');

        str.replace(str.begin(),str.end()-3,str3);
        str.replace(str.begin(),str.begin()+6,"replace");
        str.replace(str.begin()+8,str.begin()+14,"is coolness",7);
        str.replace(str.begin()+12,str.end()-4,4,'o');
        str.replace(str.begin()+11,str.end(),str4.begin(),str4.end());
        std::cout << str << '\n';
    }

    {
        std::string buyer ("money");
        std::string seller ("goods");

        std::cout << "Before the swap, buyer has " << buyer;
        std::cout << " and seller has " << seller << '\n';

        seller.swap (buyer);

        std::cout << " After the swap, buyer has " << buyer;
        std::cout << " and seller has " << seller << '\n';
    }

    {
        std::string str ("sairamkrishna Mammahe");
        str.pop_back();
        std::cout << str << '\n';
    }

    {
        std::string str ("Please divide this sentance into parts");

        char * cstr = new char [str.length()+1];
        std::strcpy (cstr, str.c_str());

        char * p = std::strtok (cstr," ");
        while (p!=0) {
            std::cout << p << '\n';
            p = std::strtok(NULL," ");
        }

        delete[] cstr;
    }

    {
        int length;

        std::string str = "sairamkrishna mammahe";
        char* cstr = "sairamkrishna mammahe";

        if ( str.length() == std::strlen(cstr) ) {
            std::cout << "str and cstr have the same length.\n";

            if ( memcmp (cstr, str.data(), str.length() ) == 0 )
                std::cout << "str and cstr have the same content.\n";
        }
    }

    {
        char buffer[20];
        std::string str ("Sairamkrishna Mammahe...");
        std::size_t length = str.copy(buffer,6,5);
        buffer[length]='\0';
        std::cout << "buffer contains: " << buffer << '\n';
    }

    {
        std::string str ("sairamkrishna Mammahe is a tech person in tutorialspoint.com.");
        std::string str2 ("needle");

        std::size_t found = str.find(str2);
        if (found!=std::string::npos)
            std::cout << "first 'needle' found at: " << found << '\n';

        found=str.find("needles are small",found+1,6);
        if (found!=std::string::npos)
            std::cout << "second 'needle' found at: " << found << '\n';

        found=str.find("haystack");
        if (found!=std::string::npos)
            std::cout << "'haystack' also found at: " << found << '\n';

        found=str.find('.');
        if (found!=std::string::npos)
            std::cout << "Period found at: " << found << '\n';

        str.replace(str.find(str2),str2.length(),"preposition");
        std::cout << str << '\n';
    }

    {
        std::string str ("sairamkrishna mammahe is a one of the tech person in tutorialspoint.com");
        std::string key ("mammahe");

        std::size_t found = str.rfind(key);
        if (found!=std::string::npos)
            str.replace (found,key.length(),"tech");

        std::cout << str << '\n';
    }

    {
        std::string str ("It replaces the vowels in this sentence by asterisks.");
        std::size_t found = str.find_first_of("aeiou");
        while (found!=std::string::npos) {
            str[found]='*';
            found=str.find_first_of("aeiou",found+1);
        }

        std::cout << str << '\n';
    }

    {
        std::string str1 ("/usr/bin/man");
        std::string str2 ("c:\\windows\\winhelp.exe");

        SplitFilename (str1);
        SplitFilename (str2);
    }

    {
        std::string str ("It looks for non-alphabetic characters...");

        std::size_t found = str.find_first_not_of("abcdefghijklmnopqrstuvwxyz ");

        if (found!=std::string::npos) {
            std::cout << "The first non-alphabetic character is " << str[found];
            std::cout << " at position " << found << '\n';
        }
    }

    {
        std::string str ("It erases trailing white-spaces   \n");
        std::string whitespaces (" \t\f\v\n\r");

        std::size_t found = str.find_last_not_of(whitespaces);
        if (found!=std::string::npos)
            str.erase(found+1);
        else
            str.clear();

        std::cout << '[' << str << "]\n";
    }

    {
        std::string str="Tutorialspoit is a one the best site in the world, hope so it will move same .";

        std::string str2 = str.substr (3,5);

        std::size_t pos = str.find("live");

        std::string str3 = str.substr (pos);

        std::cout << str2 << ' ' << str3 << '\n';
    }

    {
        std::string str1 ("green mango");
        std::string str2 ("red mango");

        if (str1.compare(str2) != 0)
            std::cout << str1 << " is not " << str2 << '\n';

        if (str1.compare(6,5,"mango") == 0)
            std::cout << "still, " << str1 << " is an mango\n";

        if (str2.compare(str2.size()-5,5,"mango") == 0)
            std::cout << "and " << str2 << " is also an mango\n";

        if (str1.compare(6,5,str2,4,5) == 0)
            std::cout << "therefore, both are mangos\n";
    }

    return 0;
}