#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

void foo() {
    std::cout << " foo is executing concurrently...\n";
}

void bar(int x) {
    std::cout << " bar is executing concurrently...\n";
}

void f1(int n) {
    for (int i = 0; i < 5; ++i) {
        std::cout << "1st Thread executing\n";
        ++n;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

void f2(int& n) {
    for (int i = 0; i < 5; ++i) {
        std::cout << "2nd Thread executing\n";
        ++n;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

void foo2() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void foo3() {
    std::this_thread::sleep_for(std::chrono::seconds(2));
}

void foo4() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void bar4() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void independentThread() {
    std::cout << "Starting thread.\n";
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "Exiting previous thread.\n";
}

void threadCaller() {
    std::cout << "Starting thread caller.\n";
    std::thread t(independentThread);
    t.detach();
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Exiting thread caller.\n";
}

int main() {
    {
        std::thread first(foo);
        std::thread second(bar, 0);

        std::cout << "main, foo and bar now execute concurrently...\n";

        first.join();
        second.join();

        std::cout << "foo and bar completed.\n";
    }

    {
        int n = 0;
        std::thread t1;
        std::thread t2(f1, n + 1);
        std::thread t3(f2, std::ref(n));
        std::thread t4(std::move(t3));
        t2.join();
        t4.join();
        std::cout << "Final value of n is " << n << '\n';
    }

    {
        std::thread sample(foo2);
        std::thread::id sample_id = sample.get_id();

        std::thread sample2(foo2);
        std::thread::id sample2_id = sample2.get_id();

        std::cout << "sample's id: " << sample_id << '\n';
        std::cout << "sample2's id: " << sample2_id << '\n';

        sample.join();
        sample2.join();
    }

    {
        std::thread t;
        std::cout << "before joinable: " << t.joinable() << '\n';

        t = std::thread(foo3);
        std::cout << "after joinable: " << t.joinable() << '\n';

        t.join();
        std::cout << "after joining, joinable: " << t.joinable() << '\n';
    }

    {
        std::cout << "starting helper...\n";
        std::thread helper1(foo4);

        std::cout << "starting another helper...\n";
        std::thread helper2(bar4);

        std::cout << "waiting for helpers to finish..." << std::endl;
        helper1.join();
        helper2.join();

        std::cout << "done!\n";
    }

    {
        threadCaller();
        std::this_thread::sleep_for(std::chrono::seconds(5));
    }

    return 0;
}