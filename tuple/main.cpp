#include <iostream>
#include <utility>
#include <tuple>
#include <functional>
#include <string>

void print_pack (std::tuple<std::string&&,int&&> pack) {
    std::cout << std::get<0>(pack) << ", " << std::get<1>(pack) << '\n';
}

int main () {
    {
        std::tuple<int, char> first;
        std::tuple<int, char> second(first);
        std::tuple<int, char> third(std::make_tuple(20, 'b'));
        std::tuple<long, char> fourth(third);
        std::tuple<int, char> fifth(10, 'a');
        std::tuple<int, char> sixth(std::make_pair(30, 'c'));

        std::cout << "fourth contains: " << std::get<0>(sixth);
        std::cout << " and " << std::get<1>(fourth) << '\n';
    }

    {
        std::pair<int,char> mypair (0,' ');

        std::tuple<int,char> a (10,'x');
        std::tuple<long,char> b, c;

        b = a;
        c = std::make_tuple (100L,'Y');
        a = c;
        c = std::make_tuple (100,'z');
        a = mypair;
        a = std::make_pair (2,'b');

        std::cout << "a contains: " << std::get<0>(a);
        std::cout << " and " << std::get<1>(a) << '\n';
    }

    {
        std::tuple<int,char> a (50,'a');
        std::tuple<int,char> b (200,'b');

        a.swap(b);

        std::cout << "a contains: " << std::get<0>(a);
        std::cout << " and " << std::get<1>(a) << '\n';
    }

    {
        std::tuple<int,char> a (100,'x');
        std::tuple<char,char> b (100,'x');
        std::tuple<char,char> c (100,'y');

        if (a==b) std::cout << "a and b are equal\n";
        if (b!=c) std::cout << "b and c are not equal\n";
        if (b<c) std::cout << "b is less than c\n";
        if (c>a) std::cout << "c is greater than a\n";
        if (a<=c) std::cout << "a is less than or equal to c\n";
        if (c>=b) std::cout << "c is greater than or equal to b\n";
    }

    {
        std::tuple<int,char> a (100,'a');
        std::tuple<int,char> b (200,'b');

        swap(a,b);

        std::cout << "a contains: " << std::get<0>(a) << " and " << std::get<1>(a) << '\n';
    }

    {
        std::tuple<int,char> mytuple (10,'x');

        std::get<0>(mytuple) = 20;

        std::cout << "tuple contains: ";
        std::cout << std::get<0>(mytuple) << " and " << std::get<1>(mytuple);
        std::cout << std::endl;
    }

    {
        std::tuple<int,int,char,double> mytuple (100,900,'a',3.14);

        std::cout << "tuple has ";
        std::cout << std::tuple_size<decltype(mytuple)>::value;
        std::cout << " elements." << '\n';
    }

    {
        auto mytuple = std::make_tuple (100,'x');

        std::tuple_element<0,decltype(mytuple)>::type first = std::get<0>(mytuple);
        std::tuple_element<1,decltype(mytuple)>::type second = std::get<1>(mytuple);

        std::cout << "tuple contains: " << first << " and " << second << '\n';
    }

    {
        auto first = std::make_tuple (10,'a');

        const int a = 0; int b[3];
        auto second = std::make_tuple (a,b);

        auto third = std::make_tuple (std::ref(a),"abc");

        std::cout << "third contains: " << std::get<0>(third);
        std::cout << " and " << std::get<1>(third);
        std::cout << std::endl;
    }

    {
        std::string str ("Tutorialspoint.com");
        print_pack (std::forward_as_tuple(str+" sairamkrishna",25));
        print_pack (std::forward_as_tuple(str+" Gopal",22));
        print_pack (std::forward_as_tuple(str+" Ram",30));
    }

    {
        int myint;
        char mychar;

        std::tuple<int,float,char> mytuple;

        mytuple = std::make_tuple (10, 2.6, 'a');

        std::tie (myint, std::ignore, mychar) = mytuple;

        std::cout << "myint contains: " << myint << '\n';
        std::cout << "mychar contains: " << mychar << '\n';
    }

    {
        std::tuple<float,std::string> mytuple (3.14,"pi");
        std::pair<int,char> mypair (100,'x');

        auto myauto = std::tuple_cat ( mytuple, std::tuple<int,char>(mypair) );

        std::cout << "myauto contains: " << '\n';
        std::cout << std::get<0>(myauto) << '\n';
        std::cout << std::get<1>(myauto) << '\n';
        std::cout << std::get<2>(myauto) << '\n';
        std::cout << std::get<3>(myauto) << '\n';
    }

    {
        std::tuple<int,char> mytuple (100,'x');

        std::get<0>(mytuple) = 200;

        std::cout << "tuple contains: ";
        std::cout << std::get<0>(mytuple) << " and " << std::get<1>(mytuple);
        std::cout << std::endl;
    }

    return 0;
}