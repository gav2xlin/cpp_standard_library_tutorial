#include <iostream>
#include <typeinfo>

struct Base {};
struct Derived : Base {};
struct Poly_Base {virtual void Member(){}};
struct Poly_Derived: Poly_Base {};

typedef int my_int_type;

class person {
public:

    person(std::string&& n) : _name(n) {}
    virtual const std::string& name() const{ return _name; }

private:

    std::string _name;
};

class employee : public person {
public:

    employee(std::string&& n, std::string&& p) :
            person(std::move(n)), _profession(std::move(p)) {}

    const std::string& profession() const { return _profession; }

private:

    std::string _profession;
};

void somefunc(const person& p) {
    if(typeid(employee) == typeid(p)) {
        std::cout << p.name() << " is an employee ";
        auto& emp = dynamic_cast<const employee&>(p);
        std::cout << "who works in " << emp.profession() << '\n';
    }
}

struct Base2 { virtual ~Base2() = default; };
struct Derived2 : Base2 {};

struct Foo { virtual ~Foo() {} };
struct Bar { virtual ~Bar() {} };

struct S {
    virtual void f();
};

int main() {
    {
        std::cout << std::boolalpha;

        std::cout << "int vs my_int_type: ";
        std::cout << (typeid(int) == typeid(my_int_type)) << '\n';

        std::cout << "Base vs Derived: ";
        std::cout << (typeid(Base) == typeid(Derived)) << '\n';

        Base *pbase = new Derived;

        std::cout << "Base vs *pbase: ";
        std::cout << (typeid(Base) == typeid(*pbase)) << '\n';

        Poly_Base *ppolybase = new Poly_Derived;

        std::cout << "Poly_Base vs *ppolybase: ";
        std::cout << (typeid(Poly_Base) == typeid(*ppolybase)) << '\n';
    }

    {
        employee paul("sairamkrishna","tutorialspoint");
        somefunc(paul);
    }

    {
        if ( typeid(int).before(typeid(char)) )
            std::cout << "int goes before char while implementation.\n";
        else
            std::cout << "char goes before int while implementation.\n";
    }

    {
        Base2 b1;
        Derived2 d1;

        const Base2 *pb = &b1;
        std::cout << typeid(*pb).name() << '\n';
        pb = &d1;
        std::cout << typeid(*pb).name() << '\n';
    }

    {
        Bar b;
        try {
            Foo& f = dynamic_cast<Foo&>(b);
        } catch(const std::bad_cast& e) {
            std::cout << e.what() << '\n';
        }
    }

    {
        S* p = nullptr;
        try {
            std::cout << typeid(*p).name() << '\n';
        } catch(const std::bad_typeid& e) {
            std::cout << e.what() << '\n';
        }
    }

    return 0;
}