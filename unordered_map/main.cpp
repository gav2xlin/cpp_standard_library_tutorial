#include <iostream>
#include <unordered_map>
#include <string>

using namespace std;

int main(void) {
    {
        unordered_map<char, int> um;

        cout << "Size of unordered_map = " << um.size() << endl;
    }

    {
        unordered_map<char, int> um1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        unordered_map<char, int>um2(um1);

        cout << "Unordered_map contains following elements" << endl;

        for (auto it = um2.begin(); it != um2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        unordered_map<char, int>um2(move(um1));

        cout << "Unordered_map contains following elements" << endl;

        for (auto it = um2.begin(); it != um2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        unordered_map<char, int>um2(um1.begin(), um2.end());

        cout << "Unordered_map contains following elements" << endl;

        for (auto it = um2.begin(); it != um2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Value of key um['a'] = " << um.at('a') << endl;

        try {
            um.at('z');
        } catch(const out_of_range &e) {
            cerr << "Exception at " << e.what() << endl;
        }
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered map contains following elements: " << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        for (int i = 0; i < um.bucket_count(); ++i) {
            cout << "Bucket " << i << " contains:" << endl;

            for (auto it = um.begin(i); it != um.end(i); ++it)
                cout << it->first << " = " << it->second << endl;
        }
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        for (auto it = um.begin(); it != um.end(); ++it) {
            cout << "Element " << "[" << it->first  << " : "
                 << it->second << "] " << "is in "
                 << um.bucket(it->first) << " bucket." << endl;
        }
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Number of buckets = " << um.bucket_count() << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        for (int i = 0; i < um.bucket_count(); ++i)
            cout << "Bucket " << i << " contains "
                 << um.bucket_size(i) << " elements." << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.cbegin(); it != um.cend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        for (int i = 0; i < um.bucket_count(); ++i) {
            cout << "Bucket " << i << " contains:" << endl;

            for (auto it = um.cbegin(i); it != um.cend(i); ++it)
                cout << it->first << " = " << it->second << endl;
        }
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Initial size of unordered map = " << um.size() << endl;

        um.clear();

        cout << "Size of unordered map after clear operation = " << um.size() << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        if (um.count('a') == 1) {
            cout << "um['a'] = " << um.at('a') << endl;
        }

        if (um.count('z') == 0) {
            cout << "Value not present for key um['z']" << endl;
        }
    }

    {
        unordered_map<char, int> um;

        um.emplace('a', 1);
        um.emplace('b', 2);
        um.emplace('c', 3);
        um.emplace('d', 4);
        um.emplace('e', 5);

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        um.emplace_hint(um.end(), 'e', 5);
        um.emplace_hint(um.begin(), 'a', 1);

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.cbegin(); it != um.cend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um;

        if (um.empty())
            cout << "Unordered map is empty" << endl;

        um.emplace('a', 1);

        if (!um.empty())
            cout << "Unordered map is not empty" << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        auto ret = um.equal_range('b');

        cout << "Lower bound is " << ret.first->first
             << " = "<< ret.first->second << endl;
        cout << "Upper bound is " << ret.second->first
             << " = " << ret.second->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered map contains following elements before erase operation"
             << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second
                 << endl;

        um.erase(um.begin());

        cout << endl;
        cout << "Unordered map contains following elements after erase operation"
             << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered map contains following elements before erase operation" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        um.erase('a');

        cout << endl;
        cout << "Unordered map contains following elements after erase operation" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered_multimap contains following elements "
             << "before remove operation" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        auto it = umm.begin();
        ++it; ++it;

        umm.erase(umm.begin(), it);

        cout << "Unordered_multimap contains following elements "
             << "after remove operation" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        auto it = um.find('d');

        cout << "Iterator points to " << it->first
             << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um;
        pair<const char, int> *p;

        p = um.get_allocator().allocate(5);

        cout << "Allocated size = " <<  sizeof(*p) * 5 << endl;
    }

    {
        unordered_map <string, string> um;

        auto fun = um.hash_function();

        cout << "Hash function for a = " << fun("a") << endl;
        cout << "Hash function for A = " << fun("A") << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
        };

        um.insert(pair<char, int>('d', 4));
        um.insert(pair<char, int>('e', 5));

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
        };

        um.insert(move(pair<char, int>('d', 4)));
        um.insert(move(pair<char, int>('e', 5)));

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        um.insert(um.begin(), pair<char, int>('a', 1));
        um.insert(um.end(), pair<char, int>('e', 5));

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        um.insert(move(um.begin()), pair<char, int>('a', 1));
        um.insert(move(um.end()), pair<char, int>('e', 5));

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        unordered_map<char, int> um2;

        um2.insert(um1.begin(), um1.end());

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um2.begin(); it != um2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        um.insert({{'a', 1}, {'e', 5}});

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um.begin(); it != um.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<string,string> um;

        bool case_insensitive = um.key_eq()("jerry","JERRY");

        cout << "mymap.key_eq() is ";
        cout << ( case_insensitive ? "case insensitive" : "case sensitive" );
        cout << endl;
    }

    {
        unordered_map<char, int> um;

        cout << "load_factor of unordered_map = " << um.load_factor() << endl;
    }

    {
        unordered_map<char, int> um;

        cout << "max_bucket_count of unordered_map = " << um.max_bucket_count() << endl;
    }

    {
        unordered_map<char, int> um;

        cout << "max_load_factor of unordered_map = "
             << um.max_load_factor() << endl;
    }

    {
        unordered_map<char, int> um;

        cout << "Initial max_load_factor = " << um.max_load_factor() << endl;

        um.max_load_factor(2);

        cout << "max_load_factor after set operation = " << um.max_load_factor() << endl;
    }

    {
        unordered_map<char, int> um;

        cout << "max_size of unordered_map = " << um.max_size() << endl;
    }

    {
        unordered_map<char, int> um1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        unordered_map<char, int> um2;

        um2 = um1;

        cout << "Unordered map contains following elements: " << endl;

        for (auto it = um2.cbegin(); it != um2.cend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        unordered_map<char, int> um2;

        um2 = move(um1);

        cout << "Unordered map contains following elements: " << endl;

        for (auto it = um2.cbegin(); it != um2.cend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um;

        um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered map contains following elements: " << endl;

        for (auto it = um.cbegin(); it != um.cend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        cout << "Unordered map contains following elements: " << endl;

        cout << "um['a'] = " << um['a'] << endl;
        cout << "um['b'] = " << um['b'] << endl;
        cout << "um['c'] = " << um['c'] << endl;
        cout << "um['d'] = " << um['d'] << endl;
        cout << "um['e'] = " << um['e'] << endl;
    }

    {
        unordered_map<char, int> um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered map contains following elements: " << endl;

        cout << "um['a'] = " << move(um['a']) << endl;
        cout << "um['b'] = " << move(um['b']) << endl;
        cout << "um['c'] = " << move(um['c']) << endl;
        cout << "um['d'] = " << move(um['d']) << endl;
        cout << "um['e'] = " << move(um['e']) << endl;
    }

    {
        unordered_map<char, int> mymap;

        cout << "Initial bucket_count: " << mymap.bucket_count() << endl;

        mymap.rehash(20);

        cout << "Current bucket_count: " << mymap.bucket_count() << endl;
    }

    {
        unordered_map<char, int> um;

        cout << "Initial bucket count = " << um.bucket_count() << endl;

        um.reserve(5);

        cout << "Bucket count after reserve = "
             << um.bucket_count() << endl;
    }

    {
        unordered_map<char, int> um;

        cout << "Initial size of unordered map = " << um.size() << endl;

        um = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Size of unordered map after inserting elements = " << um.size() << endl;
    }

    {
        unordered_map<char, int> um1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        unordered_map<char, int> um2;

        um1.swap(um2);

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um2.begin(); it != um2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_map<char, int> um1;
        unordered_map<char, int> um2;

        if (um1 == um2)
            cout << "Both unordered_maps are equal" << endl;

        um1.emplace('a', 1);

        if (!(um1 == um2))
            cout << "Both unordered_maps are not equal" << endl;
    }

    {
        unordered_map<char, int> um1;
        unordered_map<char, int> um2;

        um1.emplace('a', 1);

        if (um1 != um2)
            cout << "Both unordered_maps not are equal" << endl;

        um1 = um2;

        if (!(um1 != um2))
            cout << "Both unordered_maps are equal" << endl;
    }

    {
        unordered_map<char, int> um1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        unordered_map<char, int> um2;

        swap(um1, um2);

        cout << "Unordered map contains following elements" << endl;

        for (auto it = um2.begin(); it != um2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap <char, int> um;

        cout << "Size of unordered multimap = " << um.size() << endl;
    }

    {
        unordered_multimap<char, int> umm1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        unordered_multimap<char, int> umm2(umm1);

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm2.begin(); it != umm2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        unordered_multimap<char, int> umm2(move(umm1));

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm2.begin(); it != umm2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        unordered_multimap<char, int> umm2(umm1.begin(), umm1.end());

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm2.begin(); it != umm2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        cout << "Unordered multimap contains following elements" << endl;

        for (int i = 0; i < umm.bucket_count(); ++i) {
            cout << "Bucket " << i << " contains:" << endl;

            for (auto it = umm.begin(i); it != umm.end(i); ++it)
                cout << it->first << " = " << it->second << endl;
        }
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it) {
            cout << "Element " << "[" << it->first  << " : "
                 << it->second << "] " << "is in "
                 << umm.bucket(it->first) << " bucket." << endl;
        }
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Number of buckets = " << umm.bucket_count() << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        for (int i = 0; i < umm.bucket_count(); ++i)
            cout << "Bucket " << i << " contains "
                 << umm.bucket_size(i) << " elements." << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm.cbegin(); it != umm.cend(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        for (int i = 0; i < umm.bucket_count(); ++i) {
            cout << "Bucket " << i << " contains:" << endl;

            for (auto it = umm.cbegin(i); it != umm.cend(i); ++it)
                cout << it->first << " = " << it->second << endl;
        }
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Initial size of unordered multimap = " << umm.size() << endl;

        umm.clear();

        cout << "Size of unordered multimap after clear operaion = " << umm.size()
             << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        umm.emplace('b', 2);
        umm.emplace('c', 3);

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        umm.emplace_hint(umm.begin(), 'b', 2);
        umm.emplace_hint(umm.end(), 'e', 3);

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm;

        if (umm.empty())
            cout << "Unordered multimap is empty." << endl;

        umm.emplace_hint(umm.begin(), 'a', 1);
        umm.emplace_hint(umm.end(), 'b', 2);

        if (!umm.empty())
            cout << "Unordered multimap is not empty." << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'b', 3},
                {'b', 4},
                {'c', 5}
        };

        auto ret = umm.equal_range('b');

        cout << "Elements associated with key 'b': ";

        for (auto it = ret.first; it != ret.second; ++it)
            cout << it->second << " ";
        cout << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered_multimap contains following elements "
             << "before remove operation" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        cout << endl;

        umm.erase(umm.begin());

        cout << "Unordered_multimap contains following elements "
             << "after remove operation" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered_multimap contains following elements "
             << "before remove operation" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        cout << endl;

        umm.erase('e');

        cout << "Unordered_multimap contains following elements "
             << "after remove operation" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        cout << "Unordered_multimap contains following elements "
             << "before remove operation" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;

        auto it = umm.begin();
        ++it; ++it;

        umm.erase(umm.begin(), it);

        cout << "Unordered_multimap contains following elements "
             << "after remove operation" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };

        auto it = umm.find('b');

        cout << "Iterator point to " << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm;
        pair<const char, int> *p;

        p = umm.get_allocator().allocate(5);

        cout << "Allocated size = " <<  sizeof(*p) * 5 << endl;
    }

    {
        unordered_multimap <string, string> umm;

        auto fun = umm.hash_function();

        cout << "Hash function for a = " << fun("a") << endl;
        cout << "Hash function for A = " << fun("A") << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        auto pos = umm.insert(pair<char, int>('e', 5));

        cout << "After inserting new element iterator poinst to "
             << pos->first << " = " << pos->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        auto pos = umm.insert(move(pair<char, int>('e', 5)));

        cout << "After inserting new element iterator points to "
             << pos->first << " = " << pos->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        auto pos = umm.insert(umm.begin(), pair<char, int>('a', 1));

        cout << "After inserting new element iterator poinst to "
             << pos->first << " = " << pos->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        auto pos = umm.insert(umm.begin(), move(pair<char, int>('a', 1)));

        cout << "After inserting new element iterator poinst to "
             << pos->first << " = " << pos->second << endl;
    }

    {
        unordered_multimap<char, int> umm1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };
        unordered_multimap<char, int> umm2;

        umm2.insert(umm1.begin(), umm1.end());

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm2.begin(); it != umm2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
        };

        umm.insert({{'d', 4}, {'e', 5}});

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<string,string> umm;

        bool case_insensitive = umm.key_eq()("jerry","JERRY");

        cout << "mymap.key_eq() is ";
        cout << ( case_insensitive ? "case insensitive" : "case sensitive" );
        cout << endl;
    }

    {
        unordered_multimap<char, int> umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
        };

        cout << "Load factor = " << umm.load_factor() << endl;
    }

    {
        unordered_multimap<string,string> umm;

        cout << "max_bucket_count = " << umm.max_bucket_count() << endl;
    }

    {
        unordered_multimap<char, int> umm;

        cout << "max_load_factor = " << umm.max_load_factor() << endl;
    }

    {
        unordered_multimap<char, int> umm;

        cout << "Initial max_load_factor = " <<umm.max_load_factor()
             << endl;

        umm.max_load_factor(2);

        cout << "max_load_factor after set operation = "
             << umm.max_load_factor() << endl;
    }

    {
        unordered_multimap<char, int> umm;

        cout << "max_size = " << umm.max_size() << endl;
    }

    {
        unordered_multimap<char, int> umm1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        unordered_multimap<char, int> umm2;

        umm2 = umm1;

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm2.begin(); it != umm2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        unordered_multimap<char, int> umm2;

        umm2 = move(umm1);

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm2.begin(); it != umm2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm;
        umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm.begin(); it != umm.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm;

        cout << "Initial bucket_count: " << umm.bucket_count() << endl;

        umm.rehash(20);

        cout << "Current bucket_count: " << umm.bucket_count() << endl;
    }

    {
        unordered_multimap<char, int> umm;

        cout << "Initial bucket count = " << umm.bucket_count() << endl;

        umm.reserve(5);

        cout << "Bucket count after reserve = " << umm.bucket_count() << endl;
    }

    {
        unordered_multimap<char, int> umm;

        cout << "Initial size of unordered multimap = " << umm.size()
             << endl;

        umm = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        cout << "Size of unordered multimap after insertion = " << umm.size()
             << endl;
    }

    {
        unordered_multimap<char, int> umm1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5},
        };

        unordered_multimap<char, int> umm2;

        umm1.swap(umm2);

        cout << "Unordered multimap contains following elements" << endl;

        for (auto it = umm2.begin(); it != umm2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    {
        unordered_multimap<char, int> umm1;
        unordered_multimap<char, int> umm2;

        if (umm1 == umm2)
            cout << "Both unordered_multimaps are equal." << endl;

        umm1.emplace('a', 1);

        if (!(umm1 == umm2))
            cout << "Both unordered_multimaps are not equal." << endl;
    }

    {
        unordered_multimap<char, int> umm1;
        unordered_multimap<char, int> umm2;

        umm1.emplace('a', 1);

        if (umm1 != umm2)
            cout << "Both unordered_multimaps are not equal." << endl;

        umm1 = umm2;

        if (!(umm1 != umm2))
            cout << "Both unordered_multimaps are equal." << endl;
    }

    {
        unordered_multimap<char, int> umm1 = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'d', 4},
                {'e', 5}
        };
        unordered_multimap<char, int> umm2;

        swap(umm1, umm2);

        cout << "Unordered_multimap contains following elements" << endl;

        for (auto it = umm2.begin(); it != umm2.end(); ++it)
            cout << it->first << " = " << it->second << endl;
    }

    return 0;
}