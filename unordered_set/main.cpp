#include <iostream>
#include <string>
#include <unordered_set>
#include <array>

template<class T>
T cmerge (T a, T b) {
    T t(a);
    t.insert(b.begin(),b.end());
    return t;
}


int main () {
    {
        std::unordered_set<std::string> first;
        std::unordered_set<std::string> second({"100", "200", "300"});
        std::unordered_set<std::string> third({"400", "500", "600"});
        std::unordered_set<std::string> fourth(second);
        std::unordered_set<std::string> fifth(cmerge(third, fourth));
        std::unordered_set<std::string> sixth(fifth.begin(), fifth.end());

        std::cout << "sixth contains:";
        for (const std::string &x: sixth) std::cout << " " << x;
        std::cout << std::endl;
    }

    {
        std::unordered_set<std::string> first, second, third;
        first = {"100","200","300"};
        second = {"400","500","600"};
        third = cmerge (first, second);
        first = third;

        std::cout << "first contains:";
        for (const std::string& x: first) std::cout << " " << x;
        std::cout << std::endl;
    }

    {
        std::unordered_set<std::string> first = {"sairam","krishna","mammahe"};
        std::unordered_set<std::string> second;
        std::cout << "first " << (first.empty() ? "is empty" : "is not empty" ) << std::endl;
        std::cout << "second " << (second.empty() ? "is empty" : "is not empty" ) << std::endl;
    }

    {
        std::unordered_set<std::string> myset;
        std::cout << "0. size: " << myset.size() << std::endl;

        myset = {"sairamkrishna","mammahe"};
        std::cout << "1. size: " << myset.size() << std::endl;

        myset.insert ("kittuprasad");
        std::cout << "2. size: " << myset.size() << std::endl;

        myset.erase ("tutorialspoint");
        std::cout << "3. size: " << myset.size() << std::endl;
    }

    {
        std::unordered_set<std::string> myset;
        std::cout << "0. size: " << myset.max_size() << std::endl;

        myset = {"milk","potatoes","eggs"};
        std::cout << "1. size: " << myset.max_size() << std::endl;

        myset.insert ("pineapple");
        std::cout << "2. size: " << myset.max_size() << std::endl;

        myset.erase ("milk");
        std::cout << "3. size: " << myset.max_size() << std::endl;
    }

    {
        std::unordered_set<std::string> myset =
                {"100","200","300","400","500","600","700","800"};

        std::cout << "myset contains:";
        for ( auto it = myset.begin(); it != myset.end(); ++it )
            std::cout << " " << *it;
        std::cout << std::endl;

        std::cout << "myset's buckets contain:\n";
        for ( unsigned i = 0; i < myset.bucket_count(); ++i) {
            std::cout << "bucket #" << i << " contains:";
            for ( auto local_it = myset.begin(i); local_it!= myset.end(i); ++local_it )
                std::cout << " " << *local_it;
            std::cout << std::endl;
        }
    }

    {
        std::unordered_set<std::string> myset =
                {"100","200","300","400","500","600","700","800"};

        std::cout << "myset contains:";
        for ( auto it = myset.cbegin(); it != myset.cend(); ++it )
            std::cout << " " << *it;    // cannot modify *it
        std::cout << std::endl;

        std::cout << "myset's buckets contain:\n";
        for ( unsigned i = 0; i < myset.bucket_count(); ++i) {
            std::cout << "bucket #" << i << " contains:";
            for ( auto local_it = myset.cbegin(i); local_it!= myset.cend(i); ++local_it )
                std::cout << " " << *local_it;
            std::cout << std::endl;
        }
    }

    {
        std::unordered_set<std::string> myset = { "sai","ram,","krishna" };

        std::string input;
        std::cout << "Enter the myset char: ";
        getline (std::cin,input);

        std::unordered_set<std::string>::const_iterator got = myset.find (input);

        if ( got == myset.end() )
            std::cout << "not found in myset";
        else
            std::cout << *got << " is in myset";

        std::cout << std::endl;
    }

    {
        std::unordered_set<std::string> myset = { "sairam", "krishna", "prasad" };

        for (auto& x: {"tutorialspoint","sairam","krishna","t-shirt"}) {
            if (myset.count(x)>0)
                std::cout << "myset has " << x << std::endl;
            else
                std::cout << "myset has no " << x << std::endl;
        }
    }

    {
        std::unordered_set<std::string> myset;

        myset.emplace ("kittu");
        myset.emplace ("prasad");
        myset.emplace ("sairamkrishna");

        std::cout << "myset containing:";
        for (const std::string& x: myset) std::cout << " " << x;

        std::cout << std::endl;
    }

    {
        std::unordered_set<std::string> myset = {"sairam","krishna","mammahe"};
        std::array<std::string,2> myarray = {"ram","mammahe"};
        std::string mystring = "krishna";

        myset.insert (mystring);
        myset.insert (mystring);
        myset.insert (myarray.begin(), myarray.end());
        myset.insert ( {"sai","mammahe"} );

        std::cout << "myset contains:";
        for (const std::string& x: myset) std::cout << " " << x;
        std::cout <<  std::endl;
    }

    {
        std::unordered_set<std::string> myset =
                {"USA","Canada","France","UK","Japan","Germany","Italy"};

        myset.erase ( myset.begin() );
        myset.erase ( "France" );
        myset.erase ( myset.find("Japan"), myset.end() );

        std::cout << "myset contains:";
        for ( const std::string& x: myset ) std::cout << " " << x;
        std::cout << std::endl;
    }

    {
        std::unordered_set<std::string> myset =
                { "sai", "ram", "krishna", "prasad" };

        std::cout << "myset contains:";
        for (const std::string& x: myset) std::cout << " " << x;
        std::cout << std::endl;

        myset.clear();
        myset.insert("Tutorialspoint");
        myset.insert("Technical Analyst");
        myset.insert("Hyderabad");

        std::cout << "myset contains:";
        for (const std::string& x: myset) std::cout << " " << x;
        std::cout << std::endl;
    }

    {
        std::unordered_set<std::string>
                first = {"sai","ram","krishna"},
                second  = {"tutorials","point",".com"};

        first.swap(second);

        std::cout << "first:";
        for (const std::string& x: first) std::cout << " " << x;
        std::cout << std::endl;

        std::cout << "second:";
        for (const std::string& x: second) std::cout << " " << x;
        std::cout << std::endl;
    }

    {
        std::unordered_set<std::string> myset =
                {"sai","ram","krishna","prasad","mammahe","tutorials","point","com"};

        unsigned n = myset.bucket_count();

        std::cout << "myset has " << n << " buckets.\n";

        for (unsigned i = 0; i < n; ++i) {
            std::cout << "bucket #" << i << " contains:";
            for (auto it = myset.begin(i); it!=myset.end(i); ++it)
                std::cout << " " << *it;
            std::cout << "\n";
        }
    }

    {
        std::unordered_set<int> myset;

        std::cout << "max_size = " << myset.max_size() << std::endl;
        std::cout << "max_bucket_count = " << myset.max_bucket_count() << std::endl;
        std::cout << "max_load_factor = " << myset.max_load_factor() << std::endl;
    }

    {
        std::unordered_set<std::string> myset =
                { "sai", "ram", "krishna", "prasad", "tutorials", "point" };

        unsigned nbuckets = myset.bucket_count();

        std::cout << "myset has " << nbuckets << " buckets:\n";

        for (unsigned i = 0; i < nbuckets; ++i) {
            std::cout << "bucket #" << i << " has " << myset.bucket_size(i) << " elements.\n";
        }
    }

    {
        std::unordered_set<std::string> myset = {"sai","ram","krishna","prasad"};

        for (const std::string& x: myset) {
            std::cout << x << " is in bucket #" << myset.bucket(x) << std::endl;
        }
    }

    {
        std::unordered_set<int> myset;

        std::cout << "size = " << myset.size() << std::endl;
        std::cout << "bucket_count = " << myset.bucket_count() << std::endl;
        std::cout << "load_factor = " << myset.load_factor() << std::endl;
        std::cout << "max_load_factor = " << myset.max_load_factor() << std::endl;
    }

    {
        std::unordered_set<std::string> myset =
                {"sai", "Ram", "krishna", "prasad", "Bangalore", "india"};

        std::cout << "current max_load_factor: " << myset.max_load_factor() << std::endl;
        std::cout << "current size: " << myset.size() << std::endl;
        std::cout << "current bucket_count: " << myset.bucket_count() << std::endl;
        std::cout << "current load_factor: " << myset.load_factor() << std::endl;

        float z = myset.max_load_factor();
        myset.max_load_factor ( z / 2.0 );
        std::cout << "[max_load_factor halved]" << std::endl;

        std::cout << "new max_load_factor: " << myset.max_load_factor() << std::endl;
        std::cout << "new size: " << myset.size() << std::endl;
        std::cout << "new bucket_count: " << myset.bucket_count() << std::endl;
        std::cout << "new load_factor: " << myset.load_factor() << std::endl;
    }

    {
        std::unordered_set<std::string> myset;

        myset.rehash(12);

        myset.insert("android");
        myset.insert("java");
        myset.insert("html");
        myset.insert("css");
        myset.insert("javascript");

        std::cout << "current bucket_count: " << myset.bucket_count() << std::endl;
    }

    {
        std::unordered_set<std::string> myset;

        myset.reserve(5);

        myset.insert("android");
        myset.insert("java");
        myset.insert("html");
        myset.insert("css");
        myset.insert("wordpress");

        std::cout << "myset contains:";
        for (const std::string& x: myset) std::cout << " " << x;
        std::cout << std::endl;
    }

    {
        typedef std::unordered_set<std::string> stringset;

        stringset myset;

        stringset::hasher fn = myset.hash_function();

        std::cout << "that contains: " << fn ("that") << std::endl;
        std::cout << "than contains: " << fn ("than") << std::endl;
    }

    {
        std::unordered_set<std::string>
                a = {"goole","yahoo","verizon"},
                b = {"goole","verizon","yahoo"},
                c = {"verizon","goole","yahoo","oracle"};

        if (a==b) std::cout << "a and b are equal\n";
        if (b!=c) std::cout << "b and c are not equal\n";
    }

    {
        std::unordered_set<std::string>
                first = {"sai","ram","krishna"},
                second  = {"tutorials","point",".com"};

        first.swap(second);

        std::cout << "first:";
        for (const std::string& x: first) std::cout << " " << x;
        std::cout << std::endl;

        std::cout << "second:";
        for (const std::string& x: second) std::cout << " " << x;
        std::cout << std::endl;
    }

    {
        std::unordered_set<std::string>
                a = {"goole","yahoo","verizon"},
                b = {"goole","verizon","yahoo"},
                c = {"verizon","goole","yahoo","oracle"};

        if (a==b) std::cout << "a and b are equal\n";
        if (b!=c) std::cout << "b and c are not equal\n";
    }

    {
        std::unordered_set<std::string>
                first = {"sai","ram","krishna"},
                second  = {"tutorials","point",".com"};

        first.swap(second);

        std::cout << "first:";
        for (const std::string& x: first) std::cout << " " << x;
        std::cout << std::endl;

        std::cout << "second:";
        for (const std::string& x: second) std::cout << " " << x;
        std::cout << std::endl;
    }

    return 0;
}