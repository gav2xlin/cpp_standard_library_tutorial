#include <iostream>
#include <utility>
#include <vector>
#include <tuple>
#include <cmath>

void overloaded (const int& x) {std::cout << "[It is a lvalue]";}
void overloaded (int&& x) {std::cout << "[It is a rvalue]";}

template <class T> void fn (T&& x) {
    overloaded (x);
    overloaded (std::forward<T>(x));
}

struct Bad {
    Bad() {}
    Bad(Bad&&) {
        std::cout << "Throwing move constructor called\n";
    }
    Bad(const Bad&) {
        std::cout << "Throwing copy constructor called\n";
    }
};

struct Good {
    Good() {}
    Good(Good&&) noexcept {
        std::cout << "Non-throwing move constructor called\n";
    }
    Good(const Good&) noexcept {
        std::cout << "Non-throwing copy constructor called\n";
    }
};

struct A {
    virtual int value() = 0;
};

class B : public A {
    int val_;
public:
    B(int i,int j):val_(i*j){}
    int value() {return val_;}
};

class vector2d {
public:
    double x,y;
    vector2d (double px,double py): x(px), y(py) {}
    double length() const {return std::sqrt(x*x+y*y);}
    bool operator==(const vector2d& rhs) const {return length()==rhs.length();}
    bool operator< (const vector2d& rhs) const {return length()< rhs.length();}
};

int main () {
    {
        int foo[4];
        int bar[] = {100, 200, 300, 400};
        std::swap(foo, bar);

        std::cout << "foo contains:";
        for (int i: foo) std::cout << ' ' << i;
        std::cout << '\n';
    }

    {
        std::pair <int,char> foo;
        std::pair <int,int> bar;

        foo = std::make_pair (1,'A');
        bar = std::make_pair (100,3);

        std::cout << "foo: " << foo.first << ", " << foo.second << '\n';
        std::cout << "bar: " << bar.first << ", " << bar.second << '\n';
    }

    {
        int a;

        std::cout << "calling fn with lvalue: ";
        fn (a);
        std::cout << '\n';

        std::cout << "calling fn with rvalue: ";
        fn (0);
        std::cout << '\n';
    }

    {
        std::string foo = "It is a foo string";
        std::string bar = "It is a bar string";
        std::vector<std::string> myvector;

        myvector.push_back (foo);
        myvector.push_back (std::move(bar));

        std::cout << "myvector contains:";
        for (std::string& x:myvector) std::cout << ' ' << x;
        std::cout << '\n';
    }

    {
        Good g;
        Bad b;
        Good g2 = std::move_if_noexcept(g);
        Bad b2 = std::move_if_noexcept(b);
    }

    {
        decltype(std::declval<A>().value()) a;
        decltype(std::declval<B>().value()) b;
        a = b = B(100,20).value();
        std::cout << a << '\n';
    }

    {
        std::pair < std::string, std::vector<int> >
                foo (
                std::piecewise_construct,
                std::forward_as_tuple("sample"),
                std::forward_as_tuple(2,100)
        );
        std::cout << "foo.first: " << foo.first << '\n';
        std::cout << "foo.second:";
        for (int& x: foo.second) std::cout << ' ' << x;
        std::cout << '\n';
    }

    {
        using namespace std::rel_ops;
        vector2d a (10,10);
        vector2d b (15,20);
        std::cout << std::boolalpha;
        std::cout << "(a>b) is " << (a<b) << '\n';
        std::cout << "(a<b) is " << (a>b) << '\n';
    }

    return 0;
}