#include <iostream>
#include <valarray>
#include <cstddef>
#include <cmath>

int increment (int x) {return ++x;}

int main () {
    {
        int init[] = {10, 50, 30, 60};
        std::valarray<int> first;
        std::valarray<int> second(5);
        std::valarray<int> third(10, 3);
        std::valarray<int> fourth(init, 4);
        std::valarray<int> fifth(fourth);
        std::valarray<int> sixth(fifth[std::slice(1, 2, 1)]);

        std::cout << "sixth sums " << sixth.sum() << '\n';
    }

    {
        int init[]= {10,20,30,40};

        std::valarray<int> foo (init, 4);
        std::valarray<int> bar (25,4);
        bar += foo;

        foo = bar + 10;

        foo -= 10;

        std::valarray<bool> comp = (foo==bar);

        if ( comp.min() == true )
            std::cout << "foo and bar are equal.\n";
        else
            std::cout << "foo and bar are not equal.\n";
    }

    {
        int init[]={0,10,20,30,40};
        std::valarray<int> foo (init,5);

        std::valarray<int> bar = foo.apply(increment);

        std::cout << "foo contains:";
        for (std::size_t n=0; n<bar.size(); n++)
            std::cout << ' ' << bar[n];
        std::cout << '\n';
    }

    {
        int init[]={0,10,20,30,40};

        std::valarray<int> myvalarray (init,5);
        myvalarray = myvalarray.cshift(2);
        myvalarray = myvalarray.cshift(-1);

        std::cout << "valarray contains:";
        for (std::size_t n=0; n<myvalarray.size(); n++)
            std::cout << ' ' << myvalarray[n];
        std::cout << '\n';
    }

    {
        int init[]={10,50,20,90};
        std::valarray<int> myvalarray (init,4);
        std::cout << "The max is " << myvalarray.max() << '\n';
    }

    {
        int init[]={20,40,0,30};
        std::valarray<int> myvalarray (init,4);
        std::cout << "The min is " << myvalarray.min() << '\n';
    }

    {
        std::valarray<int> foo (10);
        std::valarray<int> bar (2,40);

        foo = bar;
        bar = 5;
        foo = bar[std::slice (0,4,1)];

        std::cout << "foo sums " << foo.sum() << '\n';
    }

    {
        std::valarray<int> myarray (10);

        myarray[std::slice(2,3,3)]=10;

        size_t lengths[]={2,2};
        size_t strides[]={6,2};
        myarray[std::gslice(1, std::valarray<size_t>(lengths,2),
                            std::valarray<size_t>(strides,2))]=20;

        std::valarray<bool> mymask (10);
        for (int i=0; i<10; ++i) mymask[i]= ((i%2)==0);
        myarray[mymask] += std::valarray<int>(3,5);

        //indirect:
        size_t sel[]= {2,5,7};
        std::valarray<size_t> myselection (sel,3);
        myarray[myselection]=99;

        std::cout << "myarray: ";
        for (size_t i=0; i<myarray.size(); ++i)
            std::cout << myarray[i] << ' ';
        std::cout << '\n';
    }

    {
        std::valarray<int> myarray (100,50);
        myarray.resize(3);

        std::cout << "myvalarray contains:";
        for (std::size_t n=0; n<myarray.size(); n++)
            std::cout << ' ' << myarray[n];
        std::cout << '\n';
    }

    {
        int init[]={0,10,20,30,40};

        std::valarray<int> myvalarray (init,5);
        myvalarray = myvalarray.shift(2);
        myvalarray = myvalarray.shift(-1);

        std::cout << "myvalarray contains:";
        for (std::size_t n=0; n<myvalarray.size(); n++)
            std::cout << ' ' << myvalarray[n];
        std::cout << '\n';
    }

    {
        std::valarray<int> myvalarray;
        std::cout << "1. After construction: " << myvalarray.size() << '\n';

        myvalarray = std::valarray<int>(15);
        std::cout << "2. After assignment: " << myvalarray.size() << '\n';

        myvalarray.resize(30);
        std::cout << "3. After downsizing: " << myvalarray.size() << '\n';

        myvalarray.resize(100);
        std::cout << "4. After resizing up: " << myvalarray.size() << '\n';
    }

    {
        int init[]={0,10,20,30};
        std::valarray<int> myvalarray (init,4);
        std::cout << "The sum is " << myvalarray.sum() << '\n';
    }

    {
        std::valarray<int> foo {0,10,20,30};
        std::valarray<int> bar {100,200,300};

        foo.swap(bar);

        std::cout << "foo contains:";
        for (auto& x: foo) std::cout << ' ' << x;
        std::cout << '\n';

        std::cout << "bar contains:";
        for (auto& x: bar) std::cout << ' ' << x;
        std::cout << '\n';
    }

    {
        int init[]= {10,20,30,40};

        std::valarray<int> foo (init, 4);
        std::valarray<int> bar (25,4);
        bar += foo;

        foo = bar + 10;

        foo -= 10;

        std::valarray<bool> comp = (foo==bar);

        if ( comp.min() == true )
            std::cout << "foo and bar are equal.\n";
        else
            std::cout << "foo and bar are not equal.\n";
    }

    {
        std::valarray<int> foo {0,10,20,30};
        std::valarray<int> bar {100,200,300};

        foo.swap(bar);

        std::cout << "foo contains:";
        for (auto& x: foo) std::cout << ' ' << x;
        std::cout << '\n';

        std::cout << "bar contains:";
        for (auto& x: bar) std::cout << ' ' << x;
        std::cout << '\n';
    }

    {
        std::valarray<int> sam {0,10,20,30,40};
        // std::valarray<int> sam {10,20,30,40,50};

        std::cout << "sam contains:";
        for (auto it = begin(sam); it!=end(sam); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }

    {
        std::valarray<int> foo (10);
        for (int i=0; i<10; ++i) foo[i]=i;

        std::valarray<int> bar = foo[std::slice(2,3,4)];

        std::cout << "slice(1,5,6):";
        for (std::size_t n=0; n<bar.size(); n++)
            std::cout << ' ' << bar[n];
        std::cout << '\n';
    }

    {
        std::valarray<int> foo (12);
        for (int i=0; i<12; ++i) foo[i]=i;

        std::size_t start=1;
        std::size_t lengths[]= {2,3};
        std::size_t strides[]= {7,2};

        std::gslice mygslice (start,
                              std::valarray<std::size_t>(lengths,2),
                              std::valarray<std::size_t>(strides,2));

        std::valarray<int> bar = foo[mygslice];

        std::cout << "gslice:";
        for (std::size_t n=0; n<bar.size(); n++)
            std::cout << ' ' << bar[n];
        std::cout << '\n';
    }

    {
        int val[] = {15, 23, -1, -40, 7};
        std::valarray<int> foo (val,5);

        std::valarray<int> bar = abs (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {0.10, 0.25, 0.50, 0.75, 1.5};
        std::valarray<double> foo (val,5);

        std::valarray<double> bar = acos (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {0.0, 0.2, 0.5, 0.7, 1.0};
        std::valarray<double> foo (val,5);

        std::valarray<double> bar = asin (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {0.0, 0.25, 0.5, 0.75, 1.0};
        std::valarray<double> foo (val,5);

        std::valarray<double> bar = atan (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double y[] = {0.0, 3.0, -2.0};
        double x[] = {-3.0, 3.0, -1.0};
        std::valarray<double> ycoords (y,3);
        std::valarray<double> xcoords (x,3);

        std::valarray<double> results = atan2 (ycoords,xcoords);

        std::cout << "results:";
        for (std::size_t i=0; i<results.size(); ++i)
            std::cout << ' ' << results[i];
        std::cout << '\n';
    }

    {
        double val[] = {1.5, 3.1, 5.5};
        std::valarray<double> foo (val,3);

        std::valarray<double> bar = cos (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar: ";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {0.1, 1.5, 3.0};
        std::valarray<double> foo (val,3);

        std::valarray<double> bar = cosh (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {1.0, 2.0, 4.0, 8.0};
        std::valarray<double> foo (val,4);

        std::valarray<double> bar = exp (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {5.0, 6.0, 8.0, 9.5};
        std::valarray<double> foo (val,4);

        std::valarray<double> bar = log (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {1.0, 10.0, 100.0, 1000.0};
        std::valarray<double> foo (val,4);

        std::valarray<double> bar = log10 (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        std::valarray<double> val (10);
        std::valarray<double> results;

        for (int i=0; i<10; ++i) val[i]=i+1;
        std::cout << "val:";
        for (std::size_t i=0; i<val.size(); ++i) std::cout << ' ' << val[i];
        std::cout << '\n';

        results = std::pow (val,val);
        std::cout << "val^val:";
        for (std::size_t i=0; i<results.size(); ++i) std::cout << ' ' << results[i];
        std::cout << '\n';

        results = std::pow (val,2.0);
        std::cout << "val^2:";
        for (std::size_t i=0; i<results.size(); ++i) std::cout << ' ' << results[i];
        std::cout << '\n';

        results = std::pow (2.0,val);
        std::cout << "2^val:";
        for (std::size_t i=0; i<results.size(); ++i) std::cout << ' ' << results[i];
        std::cout << '\n';
    }

    {
        double val[] = {1.5, 3.0, 6.0};
        std::valarray<double> foo (val,3);

        std::valarray<double> bar = sin (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar should be like this : ";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {0.3, 1.2, 3.5};
        std::valarray<double> foo (val,3);

        std::valarray<double> bar = sinh (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar: ";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {5, 25.0, 100.0};
        std::valarray<double> foo (val,3);

        std::valarray<double> bar = sqrt (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {1.2, 3.14, 5.0};
        std::valarray<double> foo (val,3);

        std::valarray<double> bar = tan (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    {
        double val[] = {0.3, 1.00, 3.0};
        std::valarray<double> foo (val,3);

        std::valarray<double> bar = tanh (foo);

        std::cout << "foo:";
        for (std::size_t i=0; i<foo.size(); ++i)
            std::cout << ' ' << foo[i];
        std::cout << '\n';

        std::cout << "bar:";
        for (std::size_t i=0; i<bar.size(); ++i)
            std::cout << ' ' << bar[i];
        std::cout << '\n';
    }

    return 0;
}