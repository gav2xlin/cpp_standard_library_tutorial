#include <iostream>
#include <vector>

using namespace std;

int main(void) {
    {
        vector<int> v1;

        cout << "size of v1 = " << v1.size() << endl;
    }

    {
        vector<int> v(5, 200);

        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        vector<int> v1(5);

        /* assigned value to vector v1 */
        for (int i = 0; i < v1.size(); ++i)
            v1[i] = i + 1;

        /* create a range constructor v2 from v1 */
        vector<int> v2(v1.begin(), v1.end());

        for (int i = 0; i < v2.size(); ++i)
            cout << v2[i] << endl;
    }

    {
        vector<int> v1(5);

        /* assign value to vector v1 */
        for (int i = 0; i < v1.size(); ++i)
            v1[i] = i + 1;

        /* create a copy constructor v2 from v1 */
        vector<int> v2(v1);

        for (int i = 0; i < v2.size(); ++i)
            cout << v2[i] << endl;
    }

    {
        /* create fill constructor */
        vector<int> v1(5, 123);

        cout << "Elements of vector v1 before move constructor" << endl;
        for (int i = 0; i < v1.size(); ++i)
            cout << v1[i] << endl;

        /* create constructor using move semantics */
        vector<int> v2(move(v1));

        cout << "Elements of vector v1 after move constructor" << endl;
        for (int i = 0; i < v1.size(); ++i)
            cout << v1[i] << endl;

        cout << "Element of vector v2" << endl;
        for (int i = 0; i < v2.size(); ++i)
            cout << v2[i] << endl;
    }

    {
        auto il = {1, 2, 3, 4, 5};
        /* create vector from initializer list */
        vector<int> v(il);

        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        vector<int> v1;

        cout << "Initial size  = " << v1.size() << endl;

        /* 5 integers with value = 100 */
        v1.assign(5, 100);

        cout << "Modified size = " << v1.size() << endl;

        /* display vector values */
        for (int i = 0; i < v1.size(); ++i)
            cout << v1[i] << endl;
    }

    {
        vector<int> v(5, 100);

        cout << "Initial vector contents" << endl;
        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;

        cout << endl;

        cout << "Modified vector contents" << endl;

        v.assign(v.begin(), v.begin() + 2);
        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        /* Create empty vector */
        vector<int> v;
        /* create initializer list */
        auto il = {1, 2, 3, 4, 5};

        /* assign values from initializer list */
        v.assign(il);

        /* display vector elements */
        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        auto il = {1, 2, 3, 4, 5};
        vector<int> v(il);

        for (int i = 0; i < v.size(); ++i)
            cout << v.at(i) << endl;
    }

    {
        auto il = {1, 2, 3, 4, 5};
        vector<int> v(il);

        cout << "Last element of vector = " << v.back() << endl;
    }

    {
        auto il = {1, 2, 3, 4, 5};
        vector<int> v(il);

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v;

        for (int i = 0; i < 5; ++i)
            v.push_back(i + 1);

        cout << "Number of elements in vector = " << v.size() << endl;
        cout << "Capacity of vector           = " << v.capacity() << endl;
    }

    {
        auto ilist = {1, 2, 3, 4, 5};
        vector<int> v(ilist);

        for (auto it = v.cbegin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        auto ilist = {1, 2, 3, 4, 5};
        vector<int> v(ilist);

        cout << "Initial size of vector     = " << v.size() << endl;
        /* destroy vector */
        v.clear();
        cout << "Size of vector after clear = " << v.size() << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};

        for (auto it = v.crbegin(); it != v.crend(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};
        int *p;

        p = v.data();

        for (int i = 0; i < v.size(); ++i)
            cout << *p++ << endl;
    }

    {
        vector<int> v = {1, 2, 5};

        /* insert element at index 3 */
        auto it = v.emplace(v.begin() + 2, 4);

        /* insert element at index 2 */
        v.emplace(it, 3);

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v = {1, 2, 3};

        v.emplace_back(4);
        v.emplace_back(5);

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v;

        if (v.empty())
            cout << "Vector v1 is empty" << endl;

        v.push_back(1);
        v.push_back(2);
        v.push_back(3);

        if (!v.empty())
            cout << "Vector v1 is not empty" << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};

        cout << "Original vector" << endl;
        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;

        /* Remove first element */
        v.erase(v.begin());

        cout << "Modified vector" << endl;
        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};

        cout << "Original vector" << endl;
        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;

        /* Remove first two element */
        v.erase(v.begin(), v.begin() + 2);

        cout << "Modified vector" << endl;
        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};

        cout << "First element of vector = " << v.front() << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};
        int *p = NULL;

        /* allocate memory to store 5 elements */
        p = v.get_allocator().allocate(5);

        for (int i = 0; i < 5; ++i)
            p[i] = i + 1;

        for (int i = 0; i < 5; ++i)
            cout << p[i] << endl;
    }

    {
        vector<int> v = {3, 4, 5};

        auto it = v.insert(v.begin(), 2);
        v.insert(it, 1);

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v = {5};

        v.insert(v.begin(), 4, 5);

        for (auto it = v.begin(); it != v.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v1 = {2, 3, 4, 5};
        vector<int> v2 = {1};

        v2.insert(v2.begin() + 1, v1.begin(), v1.begin() + 3);

        for (auto it = v2.begin(); it != v2.end(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2;

        for (int i = 0; i < v1.size(); ++i)
            v2.insert(v2.begin() + i, move(*(v1.begin() + i)));

        for (int i = 0; i < v2.size(); ++i)
            cout << v2[i] << endl;
    }

    {
        vector<int> v = {1, 2};
        auto ilist = {3, 4, 5};

        v.insert(v.begin() + 2, ilist);

        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        vector<int> v;

        cout << "max_size = " << v.max_size() << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2;

        /* assignment operator */
        v2 = v1;

        for (int i = 0; i < v2.size(); ++i)
            cout << v2[i] << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2;

        /* assignment using move construct */
        v2 = move(v1);

        for (int i = 0; i < v2.size(); ++i)
            cout << v2[i] << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};
        auto il = {1, 2, 3, 4, 5};

        /* assignment using move construct */
        v = il;

        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};

        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};

        /* Remove last three elements */
        v.pop_back();
        v.pop_back();
        v.pop_back();

        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        vector<int> v;

        /* Insert 5 elements */
        for (int i = 0; i < 5; ++i)
            v.push_back(i + 1);

        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        vector<int> v = {1, 2, 3, 4, 5};

        /* Iterate vector in reverse order */
        for (auto it =  v.rbegin(); it != v.rend(); ++it)
            cout << *it << endl;
    }

    {
        vector<int> v1;
        vector<int> v2;
        ssize_t size;

        size = v1.capacity();

        for (int i = 0; i < 25; ++i) {
            v1.push_back(i);
            if (size != v1.capacity()) {
                size = v1.capacity();
                cout << "Expanding vector v1 to hold " << size
                     << " elements" << endl;
            }
        }

        cout << endl << endl;

        /* Reserve space for 25 elements */
        v2.reserve(25);

        for (int i = 0; i < 25; ++i) {
            v2.push_back(i);
            if (size != v2.capacity()) {
                size = v2.capacity();
                cout << "Expanding vector v2 to hold " << size
                     << " elements" << endl;
            }
        }
    }

    {
        vector<int> v;

        cout << "Initial vector size = " << v.size() << endl;

        v.resize(5, 10);
        cout << "Vector size after resize = " << v.size() << endl;

        cout << "Vector contains following elements" << endl;
        for (int i = 0; i < v.size(); ++i)
            cout << v[i] << endl;
    }

    {
        vector<int> v(128);

        cout << "Initial capacity = " << v.capacity() << endl;

        v.resize(25);
        cout << "Capacity after resize = " << v.capacity() << endl;

        v.shrink_to_fit();
        cout << "Capacity after shrink_to_fit = " << v.capacity() << endl;
    }

    {
        vector<int> v;

        cout << "Initial vector size = " << v.size() << endl;

        v.resize(128);
        cout << "Vector size after resize = " << v.size() << endl;
    }

    {
        vector<int> v1;
        vector<int> v2 = {1, 2, 3, 4, 5};

        v1.swap(v2);

        cout << "Vector v1 contains" << endl;
        for (int i = 0; i < v1.size(); ++i)
            cout << v1[i] << endl;
    }

    {
        vector<int> v1;
        vector<int> v2;

        if (v1 == v2)
            cout << "v1 and v2 are equal" << endl;

        v1.resize(10, 100);

        if (!(v1 == v2))
            cout << "v1 and v2 are not equal" << endl;
    }

    {
        vector<int> v1;
        vector<int> v2;

        v1.resize(10, 100);

        if (v1 != v2)
            cout << "v1 and v2 are not equal" << endl;

        v1 = v2;
        if (!(v1 != v2))
            cout << "v1 and v2 are equal" << endl;
    }

    {
        vector<int> v1 = {1, 2};
        vector<int> v2 = {1, 2, 3, 4, 5};

        if (v1 < v2)
            cout << "v1 is less than v2" << endl;
        v1.resize(5,10);

        if (!(v1 < v2))
            cout << "v1 is greater than v2" << endl;
    }

    {
        vector<int> v1 = {1, 2};
        vector<int> v2 = {1, 2, 3, 4, 5};

        if (v1 <= v2)
            cout << "1. v1 is less than or equal to v2" << endl;

        v1 = v2;

        if (v1 <= v2)
            cout << "2. v1 is less than or equal to v2" << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2;

        if (v1 > v2)
            cout << "v1 is greater than v2" << endl;

        v2.push_back(1);
        v2.push_back(2);
        v2.push_back(3);
        v2.push_back(4);
        v2.push_back(6);

        if (!(v1 > v2))
            cout << "v1 is not greater than v2" << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2;

        if (v1 >= v2)
            cout << "v1 is greater than or equal to v2" << endl;

        v1 = v2;

        if (v1 >= v2)
            cout << "v1 is greater than or equal to v2" << endl;
    }

    {
        vector<int> v1 = {1, 2, 3, 4, 5};
        vector<int> v2 = {10, 20, 30};

        cout << "Contents of vector v1 before swap operation" << endl;
        for (int i = 0; i < v1.size(); ++i)
            cout << v1[i] << endl;

        cout << "Contents of vector v2 before swap operation" << endl;
        for (int i = 0; i < v2.size(); ++i)
            cout << v2[i] << endl;

        swap(v1, v2);
        cout << "Contents of vector v1 after swap operation" << endl;
        for (int i = 0; i < v1.size(); ++i)
            cout << v1[i] << endl;

        cout << "Contents of vector v2 after swap operation" << endl;
        for (int i = 0; i < v2.size(); ++i)
            cout << v2[i] << endl;
    }

    return 0;
}